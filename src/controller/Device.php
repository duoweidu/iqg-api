<?php
namespace controller;
class Device extends Base
{
    public function __construct($uri){
        parent::__construct($uri);
    }

    public function add(){
        $data = array_merge($this->client, $this->input);
        $device = new \model\Device;
        $data['idfa'] = isset ($data['idfa']) ? $data['idfa'] : '';
        $device->add($data);
        return $this->output;
    }
}

