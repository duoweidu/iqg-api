<?php
namespace controller;

/**
 *  金币
 */
class Ecion extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }

    /**
     * 金币兑换成余额。
     *
     * 需要登录 ：是
     *
     * @api POST /ecion/exchanges/
     *
     * @param string $subtotal 必须。想花掉多少金币。
     *
     * @return json-object {
            "total" : 123, //可用金币int
            "transactionAmount" : 200 //这次消耗了多少金币int
        }
     *
     * @example shell curl -d 'subtotal=200' 'http://api.iqianggou.com/ecion/exchanges/' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' | python -m json.tool
     */
    public function exchange($subtotal=null)
    {
        $this->checkLogin();
        $this->checkParams(array('subtotal'));
        $m = new \model\Ecion();
        $this->output['http']['X-Content-Type'] = 'json-object';
        $this->output['data'] = $m->exchange(array_merge($this->client, $this->input));
        return $this->output;
    }

    /**
     * 我的金币余额情况。
     *
     * 需要登录 ：是
     *
     * @api GET /ecion/status
     *
     * @return json-object {
            "total" : 123, //可用金币int
            "accumulativeTotal" : 456, //累计金币int
            "ranking" : 1, //排名int
            "rankingPercent" : 0.90, //排名百分比float
            "countLogin" : 20, //登录奖励次数int
            "countInvite" : 10, //邀请好友奖励次数int
            "countComment" : 20 //评论奖励次数int
        }
     *
     * @example shell curl 'http://api.iqianggou.com/ecion/status' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' | python -m json.tool
     */
    public function getStatus()
    {
        $this->checkLogin();
        $this->output['http']['X-Content-Type'] = 'json-object';
        $m = new \model\Ecion();
        $this->output['data'] = $m->getStatus($this->client);
        return $this->output;
    }

    /**
     * 金币的账单。按时间倒排。当看推荐好友的记录时，remark是好友的原始手机号。
     *
     * 需要登录 ：是
     *
     * @api GET /ecion/bills
     *
     * @param string $type 必须。类型。1是登录，2个推荐好友，3是评论，4是兑换。
     *
     * @param int $offset 选填。跳过多少条。
     *
     * @param int $limit 选填。取多少条。
     *
     * @return json-object {
        "entities":[
            {
                "addTime": 1381225277,
                "transactionAmount": -14600,
                "remark": "\u4e0b\u5355"
            },
            {
                "addTime": 1381301704,
                "transactionAmount": -5800, 交易金额，int
                "remark": "\u4e0b\u5355"
            }
         ],
         "transactionAmountSum": -5800, 交易金额合计，int
         "total":300 //总条数，翻页用的
        }
     *
     * @example shell curl 'http://api.iqianggou.com/ecion/bills?type=1' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' | python -m json.tool
     */
    public function getBills($type=null, $offset = null, $limit = null)
    {
        $this->checkLogin();
        $this->checkParams(array('type'));
 
        $m = new \model\Ecion();
        $data = array_merge($this->client, $this->input);
        if(!isset($data['offset']) || empty($data['offset'])) {
            $data['offset'] = 0;
        }
        if(!isset($data['limit']) || empty($data['limit'])) {
            $data['limit']  = \PEAR2\Conf::get('ecion', 'billsDefaultLimit');
        }
        $r = $m->getList($data);
        $data = array(
            'entities' => array(),
            'total' => intval($r['total']),
            'transactionAmountSum' => intval($r['transactionAmountSum']),
        );
        foreach($r['entities'] as $one) {
            $tmp = array(
                'addTime' => intval($one['addTime']),
                'transactionAmount'   => \intval($one['transactionAmount']),
                'remark'  => !empty($one['remark']) ? _($one['remark']) : null,
            );
            $data['entities'][] = $tmp;
        }

        $this->output['data'] = $data;
        return $this->output;
    }
}
?>
