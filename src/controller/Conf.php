<?php
namespace controller;

/**
 * 配置
 */
class Conf extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }
    
    /**
     * 取配置
     *
     * 需要登录 ： 否
     *
     * @api GET /confs/
     *
     * @return json-object {"contacts":{"weixin":"iqianggou","weibo":"\u7231\u62a2\u8d2dAPP","qq":"2507465892","email":"service@doweidu.com"},"share":"Let's bargin it! original price is {currentPrice}, now is {currentPrice}!","faq":"http://example.com/"}
     * @example shell curl 'http://api.iqianggou.com/confs/' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     * @example shell curl 'http://api.iqianggou.com/confs/' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'Accept-Language: zh' | python -m json.tool
     */
    public function index()
    {
        $all = \PEAR2\Conf::get('app', 'all');
        $data = $all['default'];
        foreach($all[$this->client['lang']] as $key=>$one) {
            if(is_array($one)) {
                $data[$key] = array_merge($data[$key], $one);
            } else {
                $data[$key] = $one;
            }
        }

	$m = new \model\Conf();
	$data['topupPlans'] = $m->getTopupGivePlan();	
        $this->output['data'] = $data;
        return $this->output;
    }
}
?>
