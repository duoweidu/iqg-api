<?php
namespace controller;

/**
 * 订单
 */
class Order extends Base
{
    /**
     * @var string $sid
     */
    private $sid;

    public function __construct($uri)
    {
        parent::__construct($uri);
        preg_match('/^\/orders\/(?P<sid>[a-zA-Z0-9]+)[\/[a-zA-Z0-9]+]?/', $uri, $matches);
        if ( isset($matches['sid']) ) {
            $this->sid = $matches['sid'];
        }
    }

    /**
     * 创建一个订单
     *
     * @api POST /orders/
     *
     * @param string $itemSid 必选。
     *
     * 需要登录 ：是
     *
     * 需要经纬度 ：是
     *
     * @return json-object {
        "sid":"0938743774513615" //string
     }
     * @example shell curl 'http://api.iqianggou.com/orders/' -i -d 'itemSid=s500524' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.588674,31.204246'
     */
    public function add($itemSid=null)
    {
        $this->checkLogin();
        $this->checkLngLat();
        $m = new \model\Order();
        try {
            $this->output['data'] = $m->add(array_merge($this->input, $this->client));
        } catch (\exception\Model $e) {
            $tmp = $e->getData();
            $tmp['http']['X-Content-Type'] = 'json-object';
            throw new \exception\Http(substr($e->getCode(), 0, 3), '', $tmp);
        }

        return $this->output;
    }

    /**
     * 添加评论
     *
     * 每个订单只能评论一次
     *
     * 需要登录 ：是
     *
     * @api POST /orders/:sid/comments/
     *
     * @param string $content 必填。内容。
     *
     * @param int $itemRating 必填。商品评分。
     *
     * @param int $attitudeRating 必填。态度评分。
     *
     * @param json-array $imgs 选填。多张图片的文件名。比如["71f6950d988e75b9b68d1ffcc2d3b5da","b8e7128ccd943c0725aa05b9440030e0"]
     *
     * @return json-array [
     ]
     * @example shell curl 'http://api.iqianggou.com/orders/1442617513854467/comments/' -i -d 'content=asdf' -d 'imgs=["71f6950d988e75b9b68d1ffcc2d3b5da","b8e7128ccd943c0725aa05b9440030e0"]' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     */
    public function addComment($content = null, $itemRating = null, $attitudeRating = null, $imgs = null)
    {
        $this->checkParams(array('content'));
        $this->checkLogin();
        $this->checkEmpty(array('content'));
        $m = new \model\Comment();
        $tmp = array_merge($this->input, $this->client);
        $tmp['orderSid'] = $this->sid;
        if (isset($this->input['imgs'])) {
            $tmp['imgs'] = json_decode($this->input['imgs'], true);
        }
        $m->add($tmp);
        $this->output['http']['X-Api-Status-Msg'] = _('评论成功');
        return $this->output;
    }

    /**
     * 删除一个订单
     *
     * @api DELETE /orders/:sid
     *
     * 需要登录 ：是
     *
     * @return json-array [
     ]
     * @example shell curl -X 'DELETE' 'http://api.iqianggou.com/orders/12752019317507562532' -i -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     */
    public function delete()
    {
        $this->checkLogin();
        $m = new \model\Order();
        $tmp = $this->client;
        preg_match('/^\/orders\/(?P<sid>\w+)$/', $this->uri, $matches);
        $tmp['sid'] = $this->sid;
        $m->delete($tmp);
        return $this->output;
    }

    /**
     * 修改订单
     *
     * @api POST /orders/:sid
     *
     * @param int $status 必填。把订单状态改成几。比如-2等待到帐，2已领用。
     *
     * @return json-array []
     * @example shell curl -d 'status=-2' 'http://api.iqianggou.com/orders/0020688277790618' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -H 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     */
    public function edit($status=null) {
        $this->checkParams(array('status'));
	$this->checkLogin();
        $m = new \model\Order();
        $data = array(
            'sid' => $this->sid,
            'userId' => $this->client['userId'],
        );
        if($this->input['status'] == '-2') {
            $m->setToArriving($data);
        } else if($this->input['status'] == '2') {
            $m->setToUsed($data);
        } else {
            throw new \exception\Http(403, 'unknown status');
        }
        return $this->output;
    }

    /**
     * 取一个订单
     *
     * @api GET /orders/:sid
     *
     * fields ： 可选。需要哪些字段，如果不传则取最大集。json格式，比如["buyDate","expiredDate"]
     *
     * 需要登录 ：是
     *
     * @return json-object {
        "addTime": 1386307552, //string。购买日期
        "ecode": "asd123", //string。兑换码，即exchange code。
        "expiredTime": 1386307552, //string。过期时间
        "isCommented": true, //boolean。是否已评论
        "itemSid": "s500016", //string。
        "subtotal": 1, //float。订单金额
        "sid": "12315778536752676667",  //string。sid即“安全id”，即加密后的id。
        "status": -1, //int。-2等待到帐，-1无效，0未付款，1已付款，2已结束
        "ecodeValidateType" : 4, //int。4是密码验证
        "ecodeValidatePassword" : "1234" //string。商户密码
       }
     * @example shell curl 'http://api.iqianggou.com/orders/0020688277790618' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -H 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     */
    public function get()
    {
        $this->checkLogin();

        $tmp = array_merge($this->input, $this->client);
        preg_match('/^\/orders\/(?P<sid>\w+)$/', $this->uri, $matches);
        $tmp['sid'] = $matches['sid'];
        $m = new \model\Order();
        $r = $m->get($tmp);
        $data = array();

        if(isset($this->input['fields'])) {
            $fields = json_decode($this->input['fields'], true);
            $tmp = array();
            foreach($fields as $column) {
                if(isset($one[$column])) {
                    $tmp[$column] = $one[$column];
                }
            }
            $data = $tmp;
        } else {
            $data = $r;
        }

        $this->output['data'] = $data;
        return $this->output;
    }

    /**
     * 取订单列表
     *
     * @api GET /orders/
     *
     * @param int $offset 必填。
     *
     * @param int $limit 必填。
     *
     * @param int/json-array $status 必填。订单状态。可以为int，比如1。可以为json-array，比如[-2,0]
     *
     * @param int $fields 选填。需要哪些字段，如果不传则取最大集。json格式，比如["buyDate","expiredDate"]
     *
     * @param int $isCommented 选填。过滤已评论或未评论的订单，值为0或1。
     *
     * 需要登录 ：是
     *
     * @return json-object {
        "entities":[
            {
                "addTime": 1386306952, //订单创建时间
                "ecode": "1234567", //兑换码
                "expiredTime": 1386306952, //过期时间
                "isCommented": true, //boolean。是否已评论
                "itemSid": "s500016",
                "subtotal": 1, //订单金额
                "sid": "12315778536752676667",  //sid即“安全id”，即加密后的id。string
                "status": -1 //-4自动退款，-3已退款，-2等待到帐，0未付款，1已付款，2已结束
            },
            {
                "itemSid": "s500016",
                ......
            }
         ],
         "total":123
      }
     * @example shell curl 'http://api.iqianggou.com/orders/?status=0&offset=0&limit=30' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -H 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     * @example shell curl 'http://api.iqianggou.com/orders/?status=\[-2,0\]&offset=0&limit=30' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -H 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     * @example shell curl 'http://api.iqianggou.com/orders/?status=0&offset=0&limit=30&fields=\["sid","status"\]' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -H 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     */
    public function index($offset=null, $limit=null, $status=null, $fields=null, $isCommented=null)
    {
        $this->output['http']['X-Content-Type'] = 'json-object';
        $this->checkParams(array('status', 'offset', 'limit'));
        $this->checkLogin();
        $m = new \model\Order();
        $tmp = array_merge($this->input, $this->client);
        if(strpos($this->input['status'], '[') === 0) {
            $tmp['status'] = json_decode($this->input['status'], true);
        } else {
            $tmp['status'] = array($this->input['status']);
        }
        $r = $m->getList($tmp);
        $data = array(
            'entities' => array(),
            'total' => intval($r['total']),
        );

        if(isset($this->input['fields'])) {
            $fields = json_decode($this->input['fields'], true);
            foreach($r['entities'] as $one) {
                $tmp = array();
                foreach($fields as $column) {
                    if(isset($one[$column])) {
                        $tmp[$column] = $one[$column];
                    }
                }
                $data['entities'][] = $tmp;
            }
        } else {
            $data = $r;
        }

        $this->output['data'] = $data;
        return $this->output;
    }

    /**
     * 获取余额支付签名
     *
     * @api GET /orders/:sid/balance
     *
     * @param string $password 登录密码
     *
     * @return json-object {
     	    "sign" : ""
     } 
     * 客户端应该sign，校验的方式是：md5(orderId&orderStatus&addTime&payPrice)
     * @example shell curl 'http://api.iqianggou.com/orders/5428007285064069/balance?password=123456' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     */
    public function getBalanceSign()
    {
	$this->checkParams(array('password'));
	$this->checkEmpty(array('password'));

	$this->checkLogin();
	$user = new \model\User();
	$user->verifyPassword( $this->client['userId'], $this->input['password'] );

        $m = new \model\Order();
	$order = $m->getOrder($this->sid);
	$sign = md5($order['orderId'] . '&' . $order['orderStatus'] . '&' . $order['addTime'] . '&' . $order['payPrice']);
	$this->output['data'] = array('sign'=> $sign);
	return $this->output;
    }

    /**
     * 获取alipay支付的签名
     * 
     * @api GET /orders/:sid/alipay
     *
     * @example shell curl 'http://api.iqianggou.com/orders/5428007285064069/alipay' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     *
     * @return json-object {"sign":"partner=\"2088801342252957\"&seller=\"leiyong@doweidu.com\"&out_trade_no=\"0025886212429135\"&subject=\"[\u6d4b]\u7126\u7cd6\u8292\u679c\u5947\u4e50\u51b0\u4e2d\u676f(12oz)\"&body=\"[\u6d4b]\u7126\u7cd6\u8292\u679c\u5947\u4e50\u51b0\u4e2d\u676f(12oz)\"&total_fee=\"28.00\"&gmt_create=\"2013-10-22 16:21:44\"&pay_expire=\"15\"&notify_url=\"http:\/\/api.iqianggou.com\/alipaynotify\"&sign_type=\"RSA\"&sign=\"tjmii7yWlTFBcYiSEzq9CWbRrsfJwaD7MGOYO%2BhAo8%2BJbRygizm8fgDQi4lY%2BGN%2FRRWRjHo0uXTufZRVgQ41grQ819Sh9JddYvptgeE6mdWmLwi2xGy2bxwtZaF0srU3KNpZmd%2BcZpXoiYXr8xwBegmz1tJWKtMnaQSc%2BhDTj9A%3D\""} 
     */
    public function getAlipaySign()
    {
	$this->checkLogin();
	$alipay = new \model\Payment;
	$sign = $alipay->getOrderAlipaySign($this->sid);
	$this->output['data']['sign'] = $sign;
	return $this->output;
    }

    /**
     * 获取阿里web支付的URL
     * 
     * @api GET /orde/:sid/aliwebpay
     *
     * @example shell curl 'http://api.iqianggou.com/orders/5428007285064069/aliwebpay' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     *
     * @return json-object {"url":"http:\/\/wappaygw.alipay.com\/service\/rest.htm?req_data=%3Cauth_and_execute_req%3E%3Crequest_token%3E2013102281ce0fc500caa553932a4beaf7c9e059%3C%2Frequest_token%3E%3C%2Fauth_and_execute_req%3E&service=alipay.wap.auth.authAndExecute&sec_id=0001&partner=2088801342252957&call_back_url=http%3A%2F%2Fapi.iqianggou.com%2Falipaywapcallback&format=xml&v=2.0&sign=aoJqo03tcjyIl09kgKKziOHHIfFJIlhi%2B6ibDkV2Pl3w%2BKmGZkPndRc3oPR6atAVd8JpAQru7b%2Brfj%2BfolQxkyKWc0bucyrvmHpSB0v2%2BJQ2UsejJdPA5DVoUgk3XPlL3BkHuubd0n907acntnuNdMtYjGISWuiDrdrea2otDYg%3D"}
     */
    public function getAliwebpayUrl()
    {
	$this->checkLogin();
	$alipay = new \model\Payment;
	$url = $alipay->getOrderAliwebpayUrl( $this->sid );
	$this->output['data']['url'] = $url;
	return $this->output;
    }

    /**
     * 获取smoov pay支付的url
     *
     * @api GET /order/:sid/smoovpay
     * @example shell curl 'http://api.iqianggou.com/orders/5428007285064069/smoovpay' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     * @return json-object {"url":"https:\/\/sandbox.smoovpay.com/TokenAccess/47140d8b7eb58b3f1b6d923f4261cb9f"}
     */
    public function getSmoovPayUrl(){
	$this->checkLogin();
	$smoovpay = new \model\Payment;
	$url = $smoovpay->getOrderSmoovPayUrl( $this->sid );
	$this->output['data']['url'] = $url;
	return $this->output;
    }
}
?>
