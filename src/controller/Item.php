<?php
namespace controller;

/**
 * 商品
 */
class Item extends Base
{
    private $itemModel;

    public function __construct($uri)
    {
        parent::__construct($uri);
        $this->itemModel = new \model\Item();
    }

    /**
     * 取商品列表 或者 指定ids取几个商品
     *
     * header token ：必须。
     *
     * header经纬度 ：取列表时必须，指定id时不用这个参数。
     *
     * @api GET /items/
     *
     * @param int $offset 取列表时必须，指定id时不用这个参数。跳过多少条。
     *
     * @param int $limit 取列表时必须，指定id时不用这个参数。取多少条。
     *
     * @param float $lng 可选。自定义经度，如果不传则返回附近的商品。如果传了，则返回那个位置的商品
     *
     * @param float $lat 可选。自定义纬度。
     *
     * @param json-array $fields 可选。需要哪些字段，如果不传则取最大集。json格式，比如["name","sid"]
     *
     * @param string $categorySid 可选。分类sid。如果不传，就不限。
     *
     * @param string $citySid 可选。城市sid。如果不传，就表示看周边。
     *
     * @param string $sids 可选。多个商品的sid，如果传，则指定取这些商品。
     *
     * @return json-object {
         "entities":[
             {
                "ad": {
                    "title": "asdf",
                    "uri": "http://nnpsp.taobao.com"
                },
                "bargainRange":20, //砍价幅度。int。

                "currentPrice":250, //现在价格。int。

                "sid":"1", //加密后的ID。string。

                "imgs": [
                    "http://img-agc.iqianggou.com/9fbff886185322ffce3fb0c19ec65e92",
                    "http://img-agc.iqianggou.com/52b55a03dccc51bbc0322ba1457c6ebd"
                ],

                "intro":"asdf", //介绍

                "isNew":true, //是否新商品。bool。

                "isBargained":true, //是否已砍价。bool。

                "likeCount":3, //今天喜欢次数。int。

                "minPrice":2, //最低价。int或float。

                "name":"绝味鸭头",

                "notice":"购买成功后，您需在7日之内至指定门店领取抢购的商品，过期作废。", //特别提示。string。

                "shelfStatus": "on", //上架状态，有3种：on、off、comingSoon，即 已上架、已下架、即将上架

                "shopsSids": [
                    "s10933"
                ],

                "stock":3 //库存。int。
            },
            {
                //等等字段，同上。
            }
         ],
         "total":123
     }
     * @example shell curl --compressed 'http://api.iqianggou.com/items/?offset=0&limit=3' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.11,31.291391' | python -m json.tool
     * @example shell curl --compressed 'http://api.iqianggou.com/items/?citySid=10&fields=\["currentPrice","distance","sid","likeCount"\]&offset=0&limit=3' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.498458,31.291391' | python -m json.tool
     * @example shell curl --compressed 'http://api.iqianggou.com/items/?fields=\["currentPrice","distance","sid","likeCount"\]&offset=0&limit=3' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.498458,31.291391' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' | python -m json.tool
     * @example shell curl --compressed 'http://api.iqianggou.com/items/?sids=\["s1","s502497"\]&offset=0&limit=3' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.498458,31.291391' | python -m json.tool
     * @example shell curl --compressed 'http://api.iqianggou.com/items/?sids=\["s1","s502497"\]&fields=\["currentPrice","sid","likeCount"\]&offset=0&limit=3' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.498458,31.291391' | python -m json.tool
     */
    public function index($offset='', $limit='', $fields='', $categorySid='', $citySid='', $sids='', $lng='', $lat='')
    {
        $clientTmp = $this->client;
        //如果用户自定义位置，则看那里的商品。log还是记真实的经纬度。赤道是0，要注意。
        if(isset($this->input['lat']) && ($this->input['lat']!=='') && isset($this->input['lng']) && ($this->input['lng']!=='')) {
            $clientTmp['lat'] = $this->input['lat'];
            $clientTmp['lng'] = $this->input['lng'];
        }
        if(isset($this->input['sids'])) {
            $tmp = array(
                'sids' => json_decode($this->input['sids'], true),
            );
            $r = $this->itemModel->getList(array_merge($tmp, $clientTmp));
        } else {
            $this->checkLngLat();
            $params = array(
                "offset",
                "limit",
            );
            $this->checkParams($params);

            $r = $this->itemModel->getList(array_merge($this->input, $clientTmp));
        }
        $data = array(
            'entities' => array_values($r['entities']),
            'total' => intval($r['total']),
        );
        if(empty($r['entities'])) {
            $this->output['data'] = $data;
            return $this->output;
        }
        if(isset($this->input['fields'])) {
            $fields = json_decode($this->input['fields'], true);
            $data['entities'] = array();
            foreach($r['entities'] as $sid=>$one) {
                $tmp = array();
                foreach($fields as $column) {
                    if(isset($one[$column])) {
                        $tmp[$column] = $one[$column];
                    }
                }
                $data['entities'][] = $tmp;
            }
        }
        
        //未登录也统计，userId可以为空
        $logs = array();
        //一条即为点击
        if(isset($this->input['sids']) && count(json_decode($this->input['sids'], true)) == 1) {
            if(count($r['entities']) == 1) {
                $one = array_values($r['entities'])[0];
                $itemId = \model\Item::decryptSid($one['sid']);
                $this->itemModel->addCountClick($itemId);

                $log = array(
                    'shopId' => \model\Shop::decryptSid($one['shopsSids'][0]),
                    'goodsId' => $itemId,
                    'userId' => $this->client['userId'],
                    'lat' => $this->client['lat'],
                    'lng' => $this->client['lng'],
                    'ip' => $this->client['ip'],
                    'addTime' => time(),
                );
                \model\Log::add('click', $log);
            }
        } else {
            //展示
            $logs = array();
            foreach($r['entities'] as $sid=>$one) {
                $itemId = \model\Item::decryptSid($one['sid']);
                $logs[] = array(
                    'shopId' => \model\Shop::decryptSid($one['shopsSids'][0]),
                    'goodsId' => $itemId,
                    'userId' => $this->client['userId'],
                    'lat' => $this->client['lat'],
                    'lng' => $this->client['lng'],
                    'ip' => $this->client['ip'],
                    'addTime' => time(),
                );
            }
            if(!empty($logs)) {
                \model\Log::multiAdd('show', $logs);
            }
        }

        $this->output['data'] = $data;
        return $this->output;
    }

    /**
     * 砍价1次
     *
     * 需要登录 ：是
     *
     * 需要经纬度 ：是
     *
     * 返回的状态码：
        4032 => '很遗憾，此商品今日您已砍过价',
        4033 => '已经抢完了，明天再来吧',
        40313 => '到底啦',
     *
     * @api POST /items/:sid/bargains/
     *
     * @return json-object {
            "currentPrice":150, //现在价格 float
            "isBargained":false,
            "likeCount":99, //现在喜欢数
            "priceReduced":50 //减价幅度
            "stock":1
        }
     *
     * @example shell curl --compressed -i -X 'POST' 'http://api.iqianggou.com/items/s1/bargains/' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.498458,31.291391'
     */
    public function bargain()
    {
        $this->checkLogin();
        $this->checkLngLat();
        preg_match('/^\/items\/(?P<itemSid>\w+)\/bargains\/$/', $this->uri, $matches);
        $m = new \model\Item();
        $r = $m->bargain($matches['itemSid'], array_merge($this->input, $this->client));
        ksort($r);
        $this->output['data'] = $r;

        $log = array(
            'goodsId' => \model\Item::decryptSid($matches['itemSid']),
            'userId' => $this->client['userId'],
            'lat' => $this->client['lat'],
            'lng' => $this->client['lng'],
            'ip' => $this->client['ip'],
            'addTime' => time(),
        );
        \model\Log::add('bargain', $log);

        return $this->output;
    }

    /**
     * 取1个商品
     *
     * 需要登录 ：否
     *
     * header经纬度 ：可选。
     *
     * @api GET /items/:sid
     *
     * @param json-array $fields 可选。需要哪些字段，如果不传则取最大集。json格式，比如["name","sid"]
     *
     * @return json-object {
            "ad": {
                "title": "asdf",
                "uri": "http://nnpsp.taobao.com"
            },
            "bargainRange":20, //砍价幅度。int。

            "currentPrice":250, //现在价格。int。

            "sid":"1",

            "imgs":[
                "http://img-agc.iqianggou.com/beb25a9470fe09165498c900b05dc691", //服务器支持任意分辨率的图片，文档：http://docs.qiniu.com/api/v6/image-process.html#imageMogr
                "http://img-agc.iqianggou.com/9fbff886185322ffce3fb0c19ec65e92"
            ],

            "intro":"辣的超过瘾超入味,连骨头里都很入味哦", //介绍

            "isNew":true, //是否新商品。bool。

            "isBargained":true, //是否已砍价。bool。

            "likeCount":3, //今天喜欢次数。int。

            "minPrice":2, //最低价。int或float。

            "name":"绝味鸭头",

            "notice":"购买成功后，您需在7日之内至指定门店领取抢购的商品，过期作废。", //特别提示。string。

            "shopsSids":["s10103", "s10010"] //多个门店sid。数组内部是string。

            "stock":3 //库存。int。
        }
     * @example shell curl --compressed 'http://api.iqianggou.com/items/s1' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.11,31.291391' | python -m json.tool
     * @example shell curl --compressed 'http://api.iqianggou.com/items/s1?fields=\["sid","currentPrice"\]' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.11,31.291391' | python -m json.tool
     * @example shell curl --compressed 'http://api.iqianggou.com/items/s1?fields=\["sid","currentPrice","isBargained"\]' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.11,31.291391' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' | python -m json.tool
     */
    public function get($fields='')
    {
        $this->output['http']['X-Content-Type'] = 'json-object';
        preg_match('/^\/items\/(?P<sid>\w+)$/', $this->uri, $matches);
        $this->input['sids'] = json_encode(array($matches['sid']));
        $r = $this->index();
        if(!isset($r['data']['entities'][0])) {
            throw new \exception\Model(4044);
        }
        $this->output['data'] = $r['data']['entities'][0];

        return $this->output;
    }
    
    /**
     * 获取详情
     * @api GET /items/{sid}/detail
     * @return html
     * @example shell curl --compressed 'http://api.iqianggou.com/items/s1/detail' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.11,31.291391' | python -m json.tool
     */
    public function getDetail()
    {
        $this->output['http']['Content-Type'] = 'text/html';
        preg_match('/^\/items\/(?P<sid>\w+)\/detail$/', $this->uri, $matches);
        $this->output['data']['item'] = $this->itemModel->getDetail($matches['sid']);
        $this->output['data']['client'] = $this->client;
        $this->output['html'] = 'items/detail';

        return $this->output;
    }

    /**
     * 取评论列表
     *
     * 需要登录 ：否
     *
     * @api GET /items/:sid/comments/
     *
     * @param int $offset 必选。跳过多少条。
     *
     * @param int $limit 必选。取多少条。
     *
     * @return json-object {
        "entities":[
            {
                "addTime": 1351059897, 
                "content": "\u8fd8\u53ef\u4ee5", 
                "itemSid": 502500,
                "sid": "83",
                "orderSid": "", 
                "userSid": "60"
                "imgs": []
            },
            {
                "addTime": 1350413990, 
                "content": "\u8fd8\u4e0d\u9519\u54e6", 
                "itemSid": 502500, 
                "sid": "79",
                "orderSid": "",
                "userSid": "82"
                "imgs": [
                    "http://com-iqianggou-img-ugc.qiniudn.com/71f6950d988e75b9b68d1ffcc2d3b5da",
                    "http://com-iqianggou-img-ugc.qiniudn.com/b8e7128ccd943c0725aa05b9440030e0"
                ],
            }
        ],
        "total":123
      }
     * @example shell curl --compressed 'http://api.iqianggou.com/items/s502625/comments/?offset=0&limit=3' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     */
    public function getComments($offset='', $limit='')
    {
        $this->checkParams(array('offset', 'limit'));
        preg_match('/^\/items\/(?P<itemSid>\w+)\/comments\/$/', $this->uri, $matches);
        $m = new \model\Comment();
        $tmp = $this->input;
        $tmp['itemSid'] = $matches['itemSid'];
        $this->output['data'] = $m->getList($tmp);
        return $this->output;
    }
}
?>
