<?php
namespace controller;

/**
 * 用户登录、注册
 *
 *
 * 接口说明
 *
 * 按照REST规范，使用HTTP Method表示动作，GET 取、POST 增、PUT 改、DELETE 删。
 *
 * 认证使用OAuth2，为每个app分配一个id，就像微博等开放平台一样，登录后返回token
 *
 * 按照HTTP协议，使用HTTP Header传递一些数据，请求格式：
 *
 * Accept: application/json; version=trunk
 *
 * 表示请求调用trunk版本的API
 *
 * Accept-Language: zh-Hans 或者 Accept-Language: zh 或者 en 或者 en-US
 *
 * 表示期望的多国文字，比如zh表示汉字，en表示英文。app调用操作系统的方法，获得zh、zh-Hans、zh-cn、en、en-US等等都可以，直接传递给API即可。
 *
 * User-Agent: iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)
 *
 * User-Agent: iqianggou/1.2.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X)
 *
 * 表示app版本、OS、手机硬件型号
 *
 * Authorization: Bearer 1-1-1362468012-1365146412-f5a81c11b279a40e208717d9e8cc61c9
 *
 * 表示token，格式为Authorization: Bearer token。需要登录的每个请求都带着token。token过期时返回错误码：4011
 *
 * 不需要登录的接口，未登录时没token，带着appId，比如商品列表。当登录后，再请求所有接口，都应该带着token，不用再带appId。
 *
 * X-LngLat: 121.123456,33.123456
 *
 * 表示经纬度。
 *
 * 取数据的api有一个fields可选参数，如果不传，则返回所有字段的结果，如果传了，则只返回指定的字段。fields为json格式，比如["id","name","title"]。比如取商品列表，保存到手机本地，以后刷新时，只取价格即可。这样可以节省用户流量。
 *
 * 返回：
 *
 * Http Status Code 200 表示正常。400、403等等表示出错。
 *
 * X-Api-Status-Code: 2000 表示api状态码，2000为正常。4031等等表示错误。
 *
 * X-Api-Status-Msg: OK 表示api状态消息
 *
 * 格式为json，UTF-8编码。每个字段都是明确有用的，如果有不明白或多余字段，请及时联系，进行删除，这样可以节省用户流量。
 *
 * 为了商业保密，各个接口返回sid，即safe id，即加密后的id。
 *
 * 返回的数字一定是int或float。已知bug：int是稳定的，而float如果末尾是0，将被舍去，变成int。比如2.00会变成2。
 *
 * 返回的字段按照字母排序。
 */
class Auth extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }

    /**
     * 登录获得token。支持 手机号登录或 用户名登录。
     *
     * @api POST /auth/token
     *
     * @param int $appId 必填。Android为1，iPhone为2
     *
     * @param string $countryCallingCode 必填。国家电话区号。
     *
     * @param string $mobile 必填。手机号。
     *
     * @param string $username 可选，用户名。
     *
     * $param string $password 必填，密码。
     *
     * @return json-object {
        "token":"1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f"
        }
     * @example shell curl 'http://api.iqianggou.com/auth/token' -i -d 'appId=1' -d 'countryCallingCode=%2B86' -d 'mobile=15312159527' -d 'password=123456' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     */
    public function grantToken($appId=null, $countryCallingCode=null, $mobile=null, $username=null, $password=null)
    {
        $params = array(
            'appId',
            'password',
        );
        $this->checkParams($params);
        if(!isset($this->input['countryCallingCode']) && !isset($this->input['mobile']) && !isset($this->input['username'])) {
            throw new \exception\Model(4002);
        }
        $this->output['http']['X-Content-Type'] = 'json-object';
        $m = new \model\Auth();
        try {
            $this->output['data'] = $m->login($this->input);
        } catch(\exception\Model $e) {
            $tmp = $e->getData();
            $tmp['http']['X-Content-Type'] = 'json-object';
            throw new \exception\Http(substr($e->getCode(), 0, 3), '', $tmp);
        }
        return $this->output;
    }
}
?>
