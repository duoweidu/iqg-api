<?php
namespace controller;

/**
 * 广告。包含：splash启动广告、锁屏广告、音乐广告
 */
class Ad extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }

    /**
     * 取广告
     *
     * @param string $citySid 必填。表示城市。
     *
     * @param string $resolution 必填。表示分辨率，比如640x960。
     *
     * @api GET /ads/
     *
     * @return json-object {
            "splash" : {
                "endTime": "1388501940",
                "imgUri": "http://img-agc.iqianggou.com/19fd94c8fd397970a50087ca4232a77b"
            }
        }
     *
     * @example shell curl 'http://api.iqianggou.com/ads/?citySid=s10&resolution=640x960' -H 'Accept: adlication/json; version=trunk' -A 'Sugar/13.12.6.1111 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     */
    public function index($citySid='', $resolution='')
    {
        $this->checkParams(array('resolution'));
        $this->output['http']['X-Content-Type'] = 'json-object';
        
        $data = $this->input;
        $tmp = explode('x', $this->input['resolution']);
        $data['width'] = $tmp[0];
        $data['height'] = $tmp[1];

        $m = new \model\Ad();
        $r = $m->getSplash($data);
        $data = new \stdClass();
        if(!empty($r)) {
            $data = array(
                'imgUri' => $r['imgUri'],
                'endTime' => $r['endTime'],
            );
        }
        $this->output['data'] = array(
            'splash' => $data,
        );
        return $this->output;
    }
}
?>
