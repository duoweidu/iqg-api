<?php
namespace controller;

/**
 * 门店
 */
class Shop extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }

    /**
     * 取多个门店
     *
     * 需要登录 ：否
     *
     * header经纬度 ：可选。如果传了，则有距离。如果不传，则距离为null。
     *
     * @api GET /shops/?sids=xxx
     *
     * @param json-array $sids 必须。表示多个门店的sid。json格式，比如["s10002","s10000","s10005"]
     *
     * @param json-array $fields 可选。需要哪些字段，如果不传则取最大集。json格式，比如["name","sid"]
     *
     * @param float $lng 可选。自定义的经度。
     *
     * @param float $lat 可选。自定义的纬度。
     *
     * @return json-object {
        "entities":
            [
             {
                "address": "国权路561号",
                "citySid": "21", //所属城市编号，int
                "desc": "提供鸡腿、鸡排、鸡翅、鸡骨、鸡米花",
                "distance":1600, //距离（米）。int。
                "sid": "s10000",
                "businessHoursOpen": "10:00",
                "businessHoursClose": "18:00",
                "imgs": [
                    "http://img-agc.iqianggou.com/52b55a03dccc51bbc0322ba1457c6ebd"
                ],
                "lat": 31.296194, //纬度，float or int
                "lng": 121.502651, //经度，float or int
                "name": "某某鸡排（国权路店）",
                "tel": "021-55222810", //string
                "ecodeValidatePassword" : "1234", //string 验证密码
                "ecodeValidateType" : 1, //int 验证方式。123是网络电话纸质，4是密码
            },
            {
                "sid":"s10002",
                "name":"某某鸡排（政肃路店）",
                ......
            }
         ],
         "total":10
      }
     * @example shell curl 'http://api.iqianggou.com/shops/?sids=\["s10000","s10001","s10002"\]&lng=120&lat=30' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.498458,31.291391' | python -m json.tool
     * @example shell curl 'http://api.iqianggou.com/shops/?sids=\["s10000","s10001","s10002"\]&fields=\["sid","name"\]' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.498458,31.291391' | python -m json.tool
     */
    public function multiGet($sids=null, $fields=null, $lng=null, $lat=null)
    {
        $params = array(
            'sids',
        );
        $this->checkParams($params);
        $clientTmp = $this->client;
        //如果用户自定义位置，则看那里的商品。
        if(isset($this->input['lat']) && ($this->input['lat']!=='') && isset($this->input['lng']) && ($this->input['lng']!=='')) {
            $clientTmp['lat'] = $this->input['lat'];
            $clientTmp['lng'] = $this->input['lng'];
        }
        $m = new \model\Shop();
        $r = $m->multiGet(array_unique(json_decode($this->input['sids'], true)), $clientTmp);
        $data = array(
            'entities' => array_values($r),
            'total' => 1000, //暂时不需要。如果要做门店列表，才需要。
        );
        if(isset($this->input['fields'])) {
            $data['entities'] = array();
            $fields = json_decode($this->input['fields'], true);
            foreach($r as $sid=>$one) {
                $tmp = array();
                foreach($fields as $column) {
                    if(isset($one[$column])) {
                        $tmp[$column] = $one[$column];
                    }
                }
                $data['entities'][] = $tmp;
            }
        }
        $this->output['data'] = $data;
        return $this->output;
    }

    /**
     * 取1个门店（此接口已暂停）
     *
     * 接口 : GET /shops/:sid
     *
     * 需要登录 ：否
     *
     * 需要经纬度 ：是
     *
     * fields ： 可选。需要哪些字段，如果不传则取最大集。json格式，比如["name","sid"]
     *
     * @return json-object {
        "address": "国权路561号",
        "citySid": "21", //所属城市编号，int
        "desc": "提供鸡腿、鸡排、鸡翅、鸡骨、鸡米花，营业时间10:00至18:00",
        "sid": "s10000",
        "imgs": [
            "http://img-agc.iqianggou.com/52b55a03dccc51bbc0322ba1457c6ebd",
        ], 
        "lat": 31.296194, //纬度，float or int
        "lng": 121.502651, //经度，float or int
        "name": "某某鸡排（国权路店）",
        "tel": "021-55222810" //string
        }
     * @example shell curl 'http://api.iqianggou.com/shops/s10000' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.498458,31.291391' | python -m json.tool
     * @example shell curl 'http://api.iqianggou.com/shops/s10000?fields=\["sid","name"\]' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.498458,31.291391' | python -m json.tool
     */
    private function get()
    {
        $this->checkLngLat();
        preg_match('/^\/shops\/(?P<shopSid>\w+)$/', $this->uri, $matches);
        $m = new \model\Shop();
        $r = $m->multiGet(array($matches['shopSid']), $this->client);
        $one = array_values($r)[0];
        if(isset($this->input['fields'])) {
            $fields = json_decode($this->input['fields'], true);
            $data = array();
            foreach($fields as $column) {
                if(isset($one[$column])) {
                    $data[$column] = $one[$column];
                }
            }
        } else {
            $data = array(
                'address' => $one['address'],
                'citySid' => $one['citySid'],
                'desc' => $one['desc'],
                'distance' => intval($one['distance']),
                'sid' => $one['sid'],
                'imgs' => $one['imgs'],
                'lat' => floatval($one['lat']),
                'lng' => floatval($one['lng']),
                'name' => $one['name'],
                'tel' => strval($one['tel']),
            );
        }
        ksort($data);
        $this->output['data'] = $data;
        return $this->output;
    }
}
?>
