<?php
namespace controller;

class File extends Base
{
    private $fileModel;

    public function __construct($uri)
    {
        parent::__construct($uri);
        $this->checkLogin();
        $this->fileModel = new \model\File();
    }

    /**
     * 上传一个文件
     *
     * @api POST /files/
     *
     * @param string $itemSid 必选。
     *
     * 需要登录 ：是
     *
     * 需要经纬度 ：是
     *
     * @return json-object {
        "sid":"0938743774513615" //string
        "httpUri" : "http://com-iqianggou-img-ugc.qiniudn.com/71f6950d988e75b9b68d1ffcc2d3b5da",
        "httpsUri" : "https://dn-com-iqianggou-img-ugc.qbox.me/71f6950d988e75b9b68d1ffcc2d3b5da",
        "width" : 2560,
        "height" : 1600
     }
     * @example shell curl 'http://api.iqianggou.com/files/' -F "files=@/home/sinkcup/1.jpg;type=image/jpeg" -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     */
    public function upload()
    {
        if (!isset($_FILES['files']['tmp_name']) || empty($_FILES['files']['tmp_name'])) {
            throw new \exception\Http(403, '上传失败', array());
        }
        if (!in_array($_FILES['files']['type'], array('image/jpeg', 'image/png'))) {
            throw new \exception\Http(403, '上传失败，不允许上传此类型', array());
        }

        $data = $this->input;
        $data['uid'] = $this->client['userId'];
        $data['contentType'] = $_FILES['files']['type'];
        $data['filePath'] = $_FILES['files']['tmp_name'];
        $data['filename'] = $_FILES['files']['name'];
        $this->output['data'] = $this->fileModel->upload($data);

        return $this->output;
    }
}
