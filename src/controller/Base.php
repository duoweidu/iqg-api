<?php
namespace controller;
abstract class Base
{
    protected $client = array(
        'userId' => null,
        'lat' => null,
        'lng' => null,
        //'appId' => null,
        'ip'=> null,
        'lang' => null,
        'userAgent' => null,
    );
    protected $httpMethod;
    protected $input;
    protected $m;
    protected $output;
    protected $uri;
    
    protected function __construct($uri)
    {
        $this->uri  = $uri;
        $this->output = \PEAR2\Conf::get('http', 'output');
        $this->httpMethod = strtolower($_SERVER['REQUEST_METHOD']);
        switch($this->httpMethod) {//todo cookie
        case 'get' :
            $this->input = $_GET;
            break;
        case 'post' :
            $this->input = $_POST;
            break;
        case 'put' :
            parse_str(file_get_contents('php://input'), $this->input);
            break;
        }
        
        //apache：
        //$_SERVER['HTTP_AUTHORIZATION'] 不存在
        //文档：http://php.net/manual/en/features.http-auth.php
        //只能使用getallheaders()
        //$this->httpHeaders = \getallheaders();
        //然后用$this->httpHeaders['Authorization']) 和 $this->httpHeaders['X-LngLat'])
        //nginx：
        //getallheaders()不存在，只能用$_SERVER['HTTP_AUTHORIZATION']
        
        //尝试解析token，过期也没问题。
        if(isset($_SERVER['HTTP_AUTHORIZATION']) && !empty($_SERVER['HTTP_AUTHORIZATION'])) {
            $tmp = explode(' ', $_SERVER['HTTP_AUTHORIZATION']);
            if(isset($tmp[0]) && ($tmp[0] == 'Bearer') && isset($tmp[1]) && !empty($tmp[1])) {
                $token = $tmp[1];
                try {
                    $m = new \model\Auth();
                    $this->client = $m->verifyToken($token);
                } catch(\Exception $e) {
                }
            }
        }

        if(isset($_SERVER['HTTP_ORIGIN'])) {
            if(preg_match('/^http([s]*):\/\/(.+\.)*iqianggou\.com((:(\d+))*)$/', $_SERVER['HTTP_ORIGIN']) == 1) {
                $this->output['http']['Access-Control-Allow-Origin'] = $_SERVER['HTTP_ORIGIN'];
            }
        }

        if(isset($_SERVER['HTTP_X_LNGLAT'])) {
            $tmp = explode(',', $_SERVER['HTTP_X_LNGLAT']);
            $this->client['lng'] = $tmp[0];
            $this->client['lat'] = $tmp[1];
        }
 
        $this->client['ip'] = $_SERVER['REMOTE_ADDR'];
        $lang = 'en';
        if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && (stripos($_SERVER['HTTP_ACCEPT_LANGUAGE'], 'zh') === 0)) {
            $lang = 'zh';
        }
        $this->client['lang'] = $lang;
        $this->client['userAgent'] = isset($_SERVER['HTTP_USER_AGENT']) ? trim($_SERVER['HTTP_USER_AGENT']) : '';
        return true;
    }

    protected function checkParams($data)
    {
        foreach($data as $one) {
            if(!isset($this->input[$one])) {
                throw new \exception\Model(4002, $one);
            }
        }
        return true;
    }

    protected function checkEmpty($data)
    {
	foreach($data as $one) {
	    if( empty($this->input[$one]) ) {
		throw new \exception\Model(4001, $one);
	    }
	}
	return true;
    }

    protected function checkLogin()
    {
        if(!isset($_SERVER['HTTP_AUTHORIZATION'])) {
            throw new \exception\Model(4014);
        }
        $tmp = explode(' ', $_SERVER['HTTP_AUTHORIZATION']);
        if($tmp[0] != 'Bearer') {
            throw new \exception\Model(4014);
            //throw new \exception\Model(4014, '', $this->output); //todo 这时无法传output
            //throw new \exception\Http(401, '', $this->output);
        }
        if(empty($this->client['userId'])) {
            $token = $tmp[1];
            $m = new \model\Auth();
            $this->client = $m->verifyToken($token);
        }
        return true;
    }

    protected function checkLngLat()
    {
        if(!isset($_SERVER['HTTP_X_LNGLAT']) || empty($_SERVER['HTTP_X_LNGLAT'])) {
            throw new \exception\Model(4002, 'HTTP Header X-LngLat');
        }
        return true;
    }

    protected function checkMobileFormat($mobile)
    {
        if(preg_match('/^\d+$/', $mobile) == 1) {
            return true;
        }
        throw new \exception\Model(4001, '手机号格式错误');
    }

    protected function checkFullMobilesFormat($mobiles)
    {
        $tmp = $mobiles;
        if(!is_array($mobiles)) {
            $tmp = array($mobiles);
        }
        foreach($tmp as $mobile) {
            if(preg_match('/^\+\d+$/', $mobile) == 1) {
                return true;
            }
        }
        throw new \exception\Model(4001, '完整手机号格式错误');
    }
}
?>
