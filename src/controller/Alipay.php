<?php
namespace controller;

class Alipay extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }

    /**
     * 功能：服务器异步通知页面
     * 支付宝回调不支持添加header，所以应拷贝部署到enabled
     * @todo 未完成
     *
     * TRADE_FINISHED(表示交易已经成功结束)
     * @example shell curl 'http://api.iqianggou.com/alipay/notify' -i -d 'mobile=13371829527' -H 'Accept: application/json; version=trunk'
    * curl -i -d 'sign=nEkR1Odw0CCERdwyh0YYcUKzDJJApjtwoxylrj5toR1v4JW5N6L9HlobiqWH6tI2N8XnF3ZtDny5kAVO5CAexHivoeiNY5yGr3lOHDBN5VCOVTENYwe59+ycMLD6HLdQKsbqCSQMOjqHdR/Ndb3Yu0ZI1sCkH/j6uzyj4X2NuJ4=' -d 'notify_data=<notify><partner>2088801342252957</partner><discount>0.00</discount><payment_type>1</payment_type><subject>Mocha</subject><trade_no>2012082005756242</trade_no><buyer_email>larryleiy@126.com</buyer_email><gmt_create>2012-08-20 14:00:59</gmt_create><quantity>1</quantity><out_trade_no>1208200006135737</out_trade_no><seller_id>2088801342252957</seller_id><trade_status>TRADE_FINISHED</trade_status><is_total_fee_adjust>N</is_total_fee_adjust><total_fee>9.90</total_fee><gmt_payment>2012-08-20 14:01:00</gmt_payment><seller_email>leiyong@doweidu.com</seller_email><gmt_close>2012-08-20 14:01:00</gmt_close><price>9.90</price><buyer_id>2088002952843427</buyer_id><use_coupon>N</use_coupon></notify>' 'http://api.iqianggou.com/alipaynotify' -H 'Accept: application/json; version=trunk'
     */
    public function notify()
    {
        $this->output['http']['Content-Type'] = 'text/plain';
        $conf = \PEAR2\Conf::get('alipay', 'alipay');

        //获取notify_data，需要添加notify_data=
        //不需要解密，是明文的格式
        $notify_data = 'notify_data=' . $this->input["notify_data"];
        
        //获取sign签名
        $sign = $this->input["sign"];
        
        //验证签名
        $isVerify = \PEAR2\Service\Alipay::verify($notify_data, $sign);
        
        //如果验签没有通过
        if(!$isVerify){
            $this->output['data'] = 'fail';
            return $this->output;
        }
        
        //获取交易状态
        $trade_status = \PEAR2\Service\Alipay::getDataForXML($this->input['notify_data'] , '/notify/trade_status');
        
        //判断交易是否完成
        if($trade_status == "TRADE_FINISHED"){
            $this->ouput['data'] = "success";
            
            //在此处添加您的业务逻辑，作为收到支付宝交易完成的依据
            //todo
            
        }
        else{
            $this->output['data'] = "fail";
        }
        return $this->output;
    }
}
?>
