<?php
namespace controller;

/**
 * 邀请别人注册
 */
class Invitation extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }

    /**
     * 取自己的邀请记录
     *
     * 需要登录 ： 是
     *
     * @api GET /invitations/
     *
     * @param int $offset 必填。
     *
     * @param int $limit 必填。
     *
     * @param int $isAccepted 可选。
     *
     * @return json-object {
        "entities": [
            {
                "addTime": 1398137455 //邀请时间。int
                "countryCallingCode":"+86", //国家电话区号。string
                "mobile":"13800138000" //手机号。string。
                "isAccepted":true //是否已接受邀请来注册了。
            },
            {
                "addTime": 1398137455 //邀请时间。int
                "countryCallingCode":"+86", //国家电话区号。string
                "mobile":"13800138001" //手机号。string。
                "isAccepted":false //是否已接受邀请来注册了。
            }
        ],
        "total": 50
       }
     * @example shell curl 'http://api.iqianggou.com/invitations/?offset=0&limit=10' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     */
    public function index($offset=null, $limit=null, $isAccepted=null)
    {
        $this->checkLogin();
        $m = new \model\Invitation();
        $tmp = $this->input;
        $tmp['userId'] = $this->client['userId'];
        $r = $m->getHistory($tmp);
        $this->output['data'] = array(
            'entities' => array_values($r['entities']),
            'total' => intval($r['total']),
        );
        return $this->output;
    }

    /**
     * 发送一条邀请
     *
     * 需要登录 ： 是
     *
     * @api POST /invitations/
     *
     * @param string $fullMobiles 必填。带国家区号的完整手机号，如果不带国家区号，则以邀请人为准。json-array格式，比如 [13800138000, 13800138001]
     *
     * @param string $nickname 选填。邀请人的昵称。
     *
     * @return json-array []
     *
     * @example shell curl -d 'nickname=Jim' -d 'fullMobiles=["%2B8615312159527","%2B8613671671796"]' 'http://api.dev.cn.iqianggou.com:8183/invitations/' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'  -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f'  -H 'Accept-Language: en-US' | python -m json.tool
     */
    public function add($fullMobiles='', $nickname='')
    {
        $params = array(
            'fullMobiles',
        );
        $this->checkParams($params);
        $fullMobiles = json_decode($this->input['fullMobiles'], true);

        $m = new \model\Invitation();
        $tmp = array_merge($this->input, $this->client);
        $tmp['fullMobiles'] = $fullMobiles;
        $m->add($tmp);
        return $this->output;
    }
}
?>
