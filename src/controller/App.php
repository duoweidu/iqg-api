<?php
namespace controller;

/**
 * app版本升级等等
 */
class App extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }

    /**
     * 取最新版本。如果用户已经是最新版本，则返回空。
     *
     * 需要登录 ：否
     *
     * @param int $id 必填，写在URI里。app的id，安卓手机版是1, iPhone版是2, iPhone越狱版是3
     *
     * @param string $stability 必填，写在URI里。表示稳定性。开发版为dev，发布正式版打包时改成stable
     *
     * @api GET /apps/:id/:stability/latest
     *
     * @return json-object {
            "changeLog": "hello update",
            "downloadUri": "http://com-iqianggou-intra-dev-dl.qiniudn.com/iqianggou-bd0e538cba5d5fdd46f6ce75d275a061.apk",
            "isCompulsory": false,
            "versionName": "2.0.1"
        }
     *
     * @example shell curl 'http://api.iqianggou.com/apps/1/dev/latest' -H 'Accept: application/json; version=trunk' -A 'Sugar/13.12.6.1111 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     * @example shell curl 'http://api.iqianggou.com/apps/2/stable/latest' -H 'Accept: application/json; version=trunk' -A 'Sugar/13.12.6.1110  (iPhone; CPU iPhone OS 6_1 like Mac OS X)' | python -m json.tool
     */
    public function getLatest($id='', $stability='')
    {
        $this->output['http']['X-Content-Type'] = 'json-object';
        preg_match('/^\/apps\/(?P<id>\d+)\/(?P<stability>[a-z]+)\/latest$/', $this->uri, $matches);
        $id = $matches['id'];
        $stability = $matches['stability'];

        $userAgent = \PEAR2\Conf::get('app', 'userAgent');
        //preg_match('/^' . $userAgent . '\/(?P<versionName>(\d+\.)+\d+) .*$/', $_SERVER['HTTP_USER_AGENT'], $matches); //php原生的get_browser需要browscap.ini，文件冗余，每个版本都要录入，无法自动检测版本号。所以正则解析更方便。
        preg_match('/^[a-zA-z]+\/(?P<versionName>(\d+\.)+\d+) .*$/', $_SERVER['HTTP_USER_AGENT'], $matches); //php原生的get_browser需要browscap.ini，文件冗余，每个版本都要录入，无法自动检测版本号。所以正则解析更方便。
        if(!isset($matches['versionName'])) {
            try {
                throw new \exception\Model(4012);
            } catch (\exception\Model $e) {
                $tmp = $e->getData();
                $tmp['http']['X-Content-Type'] = 'json-object';
                throw new \exception\Http(substr($e->getCode(), 0, 3), '', $tmp);
            }
        }
        $versionName = $matches['versionName'];

        $m = new \model\App();
        $r = $m->getLatest($id, $stability);
        $data = new \stdClass();
        if(!empty($r) && strnatcmp($versionName, $r['versionName']) <0) {
            $data = array(
                'changeLog' => $r['changeLog'],
                'downloadUri' => $r['downloadUri'],
                'isCompulsory' => $r['isCompulsory'] == 1 ? true : false,
                'versionName' => $r['versionName'],
            );
        }
        $this->output['data'] = $data;
        return $this->output;
    }
}
?>
