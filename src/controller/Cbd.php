<?php
namespace controller;

/**
 * 商圈
 */
class Cbd extends Base
{
    private $cbdModel;

    public function __construct($uri)
    {
        parent::__construct($uri);
        $this->cbdModel = new \model\Cbd();
    }

    /**
     * 取商品列表 或者 指定ids取几个商品
     *
     * 需要登录 ：否
     *
     * 需要经纬度 ：否
     *
     * @api GET /cbds/
     *
     * @param int $offset 必须。跳过多少条。
     *
     * @param int $limit 必须。取多少条。
     *
     * @param string $citySid 必须。城市sid。
     *
     * @param json-array $fields 可选。需要哪些字段，如果不传则取最大集。json格式，比如["name","sid"]
     *
     * @return json-object {
         "entities":[
             {
                "sid":"s1", //加密后的ID。string。

                "name":"五角场",

                "lat": 131.012,

                "lng": 33.456,
            },
            {
                //等等字段，同上。
            }
         ],
         "total":23
     }
     * @example shell curl --compressed 'http://api.iqianggou.com/cbds/?citySid=s10&offset=0&limit=3' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.11,31.291391' | python -m json.tool
     * @example shell curl --compressed 'http://api.iqianggou.com/cbds/?citySid=10&fields=\["name","sid"\]&offset=0&limit=3' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'X-LngLat: 121.498458,31.291391' | python -m json.tool
     */
    public function index($offset='', $limit='', $fields='', $categorySid='', $citySid='', $sids='')
    {
        $params = array(
            "offset",
            "limit",
        );
        $this->checkParams($params);

        $m = new \model\Cbd();
        $r = $m->getList(array_merge($this->input, $this->client));
        $data = array(
            'entities' => array_values($r['entities']),
            'total' => intval($r['total']),
        );
        if(empty($r['entities'])) {
            $this->output['data'] = $data;
            return $this->output;
        }
        if(isset($this->input['fields'])) {
            $fields = json_decode($this->input['fields'], true);
            $data['entities'] = array();
            foreach($r['entities'] as $sid=>$one) {
                $tmp = array();
                foreach($fields as $column) {
                    if(isset($one[$column])) {
                        $tmp[$column] = $one[$column];
                    }
                }
                $data['entities'][] = $tmp;
            }
        }
        
        $this->output['data'] = $data;
        return $this->output;
    }
}
?>
