<?php
namespace controller;

/**
 *  账单 
 */
class Bill extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }

    /**
     * 取我最近30天的账单。按时间倒排。
     *
     * 需要登录 ：是
     *
     * @api GET /bills/30days
     *
     * @return json-object {
        "entities":[
            {
                "addTime": 1381225277,
                "transactionAmount": -14600,
                "remark": "\u4e0b\u5355"
            },
            {
                "addTime": 1381301704,
                "transactionAmount": -5800, 交易金额，int
                "remark": "\u4e0b\u5355"
            }
         ],
         "total":300
        }
     *
     * @example shell curl 'http://api.iqianggou.com/bills/30days' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' | python -m json.tool
     */
    public function index()
    {
        $this->checkLogin();
        $m = new \model\Bill();
        preg_match('/^\/bills\/(?P<range>\w+)$/', $this->uri, $matches);
        switch($matches['range']) {
	    case '30days' :
	    default :
		$startTime = time() - 30*86400;
		break;
        }
        $r = $m->getList($this->client['userId'], $startTime);
        $data = array(
            'entities' => array(),
            'total' => intval($r['total']),
        );
        foreach($r['entities'] as $one) {
            $tmp = array(
                'addTime' => intval($one['addTime']),
                'transactionAmount'   => \intval(\bcmul($one['money'], 100, 2)),
                'remark'  => _($one['remark']),
            );
            $data['entities'][] = $tmp;
        }

        $this->output['data'] = $data;
        return $this->output;
    }
}
?>
