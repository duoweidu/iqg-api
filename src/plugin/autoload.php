<?php
ini_set('include_path', dirname(__FILE__) . '/../lib/php');
function autoload($class_name)
{
    $this_dir = dirname(__FILE__) . '/';
    if(strpos($class_name, '\\') !== false) {
        $path = str_replace('\\', '/', $class_name) . '.php';
        if(strpos($class_name, 'PEAR2') === 0) {
            $path = $this_dir . '/../lib/php/' . $path;
        } else if (strpos($class_name, 'controller') === 0 || strpos($class_name, 'model') === 0 || strpos($class_name, 'exception') === 0) {
            $path = $this_dir . '/../' . $path;
        }
        require_once $path;
    } else if(strpos($class_name, '_') !== false) {
        $path = str_replace('_', '/', $class_name) . '.php';
        $path = $this_dir . '/../lib/php/' . $path;
        require_once $path;
    }
    return true;
}
spl_autoload_register('autoload');
include_once __DIR__ . '/../vendor/autoload.php';
?>
