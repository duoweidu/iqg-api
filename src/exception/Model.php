<?php
namespace exception;
class Model extends \Exception
{
    private $data = array();
    public function __construct($code, $message='', $data=array())
    {
        $org = \PEAR2\Conf::get('http', 'output');
        $this->data = $org;
        if(!empty($data)) {
            $this->data['data'] = array_merge($org['data'], $data);
        }
 
        $this->data['http']['X-Api-Status-Code'] = $code;
        $map = \PEAR2\Conf::get('status_code_msg', 'api');
        if(!empty($map[$code])) {
            $tmp = _($map[$code]);
            if(strpos($tmp, '{') !== false) {
                $tmp2 = str_replace(array_keys($data), array_values($data), $tmp);
                $this->data['http']['X-Api-Status-Msg'] = str_replace(array('{', '}'), array('', ''), $tmp2);
            } else {
                $this->data['http']['X-Api-Status-Msg'] = $tmp;
            }
        }
        if(!empty($message)) {
            $this->data['http']['X-Api-Status-Msg'] = _($message);
        }
        parent::__construct($message, $code);
    }

    public function getData()
    {
        return $this->data;
    }
}
?>
