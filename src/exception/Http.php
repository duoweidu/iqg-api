<?php
namespace exception;
class Http extends \Exception
{
    private $data;
    public function __construct($code, $message='', $data=array())
    {
        $org = \PEAR2\Conf::get('http', 'output');
        if(empty($data)) {
            $this->data = $org;
            $this->data['tpl'] = $code; //todo json 怎么办
        } else {
            $this->data = array_merge($org, $data);
        }
        if($this->data['http']['X-Content-Type'] == 'json-object' && !is_object($this->data['data'])) {
            $this->data['data'] = new \stdClass();
        } else if($this->data['http']['X-Content-Type'] == 'json-array' && !is_array($this->data['data'])) {
            $this->data['data'] = array();
        }
        $this->data['http']['Status-Code'] = $code;
        parent::__construct($message, $code);
    }

    public function getData()
    {
        return $this->data;
    }
}
?>
