delimiter $
CREATE TRIGGER update_order_status BEFORE UPDATE ON murcielago_order
FOR EACH ROW
    BEGIN
        IF NEW.useStatus = 1 THEN
            set NEW.orderStauts = 2;
        ELSE
            SET NEW.orderStauts = NEW.payStatus;
        END IF;
    END;
$
delimiter ;
