<?php
namespace PEAR2\Service;
class SmoovPay{

    public function __construct( $conf ){
	$this->conf = $conf;
    }

    public function sign($params){
   	$dataToBeHashed = $this->conf['secret_key']
                . $params['merchant']
                . $params['action']
                . $params['ref_id']
                . $params['total_amount']
                . $params['currency'];

	$utfString = mb_convert_encoding($dataToBeHashed, "UTF-8");
	$signature = sha1($utfString, false); 
	return $signature;
    }


    public function getSmoovPayUrl($params)
    {
	$params = array_merge($params,$this->conf, array('action'=> "2.0", 'total_amount'=>$params['item_amount_1'] ));
	$data = array(
	    "version"		 => "2.0",
	    "action"		 => "pay",
	    "merchant"		 => $params['merchant'], 
	    "ref_id"		 => $params['ref_id'],
	    "item_name_1"        => $params['item_name_1'],
	    "item_description_1" => $params['item_description_1'],
	    "item_quantity_1"    => $params['item_quantity_1'],
	    "item_amount_1"	 => $params['item_amount_1'],
	    "currency"           => $params['currency'],
	    "total_amount"       => $params['item_amount_1'],
	    "success_url"        => $params['success_url'],
	    "cancel_url"         => $params['cancel_url'],
	    "str_url"            => $params['str_url'],
	    "signature"            => $this->sign($params),
	    "signature_algorithm"  => 'sha1',
	);
	    
	$requestData = $this->createLinkstring($data);

	$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->conf['api_url']);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
        $result = curl_exec($ch);
        curl_close($ch);

	$result = json_decode($result, true);

	if ( $result['status'] == 1) {
	    return $result['redirect_url'];
	} else {
	    return false;
	}
    }

    public function isPaidSuccess($params){
	$data = array(
	    'secret_key'	    => $this->conf['secret_key'], 
	    'merchant'		    => $params['merchant'],
	    'ref_id'		    => $params['ref_id'],
	    'reference_code'	    => $params['reference_code'], 
	    'response_code'	    => $params['response_code'], 
	    'currency'		    => $params['currency'],
	    'total_amount'	    => $params['total_amount'],
	    'signature'		    => $params['signature'],
	    'signature_algorithm'   => $params['signature_algorithm'],
 	);
	if( $this->checkSign($data) ) {
	   return $params['ref_id']; 
	}else{
	    return false;
	}
    }

    public function checkSign($params)
    {
	$dataToBeHashed = $this->conf['secret_key']
                . $params['merchant']
                . $params['ref_id']
                . $params['reference_code']
                . $params['response_code']
		. $params['currency']
		. $params['total_amount'];

	$utfString = mb_convert_encoding($dataToBeHashed, "UTF-8");	
	$signature = sha1($utfString, false);	
	return ($params['signature'] == $signature) ? true :false;
    }



    public function createLinkstring ($array, $urlencode = 0)
    {
        $arg = "";
        while (list ($key, $val) = each($array)) {
            if( $urlencode == 1){
                $arg .= $key . "=" . urlencode($val) . "&";
            }else{
                $arg .= $key . "=" . $val . "&";
            }
        }
        $arg = substr($arg, 0, count($arg) - 2);
        return $arg;
    }

}
