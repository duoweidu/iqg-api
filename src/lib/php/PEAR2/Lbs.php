<?php
namespace PEAR2;
class Lbs
{
    private function __construct()
    {
    }
   
    public static function calculateDistance($point1, $point2)
    {
        if(is_null($point1['lat']) || is_null($point1['lng']) || is_null($point2['lat']) || is_null($point2['lng'])) {
            return null;
        }
        $earthRadius = 3958.75;

        $dLat = deg2rad($point2['lat']-$point1['lat']);
        $dLng = deg2rad($point2['lng']-$point1['lng']);


        $a = sin($dLat/2) * sin($dLat/2) +
           cos(deg2rad($point1['lat'])) * cos(deg2rad($point2['lat'])) *
           sin($dLng/2) * sin($dLng/2);
        $c = 2 * atan2(sqrt($a), sqrt(1-$a));
        $dist = $earthRadius * $c;

        // from miles
        $meterConversion = 1609;
        $geopointDistance = $dist * $meterConversion;

        return intval($geopointDistance);
    }
}
?>
