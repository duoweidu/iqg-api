#/bin/bash
help="Usage: $0 -p project -v version"
if [ -z $1 ]
then
    echo $help
    exit 1
fi

while getopts p:v:h opt
do
    case $opt in
    "p" | "v")
        eval $opt=$OPTARG
        ;;
    *)
        echo $help
        exit
        ;;
    esac
done

for i in "p" "v"; do
    eval tmp="\$$i"
    if [ -z $tmp ]
    then
        echo need -$i
        exit 1
    fi
done

SVN_DIR='svn://dwd.8800.org/php/'$p'/tags/'$v;
TMP_DIR=~/tmp/$p/tags/$v
mkdir -p $TMP_DIR
rm -rf $TMP_DIR
svn co -q --username release --password @dwd2810 $SVN_DIR $TMP_DIR
cd $TMP_DIR/src
make env=prod server=http1
make release env=prod server=http1
make env=prod server=http2
make release env=prod server=http2
exit
