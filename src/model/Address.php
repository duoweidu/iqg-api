<?php
namespace model;

/**
 * @todo 一个人最多收藏多少个地址？像淘宝购物车是50个
 */
class Address extends Base
{
    private $userAddressDb;

    public function __construct()
    {
        $this->userAddressDb = new \model\dao\db\iqg2012\MurcielagoUserAddress();
    }

    /**
     * 添加一个地址收藏
     * 如果重复添加，则先删除，再添加
     * @todo 用replace好点。
     */
    public function add($userId, $data)
    {
        $where = array(
            'e' => array(
                'address' => $data['address'],
                'uid' => $userId,
            ),
        );
        $cnt = $this->userAddressDb->selectCount($where);
        //同一地址，不能重复添加。现在按照地址判断，而不是经纬度。因为挪几米经纬度就不一样了，但地址一样。
        if($cnt > 0) {
            throw new \exception\Model(40312);
        }

        $tmp = $data;
        $tmp['uid'] = $userId;
        $tmp['addIp'] = $data['ip'];
        $tmp['addTime'] = time();
        $r = $this->userAddressDb->insertRow($tmp, $where);
        return array(
            'sid' => self::encryptId($r),
        );
    }

    public function delete($input)
    {
        $where = array(
            'e' => array(
                'addressId' => self::decryptSid($input['sid']),
                'uid' => $input['userId'],
            ),
        );
        return $this->userAddressDb->delete($where);
    }

    public function getRowsByUserId($userId)
    {
        $where = array(
            'e' => array(
                'uid' => $userId,
            ),
        );
        $column = array(
            'addressId',
            'address',
            'lng',
            'lat',
        );
        $orderBy = 'addTime DESC';
        $r = $this->userAddressDb->selectRows($where, $column, '', '', '', $orderBy);
        $data = array();
        foreach($r as $one) {
            $tmp = array(
                'sid' => strval(self::encryptId($one['addressId'])),
                'address' => $one['address'],
                'lat' => floatval($one['lat']),
                'lng' => floatval($one['lng']),
            );
            $data[$tmp['sid']] = $tmp;
        }
        return $data;
    }
}
?>
