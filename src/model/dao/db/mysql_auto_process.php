<?php
/**
 * mysqldump -hlocalhost -uroot -p1 -d test >mysql_schema.sql
 * php mysql_auto_process.php
 * @author sink
 */
//todo datatime timestamp update

if(!isset($argv[1])) {
    exit('please input db name' . "\n");
}
$dbFakeName = $argv[1];
$thisDir = dirname(__FILE__) . '/';
require $thisDir . '../../../conf/db.conf';
$dbConf = $db[$dbFakeName];
$dbName = $db[$dbFakeName]['dbName'];
$dsn = 'mysql:dbname=' . $dbConf['dbName'] . ';host=' . $dbConf['host'] . ';port=' . $dbConf['port'] . ';'; //mysql
$user = $dbConf['username'];
$password = $dbConf['password'];
$conn = new PDO($dsn, $user, $password);
$sql = 'show tables';
$stmt = $conn->query($sql);
$stmt->setFetchMode(PDO::FETCH_NUM);
$tables = $stmt->fetchAll();
$schema = '';
foreach($tables as $v) {
    $stmt = $conn->query('show create table `' . $v[0] . '`');
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    $tmp = $stmt->fetchAll();
    $schema .= $tmp[0]['Create Table'] . ';' . "\n\n";
}
@mkdir($thisDir . $dbFakeName);
file_put_contents($thisDir . $dbFakeName . '/mysql_schema.sql', $schema);

function firstToUpper($s)
{
    $first = substr($s,0,1);
    $left = substr($s,1);
    return strtoupper($first).$left;
}
function tableNameToClassName($s)
{
    $tmp = explode('_',$s);
    $result = '';
    foreach($tmp as $value)
    {
        $result .= firstToUpper(strtolower($value)); //阿里云会把数据库名转小写。为了避免其他服务器的大写冲突，所以代码里全用小写。
    }
    return $result;
}
function underlineToHump($s)
{
    $tmp = explode('_',$s);
    $result = $tmp[0];
    unset($tmp[0]);
    foreach($tmp as $value)
    {
        $result .= firstToUpper($value);
    }
    return $result;
}
function getInsertRowFunction($one_line,$column_name)
{
    $result = '';
    if(stripos($one_line,'binary(')!==false)
    {
        $result .= "        ".'if(isset($data[\''.$column_name.'\'])) {'
        ."\n            ".'$data_for_query[\''.$column_name.'\'] = $data[\''.$column_name.'\'];'
        ."\n        }\n";
    }
    else
    {
        $result .= "        ".'if(isset($data[\''.$column_name.'\'])) {'
        ."\n            ".'$data_for_query[\''.$column_name.'\'] = trim($data[\''.$column_name.'\']);'
        ."\n        }\n";
    }
    if((stripos($one_line,'AUTO_INCREMENT')===false))
    {
        if((stripos($one_line,'NOT NULL')!==false)&&(stripos($one_line,' DEFAULT ')==false))
        {
            $result .= "        ".'else'
            ." {"
            ."\n            ".'return false;'
            ."\n        }\n";
        }
    }
    return $result;
}
function getInsertRowsFunction($one_line,$column_name)
{
    $result = '';
    if(stripos($one_line,'binary(')!==false) {
        $result .= "            ".'if(isset($value[\''.$column_name.'\']))'
        ." {"
        ."\n                ".'$tmp_data[\''.$column_name.'\'] = $value[\''.$column_name.'\'];'
        ."\n            }\n";
    } else {
        $result .= "            ".'if(isset($value[\''.$column_name.'\']))'
        ." {"
        ."\n                ".'$tmp_data[\''.$column_name.'\'] = trim($value[\''.$column_name.'\']);'
        ."\n            }\n";
    }
    if((stripos($one_line,'AUTO_INCREMENT')===false))
    {
        if((stripos($one_line,'NOT NULL')!==false)&&(stripos($one_line,' DEFAULT ')==false))
        {
            $result .= "            ".'else {'
            ."\n                ".'return false;'
            ."\n            }\n";
        }
    }
    return $result;
}
function getUpdateFunction($one_line, $column_name)
{
    $result = '';
    if((stripos($one_line,'char(')!==false)||(stripos($one_line,'` text')!==false))
    {
        if((stripos($one_line,'NOT NULL')!==false))
        {
            $result .= "        ".'if(isset($data[\''.$column_name.'\']) && trim($data[\''.$column_name.'\'])!=\'\')';
        }
        else
        {
            $result .= "        ".'if(isset($data[\''.$column_name.'\']))';
        }
        $result .= " {"
        ."\n            ".'$data_for_query[\''.$column_name.'\'] = trim($data[\''.$column_name.'\']);'
        ."\n        }\n";
    }
    else
    {
        if((stripos($one_line,'int(')!==false)||(stripos($one_line,'float(')!==false))
        {
            $result .= "        ".'if(isset($data[\''.$column_name.'\'])&&is_numeric(trim($data[\''.$column_name.'\'])))';
            $result .= ' {' . "\n            ".'$data_for_query[\''.$column_name.'\'] = trim($data[\''.$column_name.'\']);'
            ."\n        }\n";
        } else {
            if((stripos($one_line,'binary(')!==false)) {
                if(stripos($one_line,'NOT NULL')!==false) {
                    $result .= "        ".'if(isset($data[\''.$column_name.'\'])&&!empty($data[\''.$column_name.'\']))';
                }
            }
            $result .= "        ".'if(isset($data[\''.$column_name.'\']))';
            $result .= " {"
            ."\n            ".'$data_for_query[\''.$column_name.'\'] = trim($data[\''.$column_name.'\']);'
            ."\n        }\n";
        }
    }
    return $result;
}
function convertComment($one_line,$column_name)
{
    $result = '';
    return $result;
    $end_pos = strripos($one_line,'`');
    $new = substr($one_line,$end_pos+1);
    if((stripos($new,'COMMENT')!==false))
    {
        $arr = explode('COMMENT',$new);
        $content = trim($arr[1]);
        $content = str_replace(array('\'',','),'',$content);
        $arr_2 = explode(' ',$content);
        $result .= "    ".'public static $'.$column_name.' = array(';

        for($i=1,$l=count($arr_2);$i<$l;$i=$i+2)
        {
            $result .= "\n        ".'\''.$arr_2[$i].'\' => \''.$arr_2[$i-1].'\'';
            if($i+2<$l)
            {
                $result .= ',';
            }
        }
        $result .= "\n    ".');'."\n";
    }
    return $result;
}
$tmp = explode("\n", $schema);
foreach($tmp as $one_line) {
    if(stripos($one_line,'CREATE TABLE')!==false)
    {
        $start_pos = stripos($one_line,'`');
        $end_pos = strripos($one_line,'`');
        $l = $end_pos - $start_pos - 1;
        $tableName = substr($one_line,$start_pos+1,$l);
        $class_name = tableNameToClassName($tableName);
        $file_name = $thisDir . $dbFakeName . '/' . $class_name.'.php';
        $dao_file_content = '<?php'
        ."\n".'namespace model\dao\db\\' . $dbFakeName . ';' . "\n"
        ."\n".'class '.$class_name.' extends \model\dao\db\Base' . "\n" . '{'
        ."\n    ".'protected $dbFakeName = \''.$dbFakeName.'\';'."\n"
        ."\n    ".'protected $tableName = \''.$tableName.'\';'."\n";
        echo $file_name."\n";
        $comment_result = '';
        $insert_row_function = "    ".'public function insertRow($data)' . "\n    " . '{'."\n";
        $insert_rows_function = "    ".'public function insertRows($data)' . "\n    " . '{'
        ."\n        ".'$column_name_array = array();'
        ."\n        ".'$data_for_query = array();'
        ."\n        ".'foreach($data as $value) {'
        ."\n            ".'$tmp_data = array();'."\n";
        //$select_rows_function = '';//继承
        //$select_row_function = '';//继承
        //$select_count_function = '';//继承
        $update_function = "    ".'public function update($data, $where=\'\')
    {' . "\n";
        //$delete_function = '';//继承
        continue;
    }
    else
    {
        if(stripos(trim($one_line),'`')===0)
        {
            $start_pos = stripos($one_line,'`');
            $end_pos = strripos($one_line,'`');
            $l = $end_pos - $start_pos - 1;
            $column_name = substr($one_line,$start_pos+1,$l);
            $comment_result .= convertComment($one_line,$column_name);
            $insert_row_function .= getInsertRowFunction($one_line,$column_name);
            $insert_rows_function .= getInsertRowsFunction($one_line,$column_name);
            $update_function .= getUpdateFunction($one_line,$column_name);
        }
        if(stripos(trim($one_line), ')')===0)
        {
            $insert_row_function .= '        $sql = $this->sql->insertRow($data_for_query);
        return $this->db->insertRow($sql);' ."\n" . '    }'."\n";
            $insert_rows_function .=
            "            ".'$data_for_query[] = $tmp_data;'
            ."\n        ".'}'
            ."\n".'        $sql = $this->sql->insertRows($data_for_query);
        return $this->db->insertRows($sql);'
            ."\n    ".'}'."\n";
            $update_function .= '        if(empty($data_for_query)) {
            return true;;
        }' . "\n";
            $update_function .= '        $sql = $this->sql->update($data_for_query, $where);
        return $this->db->update($sql);'
            ."\n    ".'}'."\n";
            $dao_file_content .= $comment_result."    \n".$insert_row_function."    \n".$insert_rows_function."    \n".$update_function.'}'."\n".'?>';
            file_put_contents($file_name, $dao_file_content);
            //break;
        }

    }
}
?>
