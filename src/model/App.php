<?php
namespace model;

/**
 * app
 */
class App extends Base
{
    private $appVersionDb;

    public function __construct()
    {
        $this->appVersionDb = new \model\dao\db\iqg2013\AppVersion();
    }

    public function getLatest($id, $stability)
    {
        $where = array(
            'e' => array(
                'appId' => $id,
                'stability' => $stability,
            ),
        );
        $column = array(
            'versionName',
            'downloadUri',
            'changeLog',
            'isCompulsory',
        );
        return $this->appVersionDb->selectRow($where, $column, '', '', 'versionCode DESC');
    }
}
?>
