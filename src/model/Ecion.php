<?php
/**
 * 金币
 */
namespace model;
class Ecion extends Base
{
    private $userEcionDb;
    private $ecionChangeDb;
    private $conf;

    public static $types = array(
        'login'   => 1,
        'invite'  => 2,
        'comment' => 3,
        'exchange' => 4,
    );

    public function __construct()
    {
        $this->userEcionDb = new \model\dao\db\iqg2012\MurcielagoUserEcion();
        $this->ecionChangeDb = new \model\dao\db\iqg2012\MurcielagoEcionChange();
        $confDb = new \model\dao\db\iqg2013\Conf();
        $where = array(
            'e' => array(
                'name' => array('ecion', 'reward'),
            )
        );
        $r = $confDb->selectRows($where);
        foreach($r as $one) {
            $this->conf[$one['name']] = json_decode($one['value'], true);
        }
    }

    public function getStatus($input)
    {
        $where = array(
            'e' => array(
                'uid' => $input['userId'],
            ),
        );
        $r = $this->userEcionDb->selectRow($where);

        if(empty($r)) {
            return array(
                'accumulativeTotal' => 0,
                'countComment' => 0,
                'countInvite' => 0,
                'countLogin' => 0,
                'ranking' => 0,
                'rankingPercent' => 0,
                'total' => 0,
            );
        }

        return array(
            'accumulativeTotal' => intval($r['accumulativeTotal']),
            'countComment' => intval($r['countComment']),
            'countInvite' => intval($r['countInvite']),
            'countLogin' => intval($r['countLogin']),
            'ranking' => intval($r['ranking']),
            'rankingPercent' => floatval($r['rankingPercent']),
            'total' => intval($r['total']),
        );
    }

    public function change($input)
    {
        if($input['transactionAmount'] < 0) {
            //减金币
            $sql = 'UPDATE murcielago_user_ecion SET total=total' . intval($input['transactionAmount']) . ' WHERE uid=' . $input['userId'] . ' LIMIT 1';
        } else {
            //加金币
            $sql = 'UPDATE murcielago_user_ecion SET accumulativeTotal=accumulativeTotal+' . intval($input['transactionAmount']) . ', total=total+' . intval($input['transactionAmount']) . ' WHERE uid=' . $input['userId'] . ' LIMIT 1';
        }
        $this->userEcionDb->db->update($sql);

        //记账
        $tmp = array(
            'uid' => $input['userId'],
            'transactionAmount' => $input['transactionAmount'],
            'addTime' => time(),
            'type' => self::$types[$input['type']],
        );
        if(isset($input['remark'])) {
            $tmp['remark'] = $input['remark'];
        }
        $r = $this->ecionChangeDb->insertRow($tmp);
        if(empty($r)) {
            throw new \exception\Model(5000);
        }
        return true;
    }

    /**
     * 用金币换余额
     */
    public function exchange($input)
    {
        if(!isset($this->conf['ecion']['exchange_rate']) || !isset($this->conf['ecion']['trading_unit'])) {
            throw new \exception\Model(5000);
        }

        if($input['subtotal'] < $this->conf['ecion']['trading_unit']) {
            throw new \exception\Model(40315, null, array(
                'trading_unit' => $this->conf['ecion']['trading_unit'],
            ));
        }
        
        $where = array(
            'e' => array(
                'uid' => $input['userId'],
            ),
            'gte' => array(
                'total' => $input['subtotal'],
            ),
        );
        $cnt = $this->userEcionDb->selectCount($where);
        if($cnt == 0) {
            throw new \exception\Model(4030, _('金币不足'));
        }

        $transactionAmount = bcmul(bcdiv($input['subtotal'], $this->conf['ecion']['trading_unit'], 0), $this->conf['ecion']['trading_unit'], 0); //这次消耗了多少金币，必须为 交易单元的整数倍
        $money = bcdiv($transactionAmount, $this->conf['ecion']['exchange_rate'], 2); //金币除以汇率 即可

        //减金币
        $tmp = array(
            'userId' => $input['userId'],
            'transactionAmount' => -1 * $transactionAmount,
            'type' => 'exchange',
        );
        $this->change($tmp);
        
        //加钱
        //todo 待重构。应写一个\model\Money->change()
        $sql = 'UPDATE murcielago_user SET money=money+' . $money . ' WHERE uid=' . $input['userId'] . ' LIMIT 1';
        $this->userEcionDb->db->update($sql);
        $sql = 'INSERT INTO `murcielago_moneychange` (`uid`, `money`, `addTime`, `remark`) VALUES (' . $input['userId'] . ', ' . $money . ', ' . time() .', \'' . _('金币兑换'). '\')';
        $this->userEcionDb->db->query($sql);

        $where = array(
            'e' => array(
                'uid' => $input['userId'],
            ),
        );
        $r = $this->userEcionDb->selectRow($where, array('total'));
 
        return array(
            'total' => $r['total'], //可用金币int
            'transactionAmount' => $transactionAmount,
        );
    }

    public function giveLoginGift($input)
    {
        $where = array(
            'e' => array(
                'uid' => $input['userId'],
                'type' => self::$types['login'],
            ),
            'gte' => array(
                'addTime' => strtotime(date('Y-m-d', time())), //登录奖励每天一次
            ),
        );
        $cnt = $this->ecionChangeDb->selectCount($where);
        if($cnt > 0) {
            throw new \exception\Model(4030); //今天已送过
        }

        $input['type'] = 'login';
        $this->giveGift($input);

        $sql = 'UPDATE murcielago_user_ecion SET countLogin=countLogin+1 WHERE uid=' . $input['userId'] . ' LIMIT 1';
        $this->userEcionDb->db->update($sql);
        
        return true;
    }

    public function giveGift($input)
    {
        $type = $input['type'];
        if(!isset($this->conf['reward'][$type])) {
            throw new \exception\Model(5000);
        }

        try {
            $this->userEcionDb->insertRow(array(
                'uid' => $input['userId'],
            ));
        } catch (\Exception $e) {
        }

        $input['transactionAmount'] = $this->conf['reward'][$type];
        $this->change($input);

        if($type == 'invite') {
            $sql = 'update murcielago_user_ecion set countInvite=countInvite+1 where uid=' . $input['userId'] . ' LIMIT 1';
            $this->userEcionDb->db->update($sql);

            $notice = new Notice();
            $tmp = array(
                'userId' => $input['userId'],
                'title' => _('礼物到^_^'),
                'content' => str_replace('{transactionAmount}', $input['transactionAmount'], _('谢谢亲的热情推荐！抢宝特此奉上 {transactionAmount}金币供您享用，快来兑换爱抢购账户余额，累计金币越多，吃喝玩乐享更多哦！')),
            );
            $notice->add($tmp);
        }
        return true;
    }
    
    public function getList($input)
    {
        $where = array(
            'e' => array(
                'uid' => $input['userId'],
                'type' => $input['type'],
            ),
        );
        $column = array(
            'addTime',
            'transactionAmount',
            'remark',
        );
        $orderBy = 'addTime DESC';
        $entities = $this->ecionChangeDb->selectRows($where, $column, $input['offset'], $input['limit'], '', $orderBy);

        $column = array(
            'sum(transactionAmount) as transactionAmountSum',
        );
        $r = $this->ecionChangeDb->selectRow($where, $column);
        return array(
            'entities' => $entities,
            'transactionAmountSum' => $r['transactionAmountSum'],
            'total' => count($entities),
        );
    }
}
?>
