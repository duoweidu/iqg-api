<?php
namespace model;
class Log
{
    private function __construct()
    {
    }

    public static function add($type, $data)
    {
        if(!isset($data['shopId']) || $data['shopId'] == null) {
            $goodsDb = new \model\dao\db\iqg2012\MurcielagoGoodsShop();
            $where = array(
                'e' => array(
                    'goodsId' => $data['goodsId'],
                ),
            );
            $r = $goodsDb->selectRow($where, array('shopId'));
            $data['shopId'] = $r['shopId'];
        }
        $dbClass = '\model\dao\db\iqg_log\\' . \PEAR2\Str::firstToUpper($type) . 'Goods';
        $db = new $dbClass ();
        $r = $db->insertRow($data);
        return true;
    }

    public static function multiAdd($type, $data)
    {
        $dbClass = '\model\dao\db\iqg_log\\' . \PEAR2\Str::firstToUpper($type) . 'Goods';
        $db = new $dbClass ();
        $r = $db->insertRows($data);
        return true;
    }
}
?>
