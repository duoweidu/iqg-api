<?php
namespace model;

/**
 * ad
 */
class Ad extends Base
{
    private $adVersionDb;

    public function __construct()
    {
        $this->adDb = new \model\dao\db\iqg2012\MurcielagoAd();
    }

    public function getSplash($input)
    {
        $now = time();
        $ad = array();
        if(isset($input['citySid'])) {
            $cityId = City::decryptSid($input['citySid']);
            $sql = 'select imgUri,endTime from murcielago_ad where cityId=' . $cityId . ' AND beginTime<' . $now . ' AND endTime>' . $now . ' order by abs(width-' . $input['width'] . ') asc limit 1';
            $ad = $this->adDb->db->selectRow($sql);
        }

        if(empty($ad)) {
            $cityId = 0;
            $sql = 'select imgUri,endTime from murcielago_ad where cityId=' . $cityId . ' AND beginTime<' . $now . ' AND endTime>' . $now . ' order by abs(width-' . $input['width'] . ') asc limit 1';
            $ad = $this->adDb->db->selectRow($sql);
        }
        return $ad;
    }
}
?>
