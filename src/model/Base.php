<?php
/**
 *  model 基类
 */
namespace model;
class Base
{
    public function __construct()
    {
        
    }

    public static function encryptId($id)
    {
        return strval($id);
        //$sid = 's' . $id;
        //return strval($sid);
    }

    public static function decryptSid($sid)
    {
        if(is_numeric($sid)) {
            return intval($sid);
        } else {
            $id = substr($sid, 1);
            return intval($id);
        }
    }

    public static function decryptSids($sids)
    {
        $ids = array();
        foreach($sids as $sid) {
            $ids[] = self::decryptSid($sid);
        }
        return $ids;
    }

    /**
     *
     * 生成支付的订单号
     */
    public static function encryptPaymentId($num)
    {
        //假设$num = 12345
        $len = str_pad(strlen($num), 2, '0', STR_PAD_LEFT); // $len = 05
        $maps = \PEAR2\Conf::get('encrypt_num', 'maps');
        $key = substr($num, -2, 1) * substr($num, -1); // $key = 0
        $map = $maps[$key];
        $r = str_pad($key, 2, '0', STR_PAD_LEFT) . $map[$len{0}] . $map[$len{1}]; // $r = 00**
        for($i=1; $i<=$len; $i++) {
            $tmp = substr($num, $i - 1, 1);
            $r .= $map[$tmp]; 
        } // $r = 00**54321
        //补满
        for($i=strlen($r); $i<16; $i++) {
            $r .= rand(0, 9);
        } // $r = 00**54321-----------
        return $r;
    }


    /**
     * 解密支付的订单号
     */
    public static function decryptPaymentSid($num)
    {
        //假设$num = '12752019317507562532'
        $key = intval(substr($num, 0, 2)); // 12
        $encryptedLen = substr($num, 2, 2); // 75
        $maps = \PEAR2\Conf::get('encrypt_num', 'maps');
        $map = $maps[$key];
        $tmp = array_search($encryptedLen{0}, $map) . array_search($encryptedLen{1}, $map);
        $len = intval($tmp);
        $encryptedId = substr($num, 4, $len);
        $r = '';
        for($i=0; $i<$len; $i++) {
            if (!isset($encryptedId{$i})) {
                throw new \exception\Model(4043);
            }
            $r .= array_search($encryptedId{$i}, $map);
        }
        return $r;
    }
}
?>
