<?php
namespace model;

/**
 * 账单
 */
class Bill extends Base
{
    private $moneyChangeDb;

    public function __construct()
    {
        $this->moneyChangeDb = new \model\dao\db\iqg2012\MurcielagoMoneychange();
    }

    public function getList($userId, $startTime)
    {
        $where = array(
            'e' => array(
                'uid' => $userId,
            ),
            'gte' => array(
                'addTime' => $startTime,
            ),
        );
        $column = array(
            'addTime',
            'money',
            'remark',
        );
        $orderBy = 'addTime DESC';
        $entities = $this->moneyChangeDb->selectRows($where, $column, '', '', '', $orderBy);
        return array(
            'entities' => $entities,
            'total' => count($entities),
        );
    }
}
?>
