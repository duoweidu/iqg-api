<?php
namespace model;
class Comment extends Base
{
    private $itemCommentDb;

    public function __construct()
    {
        $this->itemCommentDb = new \model\dao\db\iqg2012\MurcielagoGoodsComment();
        $this->imgUgcDb = new \model\dao\db\iqg2012\MurcielagoImgUgc();
    }

    public static function getImgUri($filename) {
        $domain = \PEAR2\Conf::get('qiniu', 'all[\'buckets\'][\'img-ugc\'][\'customDomain\']');
        return 'http://' . $domain . '/' . $filename;
    }

    /**
     * 评论一个订单
     */
    public function add($input)
    {
        $orderDb = new \model\dao\db\iqg2012\MurcielagoOrder();
        try {
            //老订单解析sid会失败，然后进入catch
            $orderId = \model\Order::decryptSid($input['orderSid']);
            $where = array(
                'e' => array(
                    'orderId' => $orderId,
                    'uid' => $input['userId'],
                ),
            );
            $order = $orderDb->selectRow($where, array('orderId', 'isCommented'));
            if (empty($order)) {
                throw new \exception\Model(4043);
            }
        } catch (\exception\Model $e) {
            $where = array(
                'e' => array(
                    'orderSn' => $input['orderSid'],
                    'uid' => $input['userId'],
                ),
            );
            $order = $orderDb->selectRow($where, array('orderId', 'isCommented'));
            if (empty($order)) {
                throw new \exception\Model(4043);
            }
            $orderId = $order['orderId'];
        }

        $where = array(
            'e' => array(
                'orderId' => $orderId,
            ),
        );
        $orderGoodsDb = new \model\dao\db\iqg2012\MurcielagoOrderGoods();
        $r = $orderGoodsDb->selectRow($where, array('goodsId'));
        if (empty($r)) {
            throw new \exception\Model(4041);
        }

        if ($order['isCommented'] == 1) {
            throw new \exception\Model(4035);
        }

        $data = array(
            'orderId' => $orderId,
            'orderSn' => $input['orderSid'],
            'uid' => $input['userId'],
            'goodsId' => $r['goodsId'],
            'content' => $input['content'],
            'addIp' => $input['ip'],
            'addTime' => time(),
        );
        $goodscommentId = $this->itemCommentDb->insertRow($data);

        if (!empty($input['imgs'])) {
            $where = array(
                'e' => array(
                    'filename' => $input['imgs'],
                    'uid' => $input['userId'],
                    'goodscommentId' => null,
                )
            );
            $data = array(
                'goodscommentId' => $goodscommentId,
            );
            $r = $this->imgUgcDb->update($data, $where);
        }
        
        $where = array(
            'e' => array(
                'orderId' => $orderId,
            ),
        );
        $data = array(
            'isCommented' => 1,
        );
        $r = $orderDb->update($data, $where);
 
        return true;
    }

    public function getList($input)
    {
        $where = array(
            'e' => array(
            ),
        );
        if(isset($input['userId'])) {
            $where['e']['uid'] = $input['userId'];
        }
        if(isset($input['itemSid'])) {
            $where['e']['goodsId'] = Item::decryptSid($input['itemSid']);
            $where['e']['isApproved'] = 1;
        }
        $total = $this->itemCommentDb->selectCount($where);
        $data = array();
        if ($total == 0 || $input['limit'] == 0) {
            return array(
                'entities' => $data,
                'total' => intval($total),
            );
        }
        $r = $this->itemCommentDb->selectRows($where, '*', $input['offset'], $input['limit'], '',
            array(
                'helpfulCount' => 'DESC',
                'addTime' => 'DESC'
            )
        );
        $data = array();
        foreach($r as $one) {
            $data[$one['goodscommentId']] = array(
                'addTime' => intval($one['addTime']),
                'content' => $one['content'],
                'itemSid' => self::encryptId($one['goodsId']),
                'sid' => self::encryptId($one['goodscommentId']),
                'userSid' => self::encryptId($one['uid']),
                'imgs' => array(),
            );
        }
        $where = array(
            'e' => array(
                'goodscommentId' => array_keys($data),
            )
        );
        $r = $this->imgUgcDb->selectRows($where, '*');
        foreach ($r as $one) {
            $data[$one['goodscommentId']]['imgs'][] = self::getImgUri($one['filename']);
            $data[$one['goodscommentId']]['imgs'] = array_values(array_unique($data[$one['goodscommentId']]['imgs']));
        }
        return array(
            'entities' => array_values($data),
            'total' => intval($total),
        );
    }
}
?>
