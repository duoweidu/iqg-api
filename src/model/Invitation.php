<?php
namespace model;
class Invitation extends Base
{
    private $recordInvitationDb;

    public function __construct()
    {
        $this->recordInvitationDb = new \model\dao\db\iqg2012\MurcielagoRecordInvitation();
    }

    public static function decodeFullMobile($fullMobile, $defaultCountryCallingCode)
    {
        $data = array(
            'countryCallingCode' => $defaultCountryCallingCode,
            'mobile' => $fullMobile,
            'originalMobile' => $fullMobile,
        );
        if(stripos($fullMobile, '+') !== 0) {
            return $data;
        }
        $codes = \PEAR2\Conf::get('sms', 'countryCallingCodes');
        $tmp = substr($fullMobile, 1);
        $code = '+';
        for($i=1; $i<=3; $i++) {
            $tmp1 = substr($tmp, 0, $i);
            if(in_array($tmp1, $codes)) {
                $code .= $tmp1;
                $mobile = substr($tmp, $i);
                break;
            }
        }
        if($code == '+') {
            throw new \exception\Model(4001, '完整手机号格式错误');
        }
        $data['countryCallingCode'] = $code;
        $data['mobile'] = $mobile;
        return $data;
    }

    /**
     * 邀请多个人
     */
    public function add($input)
    {
        $where = array(
            'e' => array(
                'uid' => $input['userId'],
            )
        );
        $userDb = new \model\dao\db\iqg2012\MurcielagoUser();
        $user = $userDb->selectRow($where, array('countryCallingCode', 'mobile'));
        if(isset($input['nickname']) && !empty($input['nickname'])) {
            $nickname = $input['nickname'];
        } else {
            $nickname = $user['mobile'];
        }

        $userModel = new User();
        foreach($input['fullMobiles'] as $fullMobile) {
            try {
                //如果有的手机号格式错误，跳过
                $tmp = self::decodeFullMobile($fullMobile, $user['countryCallingCode']);
            } catch (\Exception $e) {
                continue;
            }
            try {
                $userModel->checkRegistered($tmp['countryCallingCode'], $tmp['mobile']);
            } catch (\Exception $e) {
                continue;
            }

            $data = array(
                'ip' => $input['ip'],
                'countryCallingCode' => $tmp['countryCallingCode'],
                'mobile' => $tmp['mobile'],
                'addTime' => time(),
                'nickname' => $nickname,
            );
            
            $userModel->sendVcode($data, 'inviteFriend');

            $data = array(
                'fromUserId' => $input['userId'],
                'countryCallingCode' => $tmp['countryCallingCode'],
                'mobile' => $tmp['mobile'],
                'originalMobile' => $tmp['originalMobile'],
                'addTime' => time(),
            );
            $this->recordInvitationDb->insertRow($data);
        }

        return true;
    }

    /**
     * 批量检查手机号 是否已被邀请
     */
    public function multiCheckInvited($fullMobiles)
    {
        $code = \PEAR2\Conf::get('sms', 'defaultCountryCallingCode');
        $invited = array();
        foreach($fullMobiles as $fullMobile) {
            try {
                //如果有的手机号格式错误，跳过
                $tmp = self::decodeFullMobile($fullMobile, $code);
            } catch (\Exception $e) {
                continue;
            }
            try {
                $this->checkInvited($tmp['countryCallingCode'], $tmp['mobile']);
            } catch (\Exception $e) {
                $invited[] = $fullMobile;
                continue;
            }
        }
        return $invited;
    }
     
    /**
     * 检查手机号 是否已被邀请
     */
    public function checkInvited($countryCallingCode, $mobile)
    {
        $ttl = \PEAR2\Conf::get('invitation', 'ttl');
        $where = array(
            'e' => array(
                'countryCallingCode' => $countryCallingCode,
                'mobile' => $mobile,
            ),
            'gte' => array(
                'addTime' => time() - $ttl,
            ),
        );
        $r = $this->recordInvitationDb->selectCount($where);
        if($r>=1) {
            throw new \exception\Model(4039);
        }
        return false;
    }

    /**
     * 获取我的邀请历史
     */
    public function getHistory($input)
    {
        $where = array(
            'e' => array(
                'fromUserId' => $input['userId'],
            ),
        );
        if(isset($input['isAccepted'])) {
            $where['e']['isAccepted'] = intval($input['isAccepted']);
        }
        $column = array('addTime', 'countryCallingCode', 'mobile', 'isAccepted');
        $r = $this->recordInvitationDb->selectRows($where, $column, $input['offset'], $input['limit'], '', 'addTime DESC');
        $data = array(
            'entities' => array(),
            'total' => 0,
        );
        foreach($r as $one) {
            $tmp = $one;
            $tmp['isAccepted'] = $one['isAccepted']==1 ? true : false;
            $data['entities'][] = $tmp;
        }
        $data['total'] = $this->recordInvitationDb->selectCount($where);
        return $data;
    }

    /**
     * 查询是谁邀请我注册
     */
    public function getInviter($countryCallingCode, $mobile)
    {
        $where = array(
            'e' => array(
                'countryCallingCode' => $countryCallingCode,
                'mobile' => $mobile,
                'isAccepted' => 1,
            ),
        );
        $r = $this->recordInvitationDb->selectRow($where, array('fromUserId', 'originalMobile'), '', '', '', 'addTime DESC');
        if(isset($r['fromUserId'])) {
            return $r;
        }
        return false;
    }
}
