<?php
namespace model;

class File
{
    private $imgUgcDb;

    public function __construct()
    {
        $this->imgUgcDb = new \model\dao\db\iqg2012\MurcielagoImgUgc();
    }

    public function upload($input)
    {
        $md5 = md5_file($input['filePath']);
        $newName = $md5;

        $headers = array(
            'Content-Type' => $input['contentType'],
        );

        $conf = \PEAR2\Conf::get('qiniu', 'all');

        $group = 'img-ugc';
        $rs = new \Qiniu\RS(array_merge($conf['common'], $conf['buckets'][$group]));
        try {
            $r = $rs->uploadFile($input['filePath'], $newName, $headers);
        } catch (\Qiniu\Exception $e) {
            throw new \exception\Model(5000, array(), '云存储上传失败' . $e->getCode() . $e->getMessage());
        }

        $data = array(
            'filename' => $newName,
            'uid' => $input['uid'],
        );
        $this->imgUgcDb->insertRow($data);

        $r2 = $r;
        $r2['filename'] = $newName;
        if (isset($r['width'])) {
            $r2['width'] = intval($r['width']);
        }
        if (isset($r['height'])) {
            $r2['height'] = intval($r['height']);
        }
        return $r2;
    }
}
