<?php
namespace model;
class Order extends Base
{
    private $orderDb;

    private $orderItemDb;

    private $goodsDb;

    public function __construct()
    {
        $this->orderDb = new \model\dao\db\iqg2012\MurcielagoOrder();
        $this->orderItemDb = new \model\dao\db\iqg2012\MurcielagoOrderGoods();
        $this->goodsDb = new \model\dao\db\iqg2012\MurcielagoGoods();
        $this->itemCommentDb = new \model\dao\db\iqg2012\MurcielagoGoodsComment();
    }

    public static function encryptId($num)
    {
        return parent::encryptPaymentId($num);
    }

    public static function decryptSid($num)
    {
        return parent::decryptPaymentSid($num);
    }

    /**
     * 下单
     * 规则：每人每店一段时间内只能购买一个商品
     */
    public function add($input)
    {
        //查询item
        $goodsId = Item::decryptSid($input['itemSid']);
        $where = array(
            'e' => array(
                'goodsId' => $goodsId,
                'enable' => 1,
            ),
            'lte' => array(
                'beginDate' => date('Y-m-d'),
            ),
            'gte' => array(
                'endDate' => date('Y-m-d'),
            ),
        );
        $column = array(
            'currentPrice',
            'goodsNumber',
            'expiredDays',
            'buyPointPrice',
        );
        $goodsDb = new \model\dao\db\iqg2012\MurcielagoGoods();
        $goods = $goodsDb->selectRow($where, $column);
        if(empty($goods)) {
            throw new \exception\Model(4041);
        }
        if(!is_null($goods['buyPointPrice'])) { //0元才准买 存在吗？这里认为存在。todo 讨论
            if(bccomp($goods['currentPrice'], $goods['buyPointPrice'], '2') == 1) {
                sleep(rand(1, 3)); //防止连续快速点击，并发太高
                throw new \exception\Model(40310, '', $goods);
            }
        }
        if($goods['goodsNumber'] <=0) { //抢完了
            throw new \exception\Model(4033);
        }
        $goodsShopDb = new \model\dao\db\iqg2012\MurcielagoGoodsShop();
        $where = array(
            'e' => array(
                'goodsId' => $goodsId,
            ),
        );
        $goodsShop = $goodsShopDb->selectRow($where, array('shopId'));
        if(empty($goodsShop)) {
            throw new \exception\Model(4041);
        }

        $this->checkUnpaid($input['userId']);

        //今天反复下单取消这个商品，不让再买
        $startTime = strtotime(date('Y-m-d', time()));
        $where = array(
            'e' => array(
                'goodsId' => $goodsId,
                'uid' => $input['userId'],
                'orderStatus' => array(-1, 0),
            ),
            'gte' => array(
                'addTime' => $startTime,
            ),
        );
        $cancleTimes = $this->orderDb->selectCount($where);
        if($cancleTimes > 0) {
            $confDb = new \model\dao\db\iqg2013\Conf();
            $where = array(
                'e' => array(
                    'name' => array('item'),
                )
            );
            $r = $confDb->selectRow($where);
            $conf = json_decode($r['value'], true);
            
            if($cancleTimes >= $conf['oneDayCancleTimes']) {
                throw new \exception\Model(40316, '', 
                    array(
                        'cancleTimes' => $cancleTimes,
                    )
                );
            }
        }
 
        //一个店限制多次购买
        $where = array(
            'e' => array(
                'shopId' => $goodsShop['shopId'],
            ),
        );
        $shopDb = new \model\dao\db\iqg2012\MurcielagoShop();
        $shop = $shopDb->selectRow($where, array('buyCycle'));
        if(empty($shop)) {
            throw new \exception\Model(4041);
        }
        $startTime = strtotime(date('Y-m-d', time() - $shop['buyCycle'] * 86400));
        $sql = 'SELECT o.orderId, o.addTime FROM murcielago_order_goods AS og, murcielago_order AS o WHERE o.orderStatus IN (0,1,2,-2) AND o.uid=' . $input['userId'] . ' AND o.deleted=0 AND o.addTime>' . $startTime . ' AND o.orderId=og.orderId AND og.shopId=' . $goodsShop['shopId'];
        $r = $this->orderItemDb->queryFetch($sql);
        if(!empty($r)) {
            throw new \exception\Model(4034, '', 
                array(
                    'buyLimitDays' => ceil(($r[0]['addTime'] + $shop['buyCycle'] * 86400 - time()) / 86400),
                    'buyLimitDate' => date('Y-m-d', $r[0]['addTime'] + $shop['buyCycle'] * 86400 + 86400),
                )
            );
        }

        //减库存
        $sql = 'update murcielago_goods set goodsNumber=goodsNumber-1 WHERE goodsId=' . $goodsId . ' AND goodsNumber>0 LIMIT 1';
        $r = $goodsDb->db->update($sql);
        if($r<1) { //当同一秒下单时，上面拦截不住，这里才能拦截住超卖。比如 限价抢购活动 并发高。
            throw new \exception\Model(4033);
        }
        
        //写入order表。如果减库存成功，创建订单失败，也不会超卖。
        $order = array(
            'uid' => $input['userId'],
            'orderSn' => date('ymd') . str_pad($input['userId'], 4, '0', STR_PAD_LEFT) . date('His'),
            'payPrice' => $goods['currentPrice'],
            'lat' => $input['lat'],
            'lng' => $input['lng'],
            'addIp' => $input['ip'],
            'addTime' => time(),
            'goodsId' => $goodsId,
            'expiredDate' => date('Y-m-d', strtotime('+' . $goods['expiredDays'] . ' day')),
        );
        $orderId = $this->orderDb->insertRow($order);
        if(empty($orderId)) {
            throw new \exception\Model(5000);
        }
        $where = array(
            'e' => array(
                'orderId' => $orderId,
            ),
        );
        $orderSid = self::encryptId($orderId);
        $data = array(
            'orderSn' => $orderSid,
        );
        $this->orderDb->update($data, $where);

        //写入order_goods表
        $orderGoods = array(
            'orderId' => $orderId,
            'shopId' => $goodsShop['shopId'],
            'goodsId' => $goodsId,
            'uid' => $input['userId'],
            'qty' => 1,
            'addTime' => time(),
            'price' => $goods['currentPrice'],
        );
        $this->orderItemDb->insertRow($orderGoods);

        //处理下单 需要的时间长，而砍价很快。在下单的过程中，可能又有别人砍价了，所以最后要更新一下价格。
        $sql = 'UPDATE murcielago_order SET payPrice=(SELECT currentPrice FROM murcielago_goods WHERE goodsId=' . $goodsId . ' LIMIT 1) WHERE orderId=' . $orderId . ' LIMIT 1';
        $goodsDb->db->update($sql);
        $sql = 'UPDATE murcielago_order_goods SET price=(SELECT currentPrice FROM murcielago_goods WHERE goodsId=' . $goodsId . ' LIMIT 1) WHERE orderId=' . $orderId . ' AND goodsId=' . $goodsId . ' LIMIT 1';
        $goodsDb->db->update($sql);

        $data = array(
            'sid' => $orderSid,
        );
        return $data;
    }

    /**
     * 未支付订单超过几个，不准再买
     */
    private function checkUnpaid($userId)
    {
        $where = array(
            'e' => array(
                'uid' => $userId,
                'orderStatus' => 0,
                'deleted' => 0,
            ),
        );
        $cnt = $this->orderDb->selectCount($where);
        $max = \PEAR2\Conf::get('order', 'maxUnpaidCount');
        if($cnt>=$max) {
            throw new \exception\Model(40311, '', array('unpaidCount' => $cnt));
        }
        return true;
    }

    public function getList($input)
    {
        $where = array(
            'e' => array(
                'uid' => $input['userId'],
                'deleted' => 0,
            ),
        );
        if(isset($input['isCommented'])) {
            $where['e']['isCommented'] = $input['isCommented'];
        }
        $orderBy = 'addTime DESC';
        if(isset($input['status'])) {
            $where['e']['orderStatus'] = $input['status'];
            //已完成的订单，按照领用时间排序。领用时间为空的订单排序会有点问题。
            if(is_array($input['status'])) {
                if(in_array(2, $input['status'])) {
                    $orderBy = array(
                        'isCommented' => 'ASC',
                        'updateTime' => 'DESC',
                    );
                }
            } else {
                if(2 == $input['status']) {
                    $orderBy = array(
                        'isCommented' => 'ASC',
                        'updateTime' => 'DESC',
                    );
                }
            }
        }
        $column = array(
            'orderId',
            'orderSn',
            'orderStatus',
            'expiredDate',
            'addTime',
            'updateTime',
            'payPrice',
            'distributionNo',
            'isCommented',
        );
        $total = $this->orderDb->selectCount($where);
        $orders = $this->orderDb->selectRows($where, $column, $input['offset'], $input['limit'], '', $orderBy);

        $data = array(
            'entities' => array(),
            'total' => 0,
        );
        if (empty($orders)) {
            return array(
                'entities' => array(),
                'total' => 0,
            );
        }

        $ordersIds = array();
        $ordersMap = array();
        foreach($orders as $one) {
            $ordersIds[] = $one['orderId'];
            $ordersMap[$one['orderId']] = array(
                'addTime' => intval($one['addTime']),
                'expiredTime' => intval(strtotime($one['expiredDate'] . ' 23:59:59')),
                'subtotal' => intval(bcmul($one['payPrice'], 100, 2)),
                'sid' => strval($one['orderSn']),
                'status' => intval($one['orderStatus']),
                'ecode' => strval($one['distributionNo']),
                'isCommented' => boolval($one['isCommented']),
            );
            //未支付订单的有效期
            if($one['orderStatus'] == 0) {
                $ordersMap[$one['orderId']]['expiredTime'] = intval($one['addTime'] + \PEAR2\Conf::get('order', 'ttl'));
            } else if ($one['orderStatus'] == 2 && !empty($one['updateTime'])) {
                $ordersMap[$one['orderId']]['expiredTime'] = intval($one['updateTime']);
            }
        }

        $where = array(
            'e' => array(
                'orderId' => $ordersIds,
            ),
        );
        $column = array(
            'orderId',
            'goodsId',
        );
        $r = $this->orderItemDb->selectRows($where, $column);
        $itemsOrdersMap = array();
        $itemsIds = array();
        foreach($r as $one) {
            //不存在的商品，订单也不显示了。
            if(!isset($ordersMap[$one['orderId']])) {
                unset($ordersMap[$one['orderId']]);
                continue;
            }
            $itemsOrdersMap[$one['goodsId']] = $one;
            $itemsIds[] = $one['goodsId'];
            $ordersMap[$one['orderId']]['itemSid'] = Item::encryptId($one['goodsId']);
            //查兑换方式，如果是密码兑换，则要给出密码。通兑，只取第一个店
            $sql = 'SELECT h.ecodeValidatePassword, s.ecodeValidateType FROM murcielago_shop as s, murcielago_headquarter as h, murcielago_goods_shop as gs WHERE s.shopId = gs.shopId AND h.headquarterId=s.parentId AND gs.goodsId =' . $one['goodsId'];
            $tmp = $this->orderItemDb->db->selectRow($sql);
            //门店不存在了，比如被删除了
            if(empty($tmp)) {
                continue;
            }
            $ordersMap[$one['orderId']]['ecodeValidateType'] = intval($tmp['ecodeValidateType']);
            $ordersMap[$one['orderId']]['ecodeValidatePassword'] = strval($tmp['ecodeValidatePassword']);
        }
        
        //todo 通兑，可能会查出来多条，多循环了几次，浪费
        $sql = 'SELECT s.shopId, gs.goodsId, s.businessHoursClose FROM murcielago_shop as s, murcielago_goods_shop as gs WHERE gs.goodsId IN (' . implode($itemsIds, ',') . ') AND s.shopId = gs.shopId';
        $r = $this->orderItemDb->db->selectRows($sql);
        foreach($r as $one) {
            if(empty($one['businessHoursClose'])) {
                continue;
            }
            $orderId = $itemsOrdersMap[$one['goodsId']]['orderId'];
            if($ordersMap[$orderId]['status'] == 1) {
                $ordersMap[$orderId]['expiredTime'] = intval(strtotime(date('Y-m-d', $ordersMap[$orderId]['expiredTime']) . ' ' . $one['businessHoursClose']));
            }
        }

        return array(
            'entities' => array_values($ordersMap),
            'total' => $total,
        );
    }

    public function get($input)
    {
        $where = array(
            'e' => array(
                'orderId' => self::decryptSid($input['sid']),
                'deleted' => 0,
            ),
        );
        $column = array(
            'orderId',
            'orderSn',
            'orderStatus',
            'expiredDate',
            'addTime',
            'payPrice',
            'distributionNo',
        );
        $order = $this->orderDb->selectRow($where, $column);
        if(empty($order)) {
            throw new \exception\Model(4043);
        }
        $data = array(
            'addTime' => intval($order['addTime']),
            'expiredTime' => intval(strtotime($order['expiredDate'] . ' 23:59:59')),
            'ecode' => strval($order['distributionNo']),
            'subtotal' => intval(bcmul($order['payPrice'], 100, 2)),
            'sid' => strval($order['orderSn']),
            'status' => intval($order['orderStatus']),
        );
        if($order['orderStatus'] == 0) {
            $data['expiredTime'] = intval($order['addTime'] + \PEAR2\Conf::get('order', 'ttl'));
        }

        $where = array(
            'e' => array(
                'orderId' => $order['orderId'],
            ),
        );
        $column = array(
            'goodsId',
        );
        $r = $this->orderItemDb->selectRow($where, $column);
        $data['itemSid'] = Item::encryptId($r['goodsId']);

        //查兑换方式，如果是密码兑换，则要给出密码。通兑，只取第一个店
        $sql = 'SELECT h.ecodeValidatePassword, s.ecodeValidateType FROM murcielago_shop as s, murcielago_headquarter as h, murcielago_goods_shop as gs WHERE s.shopId = gs.shopId AND h.headquarterId=s.parentId AND gs.goodsId =' . $r['goodsId'];
        $r = $this->orderItemDb->db->selectRow($sql);
        $data['ecodeValidateType'] = intval($r['ecodeValidateType']);
        $data['ecodeValidatePassword'] = strval($r['ecodeValidatePassword']);

        $where = array(
            'e' => array(
                'orderId' => $order['orderId'],
                'uid' => $input['userId'],
            ),
        );
        $r = $this->itemCommentDb->selectCount($where);
        $data['isCommented'] = $r > 0 ? true : false;

        return $data;
    }

    public function delete($input)
    {
        $orderId = self::decryptSid($input['sid']);
        $where = array(
            'e' => array(
                'orderId' => $orderId,
                'uid' => $input['userId'],
                'deleted' => 0,
                'orderStatus' => 0,
            ),
        );
        $data = array(
            'deleted' => 1,
            'orderStatus' => -1,
        );
        $count = $this->orderDb->update($data, $where);
        if($count > 0) {
            $sql = 'UPDATE murcielago_goods set goodsNumber=goodsNumber+1 WHERE goodsNumber<goodsStocks AND goodsId=(SELECT goodsId FROM murcielago_order_goods WHERE orderId=' . $orderId . ') LIMIT 1';
            $this->orderDb->db->query($sql);
        }
        return true;
    }

    public function getOrder($orderSid) {
        $orderId = $this->decryptSid($orderSid);

        $where = array(
            'e' => array(
            'orderId' => $orderId,
            ),
        );

        $result = array();
        $result = $this->orderDb->selectRow($where);
        return $result;    
    }

    /**
     * 获取订单和商品数据
     *
     * @param string $orderSid 订单的sid
     * @return array 
     */
    public function getOrderGoods($orderSid) {
        $orderId = $this->decryptSid($orderSid);

        $where = array(
            'e' => array(
            'orderId' => $orderId,
            ),
        );

        $result['order'] = array();
        $result['order'] = $this->orderDb->selectRow($where, array('orderId', 'addTime', 'payPrice', 'uid'));

        $og = $this->orderItemDb->selectRow($where, array('goodsId'));

        if ( empty($result['order']) || empty($og) ){
           throw new \exception\Model(4043, 'the order('.$orderId.')  is not found'); 
        }
        
        $where = array(
            'e' => array(
                'goodsId' => $og['goodsId'],
            ),
        );
        $result['goods'] = $this->goodsDb->selectRow($where, array('goodsId', 'goodsName', 'isMultiShop'));
        
        if ( empty($result['goods']) ){
           throw new \exception\Model(4041, 'the goods ('. $og['goodsId']. ')  is not found'); 
        }
        return $result;
    } 

    /**
     * 生成充值订单
     *
     * @param int    $uid
     * @param float    $amount
     * @param int    $payway
     * @param string    $orderSid
     *
     * return array
     */
    public function makeTopUpOrder($uid, $amount, $payway, $orderSid='') { 
        $data['uid']     = $uid; 
        $data['orderSn'] = $orderSid;
        $data['amount']  = $amount; 
        $data['addTime'] = time();
        $data['isPaid']  = 0;
        $data['payway']  = $payway;

        $orderPaymentDb = new \model\dao\db\iqg2012\MurcielagoOrderPayment();
        $paymentId = $orderPaymentDb->insertRow($data);

        $paymentSid = $this->encryptId($paymentId);
        if ( !empty($paymentId) ) {
            $where = array(
                'e' => array(
                    'id' => $paymentId,
                ),
            );
            $params = array(
                'orderSn'   => $orderSid,
                'paymentSn' => $paymentSid,
            );
            $orderPaymentDb->update($params, $where);
        }

        if ( !empty($orderSid) ) {
            $orderAndGoods = $this->getOrderGoods($orderSid);
        }

        $goodsName = !empty( $orderAndGoods['goods']['goodsName'] ) ? $orderAndGoods['goods']['goodsName'] : _('爱抢购充值');

        $result = array();
        $result['paymentSid'] = $paymentSid;
        $result['goodsName']  = $goodsName;
        $result['goodsDescription'] = $goodsName;
        $result['payPrice']  = $amount;
        $result['addTime']   = $data['addTime'];

        return $result;
    }

    /**
     * 更新成 已领用。
     */
    public function setToUsed($data)
    {
        $orderId = $this->decryptSid($data['sid']);

        $where = array(
            'e' => array(
                'orderId' => $orderId,
                'uid' => $data['userId'],
            ),
        );
        $r = $this->orderDb->selectRow($where);
        if (empty($r)) {
            throw new \exception\Model(4043);
        }
        //已完成了，不用更新了。这时应返回成功，而不是报错，就像update影响0行。
        if ($r['orderStatus'] == 2) {
            return true;
        }

        $where['e']['orderStatus'] = 1;
        $newData = array(
            'orderStatus' => 2,
            'updateTime' => time(),
        );

        $cnt = $this->orderDb->update($newData, $where);
        if($cnt == 0) {
            throw new \exception\Model(4030, 'setToUsed error');
        }
        return true;
    }

    /**
     * 更新成 等待到帐。
     */
    public function setToArriving($data)
    {
        $orderId = $this->decryptSid($data['sid']);

        $where = array(
            'e' => array(
                'orderId' => $orderId,
                'orderStatus' => 0,
                'uid' => $data['userId'],
            ),
        );

        $newData = array();
        $newData['orderStatus'] = -2;

        $cnt = $this->orderDb->update($newData, $where);
        if($cnt == 0) {
            throw new \exception\Model(4030, 'setToArriving error');
        }
        return true;
    }
}
?>
