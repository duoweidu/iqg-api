<?php
namespace model;
class FeedBack extends Base
{

    /**
     * @var 
     */
    private $feedBackDb;

    /**
     * 构造函数
     */
    public function __construct()
    {
        $this->feedBackDb = new \model\dao\db\iqg2012\MurcielagoFeedback;
    }

    public function add($data){
        return $this->feedBackDb->insertRow($data);
    }
}
