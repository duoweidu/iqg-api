<?php
$this_dir = dirname(__FILE__) . '/';
require_once $this_dir . '../plugin/autoload.php';
require_once $this_dir . '../plugin/route.php';

function headerHtmlApp()
{
    if(isset($_SERVER['HTTP_ORIGIN'])) {
        header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
        header('Access-Control-Allow-Methods: DELETE, GET, OPTIONS, POST, PUT');
        header('Access-Control-Expose-Headers: X-Api-Status-Code, X-Api-Status-Msg');
    }
    if(isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
        header('Access-Control-Allow-Headers: ' . $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']);
    }
    return true;
}
if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    headerHtmlApp();
    exit;
}

$tmp = parse_url($_SERVER['REQUEST_URI']);
$uri = $tmp['path']; //因为REQUEST_URI带get参数，影响路由。

//多国文字
$lang = 'en_US';
if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && (stripos($_SERVER['HTTP_ACCEPT_LANGUAGE'], 'zh') === 0)) {
    $lang = 'zh_CN';
}
putenv('LANGUAGE=' . $lang); //ubuntu 需要，centos不需要此行
setlocale(LC_ALL, $lang . '.utf8'); //ubuntu 需要.utf8，centos可有可无
$domain = 'strings';
bindtextdomain($domain, dirname(__FILE__) . '/../res/values');
bind_textdomain_codeset($domain , 'UTF-8');
textdomain($domain);

try {
    try {
        $tmp = route($uri);
        $tmp2 = explode('->', $tmp);
        $controller_name = $tmp2[0];
        $action_name = $tmp2[1];
        
        preg_match('/([^\(]+)\(?([^\)]*)\)?/', $action_name, $match);
        $action_name = $match[1];    
        if (isset($match[2])){
            $match[2] = str_replace('\'', '', $match[2]);
            $params = explode(',', $match[2]);
        }
        $c = new $controller_name ($uri);
        $r = call_user_func_array(array($c, $action_name), $params);    
    } catch (\exception\Model $e) {
        throw new \exception\Http(substr($e->getCode(), 0, 3), '', $e->getData());
    }
} catch (\exception\Http $e) {
    $r = $e->getData();
}

function getStatusStr($code)
{
    $httpCodeMap = \PEAR2\Conf::get('status_code_msg', 'http');
    //fastcgi与apache module不同
    //这里按照module格式
    //http://php.net/manual/zh/function.header.php
    return 'HTTP/1.1 ' . $code . ' ' . $httpCodeMap[$code];
}

function output($r)
{
    $status_str = getStatusStr($r['http']['Status-Code']);
    if (in_array($r['http']['Status-Code'], array(301, 302))) {
        header('Location: ' . $r['http']['uri'], true, $r['http']['Status-Code']);
        exit;
    } else {
        header($status_str);
    }
    if ($r['http']['Status-Code'] == 404) {
        $r['html'] = 404;
    }
    $tmp = array();
    foreach ($r['http']['Cache-Control'] as $k=>$v) {
        if (is_numeric($k) && in_array($v, array('private', 'public', 'no-cache'))) {
            $tmp[] = $v;
            continue;
        }
        if ($k == 'max-age') {
            $tmp[] = 'max-age=' . $v;
            continue;
        }
    }
    header('Cache-Control:' . implode(', ', $tmp));
    if (isset($r['http']['cache-control']['max-age'])) {
        header(
            'Expires:' . gmdate(
                'D, d M Y H:i:s T', 
                time() + $r['http']['cache-control']['max-age']
            )
        );
    }
    headerHtmlApp();

    $v = $r['data'];
    header('Content-Type: ' . $r['http']['Content-Type'] . '; charset=UTF-8');
    header('X-Api-Status-Code: ' . $r['http']['X-Api-Status-Code']);
    header('X-Api-Status-Msg: ' . rawurlencode($r['http']['X-Api-Status-Msg']));
    switch($r['http']['Content-Type']) {
    case 'text/html' :
        require __DIR__ . '/../res/layout/' . $r['html'] . '.html';
        break;
    case 'application/json' :
        if($r['http']['X-Content-Type'] == 'json-object' && !is_object(json_decode(json_encode($r['data'])))) {
            $v = new \stdClass();
        } else if($r['http']['X-Content-Type'] == 'json-array' && !is_array($r['data'])) {
            $v = array();
        }
        echo json_encode($v);
        break;
    case 'text/css' :
    case 'text/plain' :
    case 'application/javascript' :
        echo $v;
        break;
    }
    return true;
}
output($r);
exit;
?>
