DROP TRIGGER copy_cityId_to_goods_on_update;
delimiter $
CREATE TRIGGER copy_cityId_to_goods_on_update BEFORE UPDATE ON murcielago_goods
FOR EACH ROW
    BEGIN
        select shopId into @shopId from `murcielago_goods_shop` where goodsId = NEW.goodsId limit 1;
        select cityId into @cityId from `murcielago_shop` where shopId = @shopId limit 1;
        IF @shopId > 0 AND @cityId > 0 THEN
            set NEW.cityId = @cityId; 
        END IF;
    END;
$
delimiter ;

DROP TRIGGER copy_cityId_to_goods_on_insert;
delimiter $
CREATE TRIGGER copy_cityId_to_goods_on_insert BEFORE INSERT ON murcielago_goods
FOR EACH ROW
    BEGIN
        select shopId into @shopId from `murcielago_goods_shop` where goodsId = NEW.goodsId limit 1;
        select cityId into @cityId from `murcielago_shop` where shopId = @shopId limit 1;
        IF @shopId > 0 AND @cityId > 0 THEN
            set NEW.cityId = @cityId; 
        END IF;
    END;
$
delimiter ;

DROP TRIGGER copy_mobile_to_username_on_insert;
delimiter $
CREATE TRIGGER copy_mobile_to_username_on_insert BEFORE INSERT ON murcielago_user
FOR EACH ROW
    BEGIN
        IF NEW.mobile > 0  THEN
            set NEW.username = NEW.mobile; 
        END IF;
    END;
$
delimiter ;

