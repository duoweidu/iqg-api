<?php
namespace PEAR2;
class Conf
{
    private function __construct()
    {
    }

    /**
     * get conf from a file
     *
     * @param string $confDir  eg. '/data/conf/'
     * @param string $filePath eg. 'db' or 'db/mysql'
     * @param string $key       eg. 'server' or 'server[\'pgsql\'][\'db_name\']'
     * @param string $conf      file_ext default is '.conf',
     *                          format default is 'php_array'
     *
     * @return   mixed
     * @throws   Conf_Exception
     */
    public static function get($filePath, $key, $confDir=null)
    {
        if(empty($confDir)) {
            $confDir = __DIR__ . '/../../../conf/';
        }
        $conf = array(
            'file_ext' => '.conf',
            'format'=>'php_array',
        );
        
        $confFile = $confDir . $filePath . $conf['file_ext'];
        if ( !is_file($confFile) ){
            exit($confFile . 'is not a real file.');
        }
        require $confFile;
        /* eg. $server = array(
            'pgsql' => array(
                'db_name' => 'example',
                'username' => 'root',
            )
        )
        */
        $tmp         = str_replace(array('[\'', '\'][\'', '\']'), array('->'), $key);
        /* eg. $key = 'server[\'pgsql\'][\'db_name\']';
         $tmp = 'server->pgsql->db_name';
        */
        $tmp_array   = explode('->', $tmp);
        /* eg. $tmp_array = array(
            'server',
            'pgsql',
            'db_name',
        );
        */
        $main_string = $tmp_array[0]; //eg. $main_string = 'server';
        $r = $$main_string; //eg. $r = $server;
        unset($tmp_array[0]);
        foreach ($tmp_array as $v) {
            if (!isset($r[$v])) {
                throw new Exception('pear Conf error: undifined');
            }
            $r = $r[$v]; //eg. 1st $r = $r['pgsql']; 2nd $r = $r['db_name'];
        }
        return $r;
    }
}
?>
