<?php
namespace PEAR2\db;
class Mysqli
{
    private $_conn;
    
    public function __construct()
    {
    }
    
    public function connect($db_conf)
    {
        try {
            $this->_conn = new \mysqli(
                $db_conf['host'], $db_conf['username'],
                $db_conf['password'], $db_conf['db_name'], $db_conf['port']
            );
            if($this->_conn->connect_error) {
                throw new Exception($this->_conn->connect_error, $this->_conn->connect_errno);
            }
            $this->_conn->set_charset($db_conf['charset']);
            return $this->_conn;
        }
        catch(Exception $e) {
            throw new Exception($_conn->error);
        }
    }
    
    public function delete($sql)
    {
        return $this->_conn->query($sql);
    }

    public function insertRow($sql)
    {
        $this->_conn->query($sql);
        return $this->_conn->insert_id;
    }
    
    public function insertRows($sql)
    {
        return $this->_conn->query($sql);
    }
    
    public function query($sql)
    {
        $result = $this->_conn->query($sql);
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function selectCount($sql)
    {
        $result = $this->_conn->query($sql);
        $result_1 = $result->fetch_row();
        return $result_1[0];
    }
    
    public function selectRow($sql)
    {
        $result = $this->selectRows($sql);
        return empty($result) ? array() : $result[0];
    }
    
    public function selectRows($sql)
    {
        $result = $this->_conn->query($sql);
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function update($table_name,$where,$data)
    {
        return $this->_conn->query($sql);
    }
}
?>
