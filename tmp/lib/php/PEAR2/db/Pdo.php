<?php
namespace PEAR2\db;
class Pdo
{
    private $_conn;
    
    public function __construct()
    {
    }
    
    public function connect($dbConf)
    {
        switch($dbConf['dbProduct']) {
        case 'mysql' :
            $dsn = 'mysql:';
            if (!empty($dbConf['unix_domain_socket'])) {
                $dsn .= 'unix_socket=' . $dbConf['unix_domain_socket'] . ';';
            } else {
                $dsn .= 'host=' . $dbConf['host'] . ';port=' . $dbConf['port'] . ';';
            }
            $dsn .= 'dbname=' . $dbConf['dbName'];
            $this->_conn = new \PDO($dsn, $dbConf['username'], $dbConf['password']);
            if(isset($dbConf['charset'])) {
                $this->_conn->query('SET NAMES '. $dbConf['charset']);
            }
            break;
        case 'pgsql' :
            $dsn = 'pgsql:host=' . $dbConf['host'] . ';port=' . $dbConf['port']
            . ';dbname=' . $dbConf['dbName'] . ';user=' . $dbConf['username']
            . ';password=' . $dbConf['password'];
            $this->_conn = new \PDO($dsn);
            break;
        }
        //http://php.net/manual/en/pdo.setattribute.php
        $this->_conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $this->_conn;
    }

    public function delete($sql)
    {
        return $this->_conn->exec($sql); //todo delete 是幂等的？
    }
    
    public function insertRow($sql)
    {
        $this->_conn->exec($sql);
        return $this->_conn->lastInsertId(); //todo
    }
    
    public function insertRows($sql)
    {
        return $this->_conn->exec($sql);
    }

    public function query($sql)
    {
        $this->_conn->query($sql);
        return true;
    }

    public function queryFetch($sql)
    {
        $r = $this->_conn->query($sql);
        if (empty($r)) {
            return array();
        }
        $r->setFetchMode(\PDO::FETCH_ASSOC);
        return $r->fetchAll();
    }

    public function selectCount($sql)
    {
        $r = $this->_conn->query($sql);
        return intval($r->fetchColumn());
    }
    
    public function selectRow($sql)
    {
        $r = $this->selectRows($sql);
        return empty($r) ? array() : $r[0];
    }
    
    public function selectRows($sql)
    {
        $r = $this->_conn->query($sql);
        $r->setFetchMode(\PDO::FETCH_ASSOC);
        return $r->fetchAll();
    }
    
    public function update($sql)
    {
        return $this->_conn->exec($sql);
    }
}
?>
