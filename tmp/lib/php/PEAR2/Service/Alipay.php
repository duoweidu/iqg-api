<?php
/**
 *
 * 支付宝支付通用库
 *
 * @author dinghui <catro@vip.qq.com>
 * @copyright 2013 Doweidu Network Technology Co., Ltd.
 */
namespace PEAR2\Service;
class Alipay {

    /**
     *
     * 构造函数
     *
     * @param array 支付宝的配置
     *
     */
    public function __construct($conf, $orderSid=null){
        $this->conf = $conf;
	$this->conf['notify_url'] = $this->conf['notify_url'] . (!empty($orderSid) ? '/payment' : '');
    }


    /**
     * 获取支付宝快捷支付的签名
     *
     * @param array $params 生成订单之后，按照参数传进来。
     *
     * @return string 签名字符串
     *
     */
    public function getAlipaySign($params)
    {
        // 注意：以下参数是给客户端请求支付宝服务器用的。
        $data  = 'partner="' . $this->conf['partner'] . '"';
        $data .= '&';
        $data .= 'seller="' . $this->conf['seller'] . '"';
        $data .= '&';
        $data .= 'out_trade_no="' . $params['out_trade_no'] . '"';
        $data .= '&';
        $data .= 'subject="' . str_replace(array('+', '&', '"', '='), array('', '', '', ''), $params['subject']) . '"';
        $data .= '&';
        $data .= 'body="' . $params['body'] . '"';
        $data .= '&';
        $data .= 'total_fee="' . $params['total_fee'] . '"';
        $data .= '&';
        $data .= 'gmt_create="' . $params['gmt_create']. '"';
        $data .= '&';
        $data .= 'pay_expire="' . $params['pay_expire'] . '"'; // 15分钟后，交易将被关闭
        $data .= '&';
        $data .= 'notify_url="' . $this->conf['notify_url'] . '"';

        return $data . '&sign_type="RSA"&sign="' . urlencode(self::sign($data)) . '"';
    }


    /**
     * 获取支付宝web支付的Url
     *
     * @param array $params 生成订单之后，按照参数传进来。
     *
     * @return string 签名字符串
     */
    public function getAliwebpayUrl($params)
    {
        $data = array (
            "req_data"  => '<direct_trade_create_req><subject>' . $params['subject'] . '</subject><out_trade_no>' . $params['out_trade_no'] . '</out_trade_no><total_fee>' . $params['total_fee'] . "</total_fee><seller_account_name>" . $this->conf['seller'] . "</seller_account_name><pay_expire>15</pay_expire><notify_url>" . $this->conf['notify_url'] . "</notify_url><out_user>" . $params['out_user'] . "</out_user><merchant_url></merchant_url><call_back_url>" . $this->conf['call_back_url'] . "</call_back_url></direct_trade_create_req>",
            "service"   => $this->conf['service_create'],
            "sec_id"    => $this->conf['sec_id'],
            "partner"   => $this->conf['partner'],
            "req_id"    => md5(uniqid()),
            "format"    => $this->conf['format'],
            "v"         => $this->conf['version'] 
        );
        $token = $this->getAliwebpayTradeToken($data);
        
        $params = array (
            "req_data"      => '<auth_and_execute_req><request_token>' . $token . '</request_token></auth_and_execute_req>',
            "service"       => $this->conf['service_execute'],
            "sec_id"        => $this->conf['sec_id'],
            "partner"       => $this->conf['partner'],
            "call_back_url" => $this->conf['call_back_url'],
            "format"        => $this->conf['format'],
            "v"             => $this->conf['version'] 
        );   
        return $this->makeAliwebpayUrl($params);    	
    }

    public function buildMysign ($sort_array)
    {
        $prestr = $this->createLinkstring($sort_array);
        $mysgin = $this->sign($prestr);
        return $mysgin;
    }

    /**
     * 用私钥签名签名订单数据
     *
     * @param stirng $data 支付宝要求格式的字符串
     *
     * @return string 签名
     */
    public function sign($data)
    {
        //读取私钥文件
        $priKey = $this->conf['privateKey'];

        //转换为openssl密钥，必须是没有经过pkcs8转换的私钥
        $res = openssl_get_privatekey($priKey);

        //调用openssl内置签名方法，生成签名$sign
        openssl_sign($data, $sign, $res);

        //释放资源
        openssl_free_key($res);
        
        //base64编码
        $sign = base64_encode($sign);
        return $sign;
    }

    /**
     * RSA验签, 验签用支付宝公钥
     * 
     * @param string $data  待签名数据
     * @param string $sign  需要验签的签名
     *
     * @return 验签是否通过 bool值
     */
    public function verify($data, $sign)
    {
        $pubKey = $this->conf['publicKey'];
        $res = openssl_get_publickey($pubKey);          
        $result = (bool)openssl_verify($data, base64_decode($sign), $res);
        openssl_free_key($res);
        return $result;
    }

    /**
     * 通过节点路径返回字符串的某个节点值
     *
     * @param string $res_data XML格式字符串
     * 
     * @return string 节点参数
     */
    public function getDataForXML($res_data,$node)
    {
        $xml = simplexml_load_string($res_data);
        $result = $xml->xpath($node);

        while(list( , $node) = each($result)) 
        {
            return $node;
        }
    }
    
   /** 
     * 除去数组中的空值和签名参数

     * @param array $parameter 签名参数组
     *
     * @return 去掉空值与签名参数后的新签名参数组
     */
    public function getAliwebpayTradeToken($parameter)
    {   
        $parameter = $this->paraFilter($parameter);
        $sortArray = $this->argSort($parameter);
        $mySign = $this->buildMysign($sortArray);
        $requestData = $this->createLinkstring($parameter) . '&sign=' . urlencode($mySign);

       	$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->conf['aliwebpayGateWay']);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
        $result = curl_exec($ch);
        curl_close($ch);

        return $this->requestToken($result);
    }

    /**
     * 返回token参数
     * @param string $result 
     */
    public function requestToken($result)
    {
        $result = urldecode($result);
        $Arr = explode('&', $result);
        $temp = array();
        $myArray = array();
        for ($i = 0; $i < count($Arr); $i ++) {
            $temp = explode('=', $Arr[$i], 2);
            $myArray[$temp[0]] = $temp[1];
        }
        $myArray['res_data'] = $this->decrypt(isset($myArray['res_data']) ? $myArray['res_data'] : '');
        $sign = isset($myArray['sign']) ? $myArray['sign'] : '';
        $myArray = $this->paraFilter($myArray);
        $sort_array = $this->argSort($myArray);
        $prestr = $this->createLinkstring($sort_array);
        $isverify = $this->verify($prestr, $sign);
        if ($isverify) {
            return $this->getDataForXML($myArray['res_data'], '/direct_trade_create_res/request_token');
        } else {
            return '签名不正确';
        }
    }

    /** 
     * 调用alipay_Wap_Auth_AuthAndExecute接口
     */
    public function makeAliwebPayUrl($args)
    {   
        $parameter = $this->paraFilter($args);
        $sortArray = $this->argSort($parameter);
        $sign = $this->buildMysign($sortArray);
        $url = 'http://' . $this->conf['aliwebpayGateWay'] . '?' . $this->createLinkstring($parameter, 1) . '&sign=' . urlencode($sign);
        return $url;
    }   
 
    /** 
     * 除去数组中的空值和签名参数
     * @param array $parameter 签名参数组
     * @return string 去掉空值与签名参数后的新签名参数组
     */
    public function paraFilter ($parameter)
    {   
        $para = array();
        while (list ($key, $val) = each($parameter)) {
            if ($key == "sign" || $key == "sign_type" || $val == "") 
                continue;
            else
                $para[$key] = $parameter[$key];
        }   
        return $para;
    }   
 
    /** 
     * 对数组排序
     *
     * @param array $array 排序前的数组
     * @return array 排序后的数组
     */
    public function argSort ($array)
    {   
        ksort($array);
        reset($array);
        return $array;
    }


    /**
     * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
     *
     * @param array $array 需要拼接的数组
     * @return string 拼接完成以后的字符串
     */
    public function createLinkstring ($array, $urlencode = 0)
    {
        $arg = "";
        while (list ($key, $val) = each($array)) {
            if( $urlencode == 1){
                $arg .= $key . "=" . urlencode($val) . "&";
            }else{
                $arg .= $key . "=" . $val . "&";
            }
        }
        // 去掉最后一个&字符
        $arg = substr($arg, 0, count($arg) - 2);
        return $arg;
    }

    /**
     * 解密数据
     * 
     * @param string $content 
     * @return string
     */
    public function decrypt ($content)
    {
        $priKey = $this->conf['privateKey'];
        $res = openssl_get_privatekey($priKey);
        $content = base64_decode($content);
        $result = '';
        // 循环按照128位解密
        for ($i = 0; $i < strlen($content) / 128; $i ++) {
            $data = substr($content, $i * 128, 128);
            openssl_private_decrypt($data, $decrypt, $res);
            $result .= $decrypt;
        }
        openssl_free_key($res);
        return $result;
    }
}
?>
