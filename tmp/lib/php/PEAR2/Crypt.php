<?php
namespace PEAR2;
class Crypt
{
    private function __construct()
    {
    }

    public static function hash($password)
    {
        $options = array(
	    'cost' => 11,
	    'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
	);
	return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    public static function verify($password, $hash)
    {
        if(strlen($hash) == 32) {
	    if($hash == md5($password)) {
                return true;
            }
        } else {
            return password_verify($password , $hash);
        }
        return false;
    }
}
?>
