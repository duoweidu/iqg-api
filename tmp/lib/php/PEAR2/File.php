<?php
namespace PEAR2;
class File
{
    private function __construct()
    {

    }
   
    public static function formatFilesize($s)
    {
        if(intval($s) > 1073741824) {
            return round(intval($s) / 1073741824, 1) . 'GiB';
        }
        return intval(intval($s) / 1048576) . 'MiB';
    }
    public static function getSubdirs($dir)
    {
        $array = glob($dir . '/*', GLOB_ONLYDIR);
        $r = $array;
        if (empty($array)) {
            return array();
        }
        foreach($array as $one) {
            $tmp = self::getSubdirs($one);
            $r = array_merge($r, $tmp);
        }
        return $r;
    }

    public static function getSubfiles($dir, $like = '*')
    {
        $dirs = self::getSubdirs($dir);
        array_unshift($dirs, $dir);
        $r = array();
        foreach($dirs as $one) {
            $r = array_merge($r, glob($one . '/' . $like));
        }
        return $r;
    }
}
?>
