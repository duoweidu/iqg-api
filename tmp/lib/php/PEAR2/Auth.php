<?php
namespace PEAR2;
class Crypt
{
    private function __construct()
    {
    }

    private static $algos = array(
        'blowfish' => '2a',
        'sha512' => '6',
    );

    public static function hash($password, $algo='sha512', $salt=null, $rounds=0)
    {
        if (empty($algo)) {
            $algo = 'sha512';
        }
        if (empty($salt)) {
            $salt = substr(md5(microtime(true) . $password), 0, 16); //SHA-512的salt最长为16个字符
        }
        if (empty($rounds)) {
            $rounds = substr(time(), -4);
            $rounds = $rounds < 5000 ? $rounds + 5000 : $rounds; //本项目的加密次数最少5000,最大9999。以后可修改。SHA-512最小是 1000，最大是 999,999,999。
        }
        return crypt($password, '$' . self::$algos[$algo] . '$rounds=' . $rounds . '$' . $salt . '$');
    }

    public static function verify($password, $hash)
    {
        $tmp = explode('$', $hash);
        $algo_id = $tmp[1];
        $salt = $tmp[3];
        $rounds_tmp = $tmp[2];
        $tmp = explode('=', $rounds_tmp);
        $rounds = $tmp[1];
        $new_hash = self::hash($password, array_search($algo_id, self::$algos), $salt, $rounds);
        $tmp = substr($new_hash, 0, strlen($hash)); //先截取，再比较。即使数据库字段不够，丢弃了最后几位hash也没关系。
        if($tmp == $hash) {
            return true;
        }
        return false;
    }
}
?>
