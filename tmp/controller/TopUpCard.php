<?php
namespace controller;

/**
 * 充值卡top-up card
 */
class TopUpCard extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }

    /**
     * 用一个充值卡给自己充值
     *
     * 需要登录 ： 是
     *
     * @api POST /top-up-cards/:number
     *
     * @return json-object {
        "balance":5000 //余额（单位为分）。int。
        }
     * @example shell curl -X 'POST' 'http://api.iqianggou.com/top-up-cards/1181655762325567' -i -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     */
    public function topUp()
    {
        $this->output['http']['X-Content-Type'] = 'json-object';
        $this->checkLogin();
        preg_match('/^\/top-up-cards\/(?P<number>\d+)$/', $this->uri, $matches);
        $m = new \model\TopUpCard();
        $tmp = array(
            'cardNumber' => $matches['number'],
            'userId' => $this->client['userId'],
        );
        try {
            $r = $m->topUp($tmp);
        } catch (\exception\Model $e) {
            $tmp = $e->getData();
            $tmp['http']['X-Content-Type'] = 'json-object';
            throw new \exception\Http(substr($e->getCode(), 0, 3), '', $tmp);
        }

        $m = new \model\User();
        $r2 = $m->get($this->client['userId']);
        $this->output['data'] = array(
            'add' => \intval(\bcmul($r['add'], 100, 2)),
            'balance' => \intval(\bcmul($r2['balance'], 100, 2)),
        );
        $this->output['http']['X-Api-Status-Msg'] = str_replace(
            array(
                '{add}',
                '{balance}',
            ),
            array(
                $r['add'],
                $r2['balance'],
            ),
            _('您已成功充值{add}元，当前账户余额为{balance}元。')
        );
        return $this->output;
    }
}
?>
