<?php
namespace controller;

/**
 * 用户
 */
class User extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }

    /**
     * 批量取用户
     *
     * @api GET /users/
     *
     * @param string $sids 用户sid数组
     *
     * 需要登录 ： 否
     *
     * @return json-object {
        "entities":[
            {
                "nickname": "180****7530",
                "sid": "s1024"
            },
            {
                "nickname": "137****9550",
                "sid": "s1025"
            },
            {
                "nickname": "186****0132",
                "sid": "s1026"
            }
        ],
        "total":100
      }
     * @example shell curl 'http://api.iqianggou.com/users/?sids=\["s1024","s1025","s1026"\]' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     */
    public function multiGet($sids=null)
    {
        $this->checkParams(array('sids'));
        $sids = json_decode($this->input['sids'], true);
        $m = new \model\User();
        $r = $m->multiGet($sids);
        $data = array();
        foreach($r as $one) {
            $data[] = array(
                'sid' => $one['sid'],
                'nickname' => substr_replace($one['mobile'], '****', 3 ,4),
            );
        }
        $this->output['data'] = array(
            'entities' => $data,
            'total' => 1000,
        );
        return $this->output;
    }

    /**
     * 取自己的资料
     *
     * 需要登录 ： 是
     *
     * @api GET /users/me
     *
     * @return json-object {
        "balance":5000 //余额（单位为分）。int。
        "countryCallingCode":"+86", //国家电话区号。string
        "mobile":"13800138000" //手机号。string。
        "username":"" //用户名。大部分用户都没有用户名。
        }
     * @example shell curl 'http://api.iqianggou.com/users/me' -i -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     */
    public function getMe()
    {
        $this->checkLogin();
        $m = new \model\User();
        $r = $m->get($this->client['userId']);
        $this->output['data'] = array(
            'balance' => \intval(\bcmul($r['balance'], 100, 2)),
            'countryCallingCode' => $r['countryCallingCode'],
            'mobile' => $r['mobile'],
            'username' => $r['username'],
        );

        $e = new \model\Ecion();
        try {
            $e->giveLoginGift($this->client);
        } catch (\Exception $e) {
        }
        return $this->output;
    }

    /**
     * 注册。第一步申请下发短信注册码（必填appId、mobile），第二步注册。
     *
     * 需要登录 ： 否
     *
     * @api POST /users/
     *
     * @param int $appId 必填。Android为1，iPhone为2
     *
     * @param string $countryCallingCode 必填。国家电话区号。
     *
     * @param string $mobile 必填。手机号。
     *
     * @param string $vcode 第二步必填。验证码。
     *
     * @param string $password 第二步必填。密码。
     *
     * @return json-array []
     *
     * @example shell curl -d 'appId=1' -d 'countryCallingCode=%2B86' -d 'mobile=15312159527' 'http://api.iqianggou.com/users/' -i -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'Accept-Language: en-US'
     *
     * @example shell curl -d 'appId=1' -d 'countryCallingCode=%2B86' -d 'mobile=15312159527' -d 'password=1' -d 'vcode=356823' 'http://api.iqianggou.com/users/' -i -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'Accept-Language: en'
     */
    public function register($appId='', $countryCallingCode='', $mobile='', $vcode='', $password='')
    {
        $params = array(
            'appId',
            'countryCallingCode',
            'mobile',
        );
        $this->checkParams($params);
        $this->checkMobileFormat($this->input['mobile']);

        $m = new \model\User();
        if(isset($this->input['vcode'])) { //如果有验证码
            $params = array(
                'password',
            );
            $this->checkParams($params);
        }

        //注册
        $id = $m->add(array_merge($this->input, $this->client));
        $data = array(
            'id' => $id,
        );

        //送钱
        $m->showMeTheMoney($data);

        return $this->output;
    }

    /**
     * 忘记密码时重置密码
     *
     * 需要登录 ： 否
     *
     * @api PUT /users/password
     *
     * @param string $countryCallingCode 必填。国家电话区号。
     *
     * @param string $mobile 必填。手机号。
     *
     * @param string $vcode 第二步必填。验证码。
     *
     * @param string $newPassword 第二步必填。新密码。
     *
     * @return json-array []
     *
     * @example shell curl -X 'PUT' 'http://api.iqianggou.com/users/password' -d 'countryCallingCode=%2B86' -d 'mobile=15312159527' -i -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     *
     * @example shell curl -X 'PUT' 'http://api.iqianggou.com/users/password' -d 'countryCallingCode=%2B86' -d 'mobile=15312159527' -d 'vcode=964923' -d 'newPassword=1' -i -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     */
    public function resetPassword($countryCallingCode='', $mobile='', $vcode='', $newPassword='')
    {
        $this->checkParams(array('mobile'));
        $m = new \model\User();
        $m->resetPassword(array_merge($this->client, $this->input));
        //todo 返回 修改密码成功
        return $this->output;
    }

    /**
     * 修改自己的资料
     *
     * 允许修改手机号、密码、deviceToken、baiduPushUserId
     *
     * 需要登录 ： 是
     *
     * @api POST /users/me
     *
     * @param string $countryCallingCode 可选。国家电话区号。修改绑定手机号时需要。
     *
     * @param string $mobile 可选。手机号。修改绑定手机号时需要。
     *
     * @param string $vcode 可选。验证码。修改绑定手机号时第二步需要。
     *
     * @param string $deviceToken 可选。ios的push token。
     *
     * @param string $baiduPushUserId 可选。百度推送的user id。
     *
     * @return json-array []
     *
     * @example shell curl 'http://api.iqianggou.com/users/me' -d 'countryCallingCode=%2B86' -d 'mobile=15312159527' -i -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     * @example shell curl 'http://api.iqianggou.com/users/me' -d 'countryCallingCode=%2B86' -d 'mobile=15312159527' -d 'vcode=123456' -i -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     * @example shell curl 'http://api.iqianggou.com/users/me' -d 'deviceToken=daaf818ecae845dd103adf9368f8d31533ef6e75c0bddc597e51917b3261b007' -i -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     * @example shell curl 'http://api.iqianggou.com/users/me' -d 'baiduPushUserId=975009391496307785' -i -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     */
    public function updateMe($countryCallingCode='', $mobile='', $vcode='', $deviceToken='', $baiduPushUserId='')
    {
        $this->checkLogin();
        $m = new \model\User();
        $r = $m->update($this->client['userId'], array_merge($this->input, $this->client));
        return $this->output;
    }
}
?>
