<?php
namespace controller;

/**
 * 用户地址
 */
class Address extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }

    /**
     * 取自己的地址列表
     *
     * @api GET /addresses/
     *
     * 需要登录 ：是
     *
     * @return json-array [
        {
            "address": "one road, 561",
            "sid": "3353",
            "lat": 31.294469, 
            "lng": 121.501533
        },
        {
            "address": "guoquan road, 561", 
            "sid": "5810", 
            "lat": 131.21, 
            "lng": 33.01
        }
     ]
     * @example shell curl 'http://api.iqianggou.com/addresses/' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     */
    public function index()
    {
        $this->checkLogin();
        $m = new \model\Address();
        $r = $m->getRowsByUserId($this->client['userId']);
        $data = array();
        foreach($r as $sid=>$one) {
            $tmp = array(
                'sid' => strval($one['sid']),
                'address' => $one['address'],
                'lng' => floatval($one['lng']),
                'lat' => floatval($one['lat']),
            );
            ksort($tmp);
            $data[] = $tmp;
        }
        $this->output['data'] = $data;
        return $this->output;
    }

    /**
     * 添加一个收藏地址
     *
     * @api POST /addresses/
     *
     * address : 必须
     *
     * lng ：必须。经度
     *
     * lat ：必须。纬度
     *
     *
     * 需要登录 ：是
     *
     * @return json-object {
        "sid" : "123"
     }
     * @example shell curl 'http://api.iqianggou.com/addresses/' -i -d 'address=guoquan road, 561' -d 'lng=33.01' -d 'lat=131.21' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     */
    public function add()
    {
        $this->checkLogin();
        $m = new \model\Address();
        $data = $this->input;
        $data['ip'] = $_SERVER['REMOTE_ADDR'];
        $r = $m->add($this->client['userId'], $data);
        $this->output['data'] = array(
            'sid' => strval($r['sid']),
        );
        return $this->output;
    }

    /**
     * 删除一个地址
     *
     * 接口：DELETE /addresses/:sid
     *
     * 需要登录 ：是
     *
     * @return json-array []
     * @example shell curl -X 'DELETE' 'http://api.iqianggou.com/addresses/s10216' -i -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     */
    public function delete()
    {
        $this->checkLogin();
        preg_match('/^\/addresses\/(?P<sid>\w+)$/', $this->uri, $matches);
        $m = new \model\Address();
        $tmp = array(
            'sid' => $matches['sid'],
            'userId' => $this->client['userId'],
        );
        $m->delete($tmp);
        return $this->output;
    }
}
?>
