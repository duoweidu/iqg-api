<?php
/**
 * 通知类
 *
 * @author hui <catro@vip.qq.com>
 * @copyright 2013 Doweidu Network Technology Co., Ltd.
 */
namespace controller;
class Notice extends Base
{

    /**
     * 构造函数
     * @param string $uri URI
     */
    public function __construct($uri){
	parent::__construct($uri);
    }

    /**
     * 获取通知列表
     *
     * 需要登录：是
     *
     * @api GET /notice/
     *
     * @param int $isRead 可选。是否已读，0或1。如果不传，则取所有。
     *
     * @param int $limit 可选。取多少条。如果为0，则取0条，可用于只需要通知数量的场景。
     *
     * @example shell curl 'http://api.iqianggou.com/notice/' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'Accept-Language: zh-Hans' | python -m json.tool
     * @example shell curl 'http://api.iqianggou.com/notice/?isRead=0&limit=0' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'Accept-Language: zh-Hans' | python -m json.tool
     *
     * @return json-object {
        "entities" :[
            {
                "startTime": "1380438600", 
                "content": "asdf", 
                "isRead": true,
                "sid": "s81", 
                "title": "\u5317\u4eac\u67d0\u6d3b\u52a84"
            },
            {
                "startTime": "1380438578", 
                "content": "33333", 
                "isRead": false, 
                "sid": "s80", 
                "title": "\u4e0a\u6d77\u67d0\u6d3b\u52a83"
            }
        ],
        "total":16
     }
     */
    public function get($isRead=null, $limit=null)
    {
        $this->checkLogin();
        $m = new \model\Notice;
        $list = $m->getList(array_merge($this->client, $this->input));
        $this->output['data'] = $list;
        return $this->output;
    }

    /**
     * 阅读通知，此接口暂不使用
     *
     * @api POST /notice/:sid
     *
     * @param int $isRead 已读，值为1。
     *
     * @example shell curl 'http://api.iqianggou.com/notice/s1' -d 'isRead=1' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'Accept-Language: zh-Hans' | python -m json.tool
     * 
     */
    public function read($isRead = null)
    {
        $this->checkLogin();
        preg_match('/^\/notice\/(?P<sid>\w+)$/', $this->uri, $matches);
	$m = new \model\Notice();
	$m->setRead($this->client['userId'], $matches['sid']);
	return $this->output;
    }
}
