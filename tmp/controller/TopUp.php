<?php

/**
 * 充值
 *
 * @author hui <catro@vip.qq.com>
 * @copyright 2013 Doweidu Network Technology Co., Ltd.
 */
namespace controller;
class TopUp extends Base
{

    /**
     * 构造函数
     */
    public function __construct($uri)
    {
	parent::__construct($uri);
    }

    /**
     * 获取支付宝签名
     *
     * @api GET /top-up/alipay
     *
     * @param float $amount	必填，需要充值的金额
     * @param string OrderSid   可选 如果传了此参数说明是订单组合支付，不传表示直接给账户充值。
     * @param string $password  可选，但是如果$orderSid不为空，则必须要填写。
     *
     ** @example shell curl 'http://api.iqianggou.com/top-up/alipay' -d 'amount=7.5' -d 'orderSid=xxx' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     *
     * @return json-object {"sign":"partner=\"2088801342252957\"&seller=\"leiyong@doweidu.com\"&out_trade_no=\"0025886212429135\"&subject=\"[\u6d4b]\u7126\u7cd6\u8292\u679c\u5947\u4e50\u51b0\u4e2d\u676f(12oz)\"&body=\"[\u6d4b]\u7126\u7cd6\u8292\u679c\u5947\u4e50\u51b0\u4e2d\u676f(12oz)\"&total_fee=\"28.00\"&gmt_create=\"2013-10-22 16:21:44\"&pay_expire=\"15\"&notify_url=\"http:\/\/api.iqianggou.com\/alipaynotify\"&sign_type=\"RSA\"&sign=\"tjmii7yWlTFBcYiSEzq9CWbRrsfJwaD7MGOYO%2BhAo8%2BJbRygizm8fgDQi4lY%2BGN%2FRRWRjHo0uXTufZRVgQ41grQ819Sh9JddYvptgeE6mdWmLwi2xGy2bxwtZaF0srU3KNpZmd%2BcZpXoiYXr8xwBegmz1tJWKtMnaQSc%2BhDTj9A%3D\""} 
     */
    public function getAlipaySign($amount='', $orderSid=''){
	$this->checkParams(array('amount'));
	$this->checkLogin();
	if ( isset($this->input['orderSid']) ) {
	   $this->checkParams( array('password') ); 
	   $user = new \model\User();
	   $user->verifyPassword( $this->client['userId'], $this->input['password'] );
	}
	$alipay = new \model\Payment;
	$orderSid = isset($this->input['orderSid']) ? $this->input['orderSid'] : '';
	$sign = $alipay->getTopUpAlipaySign( $this->client['userId'], $this->input['amount']/100, $orderSid);
	$this->output['data']['sign'] = $sign;
	return $this->output;
    }

    /**
     * 充值 获取阿里web支付的URL
     * 
     * @api GET /top-up/aliwebpay
     *
     * @param float $amount	必填，需要充值的金额
     * @param float $orderSid	可选，如果填写表示组合支付。
     * @param string $password  可选，但是如果$orderSid不为空，则必须要填写。
     *
     * @example shell curl 'http://api.iqianggou.com/top-up/aliwebpay' -d 'amount=7.5' -d 'orderSid=xxx'  -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     *
     * @return json-object {"sid":"12312dldla3103120","url":"http:\/\/wappaygw.alipay.com\/service\/rest.htm?req_data=%3Cauth_and_execute_req%3E%3Crequest_token%3E2013102281ce0fc500caa553932a4beaf7c9e059%3C%2Frequest_token%3E%3C%2Fauth_and_execute_req%3E&service=alipay.wap.auth.authAndExecute&sec_id=0001&partner=2088801342252957&call_back_url=http%3A%2F%2Fapi.iqianggou.com%2Falipaywapcallback&format=xml&v=2.0&sign=aoJqo03tcjyIl09kgKKziOHHIfFJIlhi%2B6ibDkV2Pl3w%2BKmGZkPndRc3oPR6atAVd8JpAQru7b%2Brfj%2BfolQxkyKWc0bucyrvmHpSB0v2%2BJQ2UsejJdPA5DVoUgk3XPlL3BkHuubd0n907acntnuNdMtYjGISWuiDrdrea2otDYg%3D"}
     */
    public function getAliwebPayUrl($amount='', $orderSid='', $password=''){
	$this->checkParams( array('amount') );
    	$this->checkLogin();
	if ( isset($this->input['orderSid']) ) {
	   $this->checkParams( array('password') ); 
	   $user = new \model\User();
	   $user->verifyPassword( $this->client['userId'], $this->input['password'] );
	}
	$alipay = new \model\Payment;
	$this->output['data'] = $alipay->getTopupAliwebpayUrl( $this->client['userId'], $this->input['amount']/100, $this->input['orderSid']);
	return $this->output;
    }

    /**
     * 充值 获取paypal支付的信息
     * 
     * @api GET /top-up/paypal
     *
     * @param int    $amount	必填，需要充值的金额, 单位分
     * @param string $orderSid	可选，如果填写表示组合支付。
     *
     * @example shell curl 'http://api.iqianggou.com/top-up/paypal?amount=100'  -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     *
     * @return json-object {
	    "clientId" : "AcD_TxDiLiJnMnBMiiL74EFHWdtk7bjJvmK40dZnpQlgxVG0vmEC03Y3GfqT",
	    "receiverEmail" : "admin-facilitator@howlingwolfventures.com",
	    "shortDescription" : "爱抢购充值",
	    "amount" : 10
       }
     */
    public function getPaypalSign($amount='', $orderSid=''){
        $this->checkLogin();
	$this->checkParams( array('amount') );
    	$amount = $this->input['amount'] / 100;
        $payment = new \model\Payment;
	$orderSid = isset($this->input['orderSid']) ? $this->input['orderSid'] : '';
        $sign = $payment->getTopUpPaypalSign( $this->client['userId'], $amount, $orderSid );
        $this->output['data'] = $sign;
        return $this->output;
    }

    /**
     * 充值 获取paypal支付的信息
     * 
     * @api POST /top-up/paypal/verify
     *
     * @param string $proof	    必填，需要充值的金额
     * @param string $paymentSid    必填
     *
     * @example shell curl 'http://api.iqianggou.com/top-up/paypal/verify' -d 'proof=xxxx' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     * return void
     */
    public function verifyPaypalProof ($proof = '', $paymentSid='') {
	$this->checkLogin();
	$this->checkParams( array('proof', 'paymentSid') );
	$this->checkEmpty( array('proof', 'paymentSid') );
	$proof = $this->input['proof'];
	$proof = json_decode($proof, true);
	$paymentSid = $this->input['paymentSid'];
	$payment = new \model\Payment;
	$this->output['data'] = $payment->verifyPaypalProof($proof, $paymentSid, $this->client['lang']);
        return $this->output;
    }

    /**
     * 获取smoov pay支付的url
     *
     * @api GET /top-up/smoovpay
     *
     * @param int     $amount	    金额(单位分)
     * @param string  $orderSid	    可选
     *
     * @example shell curl 'http://api.iqianggou.com/top-up/smoovpay?amount=100' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 2-2533-1389864478-1392542878-e5d40075b24bb2aa25b8b8dd3ea1831e' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     * @return json-object {"sid":"210311231312031", "url":"https:\/\/sandbox.smoovpay.com/TokenAccess/47140d8b7eb58b3f1b6d923f4261cb9f"}
     */
    public function getSmoovPayUrl($amount='', $orderSid=''){
	$this->checkLogin();
	$this->checkParams(array('amount'));
	$orderSid = isset($this->input['orderSid']) ? $this->input['orderSid'] : '';
	$amount = $this->input['amount'] / 100;
        $payment = new \model\Payment;
	$this->output['data'] = $payment->getTopUpSmoovPayUrl( $this->client['userId'], $amount, $orderSid );
	return $this->output;
    }

    /**
     * 查询充值是否成功
     * @api GET /top-up/:sid/status
     *
     * @example shell curl 'http://api.iqianggou.com/top-up/1231u23139123/status' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 2-2533-1389864478-1392542878-e5d40075b24bb2aa25b8b8dd3ea1831e' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)'
     * @return  json-object {"isPaid":1, "addTime":12312312}
     */
    public function getStatus($sid = ''){
	$this->checkLogin();
	
	$matches = array();
	$matches['sid'] = '';

	preg_match('/^\/top-up\/(?P<sid>\w+)\/status$/', $this->uri, $matches);
	
	$sid = isset($matches['sid']) ? $matches['sid'] : '';
	
	if ( empty($sid) ){
	    throw new \exception\Http(404);
	}
	$payment = new \model\Payment;
	$topUpStatus = $payment->getTopUpStatus( $sid );
        if ( false === $topUpStatus ) {
            throw new \exception\Http(404);
        }
	$this->output['data'] = $topUpStatus; 
	return $this->output;	
    }
}
?>
