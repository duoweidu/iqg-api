<?php
namespace controller;
class Callback extends Base {

    public function __construct( $uri ) {
	parent::__construct( $uri );
    } 

    /**
     * 余额支付请求接口
     *
     * @api POST /callbacks/balance
     *
     * @param string $orderSid
     * @param string $sign 
     *
     * @example shell curl 'http://api.iqianggou.com/callbacks/balance' -d 'orderSid=1231312&sign=12311231122' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     *
     * @return void
     */
    public function balance( $orderSid = '', $sign = '' ) {
	$this->checkLogin();
	$this->checkParams( array('orderSid', 'sign') );
	$m = new \model\Payment;
	$m->balanceCallBack($this->input);
	return $this->output;	
    }

    public function alipay(){
	$this->output['http']['Content-Type'] =  'text/plain';
    }


    public function smoovpay(){
	$this->output['http']['Content-Type'] =  'text/plain';
	$smoovpay = new \model\Payment;
	if ( $smoovpay->smoovPayAccept( $this->input  ) ){ 
	    $this->output['data'] = 'success';	
	}else{
	    $this->output['data'] = 'fail';
	}
	return $this->output;
    }
}
