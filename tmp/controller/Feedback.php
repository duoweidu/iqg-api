<?php
namespace controller;
class Feedback extends Base
{

    /**
     * 构造函数
     */
    public function __construct($uri){
	parent::__construct($uri);
    }

    /**
     * 添加意见反馈
     *
     * @api POST /feedbacks/
     *
     * @example shell curl -d 'content=意见反馈的内容' 'http://api.iqianggou.com/feedbacks/' -i -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' -H 'Accept-Language: zh-Hans' -H 'X-LngLat: 121.498458,31.291391'
     *
     * @param string $content 内容
     * @return void
     */
    public function add($content)
    {
	$this->checkEmpty(array(
	    'content',
	));
	$this->checkLngLat();

	$data['uid']     = $this->client['userId'];
	$data['content'] = $this->input['content'];
	$data['addTime'] = time();
	$data['addIp']	 = $this->client['ip'];
	$data['lat']     = $this->client['lat'];
	$data['lng']     = $this->client['lng'];
	$data['userAgent']     = $this->client['userAgent'];

	$m = new \model\Feedback;
	$m->add($data);
	return $this->output;
    }
}
