<?php
namespace controller;

/**
 * 手机号
 */
class Mobile extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }

    /**
     * 批量取手机号是否已注册
     *
     * @api GET /mobiles/
     *
     * @param string $fullMobiles 用户sid数组
     *
     * @return json-object {
        "registered":["+8610000", "+8613800138000"]
        "invited":["+8610010", "+8613800138000"]
       }
     * @example shell curl 'http://api.iqianggou.com/mobiles/?fullMobiles=\["%2B8615312159527","%2B8613800138000"\]' -H 'Accept: application/json; version=trunk' -H 'Authorization: Bearer 1-156107-1393378870-1424914870-6f01f94929c03d880c0fe6a9de2d3b6f' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     */
    public function multiGet($fullMobiles=null)
    {
        $this->checkParams(array('fullMobiles'));
        $fullMobiles = json_decode($this->input['fullMobiles'], true);
        $m = new \model\User();
        $invitation = new \model\Invitation();
        $this->output['data'] = array(
            'registered' => $m->multiCheckRegistered($fullMobiles),
            'invited' => $invitation->multiCheckInvited($fullMobiles),
        );
        return $this->output;
    }
}
?>
