<?php
namespace controller;

/**
 * 分类
 */
class Category extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }

    /**
     * 取分类列表
     *
     * 需要登录 ：否
     *
     * @api GET /categories/
     *
     * @param string $citySid 可选。城市的sid。如果不传，表示所有城市。
     *
     * @return json-array [
        {
            "sid":"1",
            "name":"餐饮食品"
        },
        {
            "sid":"2",
            "name":"生活服务"
        }
     ]
     *
     * @example shell curl 'http://api.iqianggou.com/categories/?citySid=s21' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     */
    public function index($citySid='')
    {
        $m = new \model\Category();
        $r = $m->getList($this->input);
        $data = array();
        foreach($r as $sid=>$one) {
            $tmp = array(
                'sid' => strval($one['sid']),
                'name' => $one['name'],
            );
            $data[] = $tmp;
        }
        $this->output['data'] = $data;
        return $this->output;
    }
}
?>
