<?php
namespace controller;

/**
 * 城市
 */
class City extends Base
{
    public function __construct($uri)
    {
        parent::__construct($uri);
    }

    /**
     * 取城市列表
     *
     * 需要登录 ：否
     *
     * @api GET /cities/
     *
     * @return json-array [
        {
            "sid":"s21",
            "name":"上海",
            "address":"上海市杨浦区国权路",
            "lat": 31.234496,
            "lng": 121.463074
        },
        {
            "sid":"s10",
            "name":"北京",
            "address":"北京市海淀区",
            "lat": 39.915897,
            "lng": 116.452337
        }
     ]
     *
     * @example shell curl 'http://api.iqianggou.com/cities/' -H 'Accept: application/json; version=trunk' -A 'iqianggou/1.3.0 (Linux; Android 4.0.4; MI_1SC Build/FRG83D)' | python -m json.tool
     */
    public function index()
    {
        $m = new \model\City();
        $r = $m->getList();
        $data = array();
        foreach($r as $sid=>$one) {
            $tmp = array(
                'sid' => $one['sid'],
                'name' => $one['name'],
                'address' => $one['address'],
                'lat' => $one['lat'],
                'lng' => $one['lng'],
            );
            $data[] = $tmp;
        }
        $this->output['data'] = $data;
        return $this->output;
    }
}
?>
