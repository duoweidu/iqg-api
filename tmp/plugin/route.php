<?php
function route($uri)
{
    $rules = \PEAR2\Conf::get('route', 'execute');
    foreach ($rules as $k=>$v) {
        if (1 === preg_match('/' . $k . '/', $uri)) {
            $method = strtolower($_SERVER['REQUEST_METHOD']);
            if (isset($v[$method])) {
                return $v[$method];
            } else {
                throw new \exception\Model(4050);
            }
        }
    }
    throw new \exception\Model(4041, _('接口不存在'));
}
?>
