<?php
namespace model;
class Item extends Base
{
    private $itemDb;

    public function __construct()
    {
        $this->itemDb = new \model\dao\db\iqg2012\MurcielagoGoods();
        $this->itemShopDb = new \model\dao\db\iqg2012\MurcielagoGoodsShop();
    }

    public static function replaceImgUriToCdn($imgUrl) {
        $domain = \PEAR2\Conf::get('qiniu', 'all[\'buckets\'][\'img-agc\'][\'customDomain\']');
        return 'http://' . $domain . '/' . substr($imgUrl, -32);
    }

    public function get($args) {
    }

    public function getDetail($sid) {
        $where = array(
            'e' => array(
                'goodsId' => self::decryptSid($sid),
            ),
        );
        $r = $this->itemDb->selectRow($where, array('goodsDesc', 'goodsName'));
        if(empty($r)) {
            throw new \exception\Model(4044);
        }
        return array(
            'detail' => $r['goodsDesc'],
            'name' => $r['goodsName'],
        );
    }

    public function getList($args)
    {
        $sql = 'SELECT
            g.`expiredDays`, g.`enable`, g.`isDeleted`, g.`goodsDesc`, g.`isNeedBooking`, g.`isNotAllowedTakeOut`, g.`beginDate`, g.`endDate`, g.`bargainRange`, g.`adTitle`, g.`adUri`, g.`adUriTarget`, g.`goodsOptions`, g.`currentPrice`, g.`goodsId`, g.`imgId`, g.`imgUrl`, g.`countClickToday`, g.`countBargainToday`, g.`goodsBriefDesc`, g.`marketPrice`, g.`minPrice`, g.`goodsName`, g.`goodsNumber`, g.`isNew`, s.`shopLogo`, s.`shopId`, s.`buyCycle` ';
        if(!is_null($args['lat']) && !is_null($args['lng'])) {
            $sql .= ', getdistance(' . $args['lat'] . ',' . $args['lng'] . ', s.shopLat, s.shopLng) AS `distance`';
        }
        $sql .= ' FROM `murcielago_goods` AS g
            INNER JOIN `murcielago_goods_shop` as gs ON (gs.goodsId=g.goodsId)
            INNER JOIN `murcielago_shop` as s ON (gs.shopId=s.shopId)
            WHERE g.`type`=1';
        $countSql = 'SELECT count(*) as cnt FROM
                    `murcielago_goods` AS g
                    WHERE g.`type`=1';

        //如果指定sids，则都让看，比如以前分享的、订单里的商品，不能限制下架等等
        if(isset($args['sids']) && !empty($args['sids'])) {
            $tmp = ' AND g.`goodsId` IN (' . implode(',', self::decryptSids($args['sids'])) . ')';
            $sql .= $tmp;
            $countSql .= $tmp;
        } else {
            $tmp = ' AND g.`isDeleted`=0 AND g.`enable`=1 AND g.`beginDate` <= CURDATE() AND g.`endDate` >= CURDATE()';
            $sql .= $tmp;
            $countSql .= $tmp;
        }

        if(isset($args['categorySid']) && !empty($args['categorySid'])) {
            $tmp = ' AND g.`catId`=' . self::decryptSid($args['categorySid']);
            $sql .= $tmp;
            $countSql .= $tmp;
        }

        if(isset($args['citySid']) && !empty($args['citySid'])) {
            $tmp = ' AND (g.`cityId` = 0 OR g.`cityId`=' . self::decryptSid($args['citySid']) . ')';
            $sql .= $tmp;
            $countSql .= $tmp;
        }

        $sql .= ' ORDER BY g.sequence DESC'; //todo 商品应该按1 2 3排序，应该用g.sequence ASC，但大部分商品都0，就导致0 0 0 1 2 3 ，这样排序就错了。只能用DESC 3 2 1 0 0 0
        if(!is_null($args['lat']) && !is_null($args['lng'])) {
            $sql .= ' , `distance` ASC';
        }
        if(isset($args['limit']) && !empty($args['limit'])) {
            $sql .= ' LIMIT ' . intval($args['offset']) . ',' . $args['limit'];
        }

        $items = $this->itemDb->queryFetch($sql);
        $tmp = $this->itemDb->db->selectRow($countSql);
        $total = $tmp['cnt'];
        if(empty($items)) {
            return array(
                'entities' => array(),
                'total' => $total,
            );
        }

        $data = array();
        $itemsIds = array();
        $detailUri = \PEAR2\Conf::get('item', 'detailUri');
        foreach($items as $one) {
            //如果通兑商品，会出现好几次，用第一个即可，那是距离最近的。
            if(isset($data[$one['goodsId']])) {
                continue;
            }
            $itemsIds[] = $one['goodsId'];
            $isBargained = false;
            if(isset($args['userId'])) {
                $isBargained = $this->checkBargained($one['goodsId'], $args['userId']);
            }

            $one['shelfStatus'] = 'on'; //已上架
            if (($one['isDeleted'] == 1) || ($one['enable'] == 0) ||  (strtotime($one['endDate'] . ' 23:59:59') < time())) {
                $one['shelfStatus'] = 'off'; //已下架
            } else if (strtotime($one['beginDate']) > time()) {
                $one['shelfStatus'] = 'comingSoon'; //即将上架
            }

            $one['isNew'] = false;
            if (strtotime($one['beginDate']) >= time() - 3600 * 24 * 3) {
                $one['isNew'] = true;
            }
            $sid = strval(self::encryptId($one['goodsId']));
            $data[$one['goodsId']] = array(
                'ad' => array(
                    'title' => $one['adTitle'],
                    'uri' => $one['adUri'],
                    //'uriTarget' => $one['adUriTarget'],
                ),
                'bargainRange' => intval(bcmul($one['bargainRange'], 100, 2)),
                'currentPrice' => intval(bcmul($one['currentPrice'], 100, 2)),
                'sid' => $sid,
                'imgs' => array(
                    self::replaceImgUriToCdn($one['imgUrl']),
                    Shop::replaceImgUriToCdn($one['shopLogo']),
                ),
                'intro' => $one['goodsBriefDesc'],
                'isBargained' => $isBargained,
                'isNew' => (bool)$one['isNew'],
                'likeCount' => intval($one['countClickToday'] + $one['countBargainToday']),
                'marketPrice' => intval(bcmul($one['marketPrice'], 100, 2)),
                'minPrice' => intval(bcmul($one['minPrice'], 100, 2)),
                'name' => $one['goodsName'],
                'notice' => str_replace('每人每天', '每人每' . $one['buyCycle'] . '天', $one['goodsOptions']),
                'shelfStatus' => $one['shelfStatus'],
                'stock' => intval($one['goodsNumber']),
                'isNeedBooking' => boolval($one['isNeedBooking']), //需预约
                'isNotAllowedTakeOut' => boolval($one['isNotAllowedTakeOut']), //限堂吃
                'detailUri' => '', //详情链接
                'orderTtl' => intval($one['expiredDays']), //订单有效期
            );
            if(!empty($one['goodsDesc'])) {
                $data[$one['goodsId']]['detailUri'] = str_replace('{sid}', $sid, $detailUri);
            }
        }

        $sql = 'SELECT gs.`goodsId`, s.`shopId`';
        if(!is_null($args['lat']) && !is_null($args['lng'])) {
            $sql .= ', getdistance(' . $args['lat'] . ',' . $args['lng'] . ', s.shopLat, s.shopLng) AS `distance`';
        }
        $sql .= ' FROM `murcielago_shop` AS s,
                `murcielago_goods_shop` AS gs
                WHERE gs.`goodsId` IN (' . implode($itemsIds, ',') . ') 
                AND gs.`shopId` = s.`shopId`';
        if(!is_null($args['lat']) && !is_null($args['lng'])) {
            $sql .= ' ORDER BY `distance` ASC';
        }
        $r = $this->itemShopDb->db->selectRows($sql);
        foreach($r as $one) {
            $data[$one['goodsId']]['shopsSids'][] = self::encryptId($one['shopId']);
        }
        return array(
            'entities' => $data,
            'total' => $total,
        );
    }

    /**
     * 今天是否砍过同1商品
     */
    private function checkBargained($itemId, $userId)
    {
        $todayStart = strtotime(date('Y-m-d', time()));
        $where = array(
            'e' => array(
                'uid' => $userId,
                'goodsId' => $itemId,
            ),
            'gte' => array(
                'addTime' => $todayStart,
            ),
            'lt' => array(
                'addTime' => $todayStart + 86400,
            ),
        );
        $logBargainDb = new \model\dao\db\iqg2012\MurcielagoLogBargain();
        $count = $logBargainDb->selectCount($where);
        if($count !== 0) {
            return true;
        }
        return false;
    }

    public function addCountClick($itemId)
    {
        $sql = 'UPDATE murcielago_goods SET countClickToday=countClickToday+1 WHERE `goodsId`=' . $itemId . ' LIMIT 1';
        return $this->itemDb->query($sql);
    }

    public function bargain($itemSid, $input)
    {
        $itemId = self::decryptSid($itemSid);
        $where = array(
            'e' => array(
                'goodsId' => $itemId,
            ),
        );
        $column = array(
            'bargainRange',
            'currentPrice',
            'minPrice',
            'goodsNumber',
            'countClickToday',
            'countBargainToday',
        );
        $item = $this->itemDb->selectRow($where, $column);
        if(empty($item)) {
            throw new \exception\Model(4044);
        }
        
        $data = array(
            'currentPrice' => intval(bcmul($item['currentPrice'], 100, 2)),
            'isBargained' => false,
            'likeCount' => intval($item['countClickToday'] + $item['countBargainToday'] + 1),
            'priceReduced' => 0,
            'stock' => intval($item['goodsNumber']),
        );

        if($item['goodsNumber'] <=0) { //抢完了
            throw new \exception\Model(4033, '', $data);
        }
        if(\bccomp($item['currentPrice'], $item['minPrice'], 2) <=0) { //到底了
            throw new \exception\Model(40313, '', $data);
        }

        //砍过了
        $r = $this->checkBargained($itemId, $input['userId']);
        if($r === true) {
            $data['isBargained'] = true;
            throw new \exception\Model(4032, '', $data);
        }

        $newRange = $item['bargainRange'];
        //如果砍完低于底价，则不能砍那么多，应砍到底。
        if(bcsub($item['currentPrice'], $item['bargainRange'], 2) < $item['minPrice']) {
            $newRange = bcsub($item['currentPrice'], $item['minPrice'], 2);
        }
        $sql = 'UPDATE murcielago_goods SET countBargainToday=countBargainToday+1, `currentPrice`=`currentPrice`-' . $newRange . ' WHERE `goodsId`=' . $itemId . ' AND `goodsNumber`>0 AND `currentPrice`>=`minPrice`+' . $newRange . ' LIMIT 1';
        $this->itemDb->query($sql);

        //记录砍价
        $newPrice = bcsub($item['currentPrice'], $newRange, 2);
        $record = $input;
        $record['prevPrice'] = $item['currentPrice'];
        $record['currentPrice'] = $newPrice;

        $where = array(
            'e' => array(
                'goodsId' => $itemId,
            ),
        );
        $r = $this->itemShopDb->selectRow($where);
        $record['shopId'] = $r['shopId'];

        $this->recordBargain($itemId, $record);

        $data['currentPrice'] = intval(bcmul($newPrice, 100, 2));
        $data['isBargained'] = true;
        $data['priceReduced'] = intval(bcmul($newRange, 100, 2));
        return $data;
    }

    /**
     * 记录砍价
     * @todo 业务记录 应和 log 分开。log不是给业务用的。
     */
    private function recordBargain($itemId, $data)
    {
        $record = array(
            'shopId' => $data['shopId'],
            'goodsId' => $itemId,
            'uid' => $data['userId'],
            'bargainRange' => bcsub($data['prevPrice'], $data['currentPrice'], 2), //无用，应从数据库删除。
            'prevPrice' => $data['prevPrice'],
            'currentPrice' => $data['currentPrice'],
            'lat' => $data['lat'],
            'lng' => $data['lng'],
            'addIp' => $data['ip'],
            'addTime' => time(),
        );
        $logBargainDb = new \model\dao\db\iqg2012\MurcielagoLogBargain();
        return $logBargainDb->insertRow($record);
    }
}
?>
