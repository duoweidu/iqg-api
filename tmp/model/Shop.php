<?php
namespace model;
class Shop extends Base
{
    private $shopDb;

    public function __construct()
    {
        $this->shopDb = new \model\dao\db\iqg2012\MurcielagoShop();
    }
 
    public static function replaceImgUriToCdn($shopLogo) {
        $domain = \PEAR2\Conf::get('qiniu', 'all[\'buckets\'][\'img-agc\'][\'customDomain\']');
        return 'http://' . $domain . '/' . substr($shopLogo, -32);
    }
   
    public static function formatDistance($dis) {
        //数据库函数计算出来的都是千米，需要处理
        $dis = \bcmul($dis, 1000, 0);
        if(\bccomp($dis, '1000', 0) < 0) {
            $d = \bcdiv($dis, 10, 0) * 10;
        } else{
            $d = \bcdiv($dis, 100, 0) * 100;
        }
        return $d;
    }

    public function multiGet($shopsSids, $point=array(
            'lat' => null,
            'lng' => null
        )
    )
    {
        if(empty($shopsSids)) {
            throw new \exception\Model(4001);
        }
        $where = array(
            'e' => array(
                'shopId' => self::decryptSids($shopsSids),
                //'isHidden' => 0, //门店没有隐藏功能
            ),
        );
        $r = $this->shopDb->selectRows($where);
        if(empty($r)) {
            throw new \exception\Model(4045);
        }
        $data = array();
        foreach($r as $one) {
            $lat = round($one['shopLat'], 6);
            $lng = round($one['shopLng'], 6);
            $data[$one['shopId']] = array(
                'address' => $one['shopAddress'],
                'businessHoursOpen' => $one['businessHoursOpen'],
                'businessHoursClose' => $one['businessHoursClose'],
                'citySid' => self::encryptId($one['cityId']),
                'desc' => $one['shopDesc'],
                'distance' => null,
                'imgs' => array(
                    \PEAR2\Iqg\Uri::getImg('img-agc', $one['logoId']),
                ),
                'lat' => $lat,
                'lng' => $lng,
                'name' => $one['shopName'],
                'sid' => self::encryptId(intval($one['shopId'])),
                'tel' => strval($one['shopTel']),
            );
            if(!is_null($point['lat']) && !is_null($point['lng'])) {
                $data[$one['shopId']]['distance'] = intval(\PEAR2\Lbs::calculateDistance($point, array('lat'=> $lat, 'lng'=>$lng)));
            }
        }
        usort($data, function($a, $b) {
            return $a['distance'] - $b['distance'];
        });
        return $data;
    }
}
?>
