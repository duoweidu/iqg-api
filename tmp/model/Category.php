<?php
namespace model;
class Category extends Base
{
    private $categoryDb;

    public function __construct()
    {
        $this->categoryDb = new \model\dao\db\iqg2012\MurcielagoCategory();
    }

    public function getList($args = array())
    {
	$orderBy = 'sortOrder ASC';
	$columns = array('catId', 'catName');
        $wheres = array(
            'e' => array(
                'isHidden' => 0,
            ),
        );
	if ( isset($args['citySid']) ) {
	    $wheres['e']['cityId'] = self::decryptSid($args['citySid']);
	    $wheres['o']['cityId'] = "= '0'";
        }
	$r = $this->categoryDb->selectRows($wheres, $columns, '', '', '', $orderBy);
        $data = array();
        foreach($r as $one) {
            $data[$one['catId']] = array(
                'sid' => strval(self::encryptId($one['catId'])),
                'name' => $one['catName'],
            );
        }
        return $data;
    }
}
?>
