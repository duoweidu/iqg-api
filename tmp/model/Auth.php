<?php
namespace model;
class Auth
{
    private $appDb;

    public function __construct()
    {
        $this->appDb = new \model\dao\db\iqg2013\App();
    }

    /**
     * 使用手机号 或者 用户名登录
     */
    public function login($data)
    {
        $userDb = new \model\dao\db\iqg2012\MurcielagoUser();
        if(isset($data['mobile'])) {
            $where = array(
                'e' => array(
                    'countryCallingCode' => $data['countryCallingCode'],
                    'mobile' => $data['mobile'],
                ),
            );
        } else {
            $where = array(
                'e' => array(
                    'username' => $data['username'],
                ),
            );
        }
        $user = $userDb->selectRow($where);
        if(empty($user)) {
            throw new \exception\Model(4013);
        }

        //封号
        if($user['isForbidden'] == 1) {
            throw new \exception\Model(4015);
        }

        if(\PEAR2\Crypt::verify($data['password'], $user['password'])) {
            return array(
                'token' => $this->getToken($data['appId'], $user['uid']),
            );
        } else {
            throw new \exception\Model(4031);
        }
    }

    /**
     * 获得token
     */
    public function getToken($appId, $userId)
    {
        $conf = \PEAR2\Conf::get('auth', 'token');
        $loginTime = time();
        $expireTime = $loginTime + $conf['ttl'];

        $r = $this->appDb->selectRow(array(
                'e' => array(
                    'id'=>$appId
                ),
            ),
            'secret'
        );

        //token 由 明文和密文组成，明文可以直接解析出数据，密文用于校验token合法性。
        return $appId . '-' . $userId . '-' . $loginTime . '-' . $expireTime . '-' . md5($appId . $userId . $loginTime . $expireTime . $r['secret']);
    }

    public function verifyToken($token)
    {
        $tokenData = self::decodeToken($token);

        //检查过期时间
        if($tokenData['expireTime'] < time()) {
            throw new \exception\Model(4011);
        }

        //检查appId
        $app = $this->appDb->selectRow(array(
                'e' => array(
                    'id'=>$tokenData['appId'],
                )
            ),
            'secret'
        );
        if(empty($app)) {
            throw new \exception\Model(4012);
        }

        //对比hash
        if($tokenData['hash'] != md5($tokenData['appId'] . $tokenData['userId'] . $tokenData['loginTime'] . $tokenData['expireTime'] . $app['secret'])) {
            throw new \exception\Model(4011);
        }

        $userDb = new \model\dao\db\iqg2012\MurcielagoUser();
        $user = $userDb->selectRow(array(
            'e' => array(
                'uid'=>$tokenData['userId']
            )),
            array(
                'isForbidden',
            )
        );
        //用户不存在
        if(empty($user)) {
            throw new \exception\Model(4013);
        }
        //封号
        if($user['isForbidden'] == 1) {
            throw new \exception\Model(4015);
        }

        /*
        //如果改了密码，则所有以前的登录都失效。
        if(!empty($user['passwordUpdateTime']) && $tokenData['loginTime'] < $user['passwordUpdateTime']) {
            throw new \exception\Model(4011);
        }*/

        return $tokenData;
    }

    public static function decodeToken($token)
    {
        $r = explode('-', $token);
        return array(
            'appId' => $r[0],
            'userId' => $r[1],
            'loginTime' => $r[2],
            'expireTime' => $r[3],
            'hash' => $r[4],
        );
    }
}
?>
