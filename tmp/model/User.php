<?php
namespace model;
class User extends Base
{
    private $userDb;

    public static $smsTypes = array(
        'register'      => 0,
        'resetPassword' => 2,
        'bindMobile'    => 3,
        'inviteFriend'  => 4,
    );

    public function __construct()
    {
        $this->userDb = new \model\dao\db\iqg2012\MurcielagoUser;
        $this->moneyChangeDb = new \model\dao\db\iqg2012\MurcielagoMoneychange;
        $this->orderPaymentDb = new \model\dao\db\iqg2012\MurcielagoOrderPayment;
    }
    
    public function add($data)
    {
        $this->checkRegistered($data['countryCallingCode'], $data['mobile']);
        if (!isset($data['vcode'])) {
            $this->sendVcode($data, 'register');
            throw new \exception\Model(4002, _('校验码短信已发送，请查收。'));
        }
        $this->useVcode($data, 'register');

        $tmp = array(
            'countryCallingCode' => $data['countryCallingCode'],
            'mobile' => $data['mobile'],
            'password' => \PEAR2\Crypt::hash($data['password']),
            'addIp' => $data['ip'],
            'addTime' => time(),
        );
        $r = $this->userDb->insertRow($tmp);
        if(empty($r)) {
            throw new \exception\Model(5000);
        }
        return $r;
    }

    public function get($id)
    {
        $where = array(
            'e' => array(
                'uid' => $id,
            ),
        );
        $column = array(
            'countryCallingCode',
            'mobile',
            'money',
            'username',
        );
        $r = $this->userDb->selectRow($where, $column);
        if(empty($r)) {
            throw new \exception\Model(4041);
        }
        return array(
            'countryCallingCode' => $r['countryCallingCode'],
            'mobile' => $r['mobile'],
            'balance' => floatval($r['money']),
            'username' => $r['username'],
        );
    }

    public function multiGet($sids)
    {
        $where = array(
            'e' => array(
                'uid' => self::decryptSids($sids),
            ),
        );
        $column = array(
            'uid',
            'countryCallingCode',
            'mobile',
        );
        $r = $this->userDb->selectRows($where, $column);
        if(empty($r)) {
            throw new \exception\Model(4041);
        }
        $data = array();
        foreach($r as $one) {
            $data[] = array(
                'sid' => self::encryptId($one['uid']),
                'countryCallingCode' => $one['countryCallingCode'],
                'mobile' => $one['mobile'],
            );
        }
        return $data;
    }

    /**
     * 批量检查手机号 是否已注册
     */
    public function multiCheckRegistered($fullMobiles)
    {
        $code = \PEAR2\Conf::get('sms', 'defaultCountryCallingCode');
        $registered = array();
        foreach($fullMobiles as $fullMobile) {
            try {
                //如果有的手机号格式错误，跳过
                $tmp = Invitation::decodeFullMobile($fullMobile, $code);
            } catch (\Exception $e) {
                continue;
            }
            try {
                $this->checkRegistered($tmp['countryCallingCode'], $tmp['mobile']);
            } catch (\Exception $e) {
                $registered[] = $fullMobile;
                continue;
            }
        }
        return $registered;
    }
    
    /**
     * 检查手机号 是否已注册
     */
    public function checkRegistered($countryCallingCode, $mobile)
    {
        $where = array(
            'e' => array(
                'countryCallingCode' => $countryCallingCode,
                'mobile'  => $mobile,
            ),
        );
        $r = $this->userDb->selectCount($where);
        if($r>=1) {
            throw new \exception\Model(4039);
        }
        return false;
    }

    /**
     * 检查验证码 是否已发过
     */
    private function checkVcodeSent($countryCallingCode, $mobile, $type)
    {
        $ttl = \PEAR2\Conf::get('sms', 'ttl');
        $where = array(
            'e' => array(
                'smsType' => self::$smsTypes[$type],
                'countryCallingCode' => $countryCallingCode,
                'mobile'  => $mobile,
                'enable'  => 1,
            ),
            'gt' => array(
                'addTime' => time() - $ttl,
            ),
        );
        $logSmsDb = new \model\dao\db\iqg2012\MurcielagoLogSms();
        $cnt = $logSmsDb->selectCount($where);
        $oneUserMaxLimitInTtl = \PEAR2\Conf::get('sms', 'oneUserMaxLimitInTtl'); //每人每天最多发几条
        if($cnt>=$oneUserMaxLimitInTtl) {
            throw new \exception\Model(4037);
        }

        $repeatInterval = \PEAR2\Conf::get('sms', 'repeatInterval'); //重发间隔，多少秒一次
        $where['gt']['addTime'] = time() - $repeatInterval;
        $r = $logSmsDb->selectCount($where);
        if($r>=1) {
            throw new \exception\Model(4038, '', array('repeatInterval' => $repeatInterval));
        }
        return $cnt;
    }

    /**
     * 重置密码
     */
    public function resetPassword($data)
    {
        $r = true;
        try {
            $r = $this->checkRegistered($data['countryCallingCode'], $data['mobile']);
        } catch (\exception\Model $e) {
        }

        if($r === false) {
            //未注册
            throw new \exception\Model(4013);
        }

        if (!isset($data['vcode'])) {
            $this->sendVcode($data, 'resetPassword');
            throw new \exception\Model(4002, _('校验码短信已发送，请查收。'));
        }
        if (!isset($data['newPassword'])) {
            throw new \exception\Model(4002, 'newPassword');
        }
        $this->useVcode($data, 'resetPassword');
        $where = array(
            'e' => array(
                'countryCallingCode' => $data['countryCallingCode'],
                'mobile' => $data['mobile'],
            ),
        );
        $tmp = array(
            'password' => \PEAR2\Crypt::hash($data['newPassword']),
        );
        $r = $this->userDb->update($tmp, $where); //todo true false
        return true;
    }

    /**
     * 发送验证码（注册、绑定手机、修改手机、找回密码）
     */
    public function sendVcode($data, $type)
    {
        $cnt = $this->checkVcodeSent($data['countryCallingCode'], $data['mobile'], $type);
        //每次发送不同的，每个都有效。
        $logSmsDb = new \model\dao\db\iqg2012\MurcielagoLogSms();
        $vcodes = array(rand(1,9));
        $n = 5;
        for ($i = 1; $i <= $n; $i++) {
            $vcodes[] = rand(0, 9);
        }
        $vcode = implode('', $vcodes);

        switch($type) {
        case 'register' :
            $content = _('注册校验码：{vcode}。如非本人操作，请忽略本短信。');
            break;
        case 'resetPassword' :
            $content = _('找回密码校验码：{vcode}。如非本人操作，请忽略本短信。');
            break;
        case 'bindMobile' :
            $content = _('绑定手机校验码：{vcode}。如非本人操作，请忽略本短信。');
            break;
        case 'inviteFriend' :
            $content = _('好友{nickname}已经通过爱抢购App省了几百元，吃喝玩乐真的可以砍到1元钱，跟我一起砍价，一起省吧！立戳爱抢购App（http://iqianggou.com/）');
            break;
        }
        $content = str_replace('{vcode}', $vcode, $content);
        if(isset($data['nickname'])) {
            $content = str_replace('{nickname}', $data['nickname'], $content);
        }

        $gateways = \PEAR2\Conf::get('sms', 'gateways');
        $gateway = isset($gateways[$data['countryCallingCode']]) ? $gateways[$data['countryCallingCode']] : $gateways['default'];
        $conf = \PEAR2\Conf::get('sms', $gateway);

        $log = array(
            'smsType' => self::$smsTypes[$type],
            'countryCallingCode'  => $data['countryCallingCode'],
            'mobile'  => $data['mobile'],
            'ucode'   => $vcode,
            'content' => $content,
            'enable'  => 1,
            'addIp'   => $data['ip'],
            'addTime' => time(),
        );

        $c = new \PEAR2\Services\Sms($gateway, $conf);
        try {
            $r = $c->send($data['countryCallingCode'] . $data['mobile'], $content);
        } catch (\Exception $e) {
            //$log['enable'] = 0; //如果发送失败，可以看后台来注册
            $logSmsDb->insertRow($log);
            throw new \exception\Model(5000, '短信发送失败');
        }

        $logSmsDb->insertRow($log);
        return true;
    }
    
    /**
     * 使用验证码
     */
    public function useVcode($data, $type)
    {
        $ttl = \PEAR2\Conf::get('sms', 'ttl');
        $where = array(
            'e' => array(
                'smsType' => self::$smsTypes[$type],
                'countryCallingCode'  => $data['countryCallingCode'],
                'mobile'  => $data['mobile'],
                'enable'  => 1,
                'ucode'   => $data['vcode'],
            ),
            'gt' => array(
                'addTime' => time() - $ttl,
            ),
        );
        $logSmsDb = new \model\dao\db\iqg2012\MurcielagoLogSms();
        $r = $logSmsDb->selectRow($where, array('logSmsId'));
        if(empty($r)) {
            throw new \exception\Model(4220, '验证码错误');
        }

        //把这个用户的所有短信都失效。因为可能重发了多条。
        $where = array(
            'e' => array(
                'smsType' => self::$smsTypes[$type],
                'countryCallingCode'  => $data['countryCallingCode'],
                'mobile'  => $data['mobile'],
                'enable'  => 1,
            )
        );
        $change = array(
            'enable' => 0,
        );
        $r = $logSmsDb->update($change, $where);
        return true;
    }

    /**
     * 修改个人资料
     */
    public function update($id, $data)
    {
        if(isset($data['password'])) {
            $this->updatePassword($id, $data);
        }
        if (isset($data['mobile'])) {
            $this->updateMobile($id, $data);
        }

        $tmp = array();
        if (isset($data['deviceToken'])) {
            $tmp['deviceToken'] = str_replace(' ', '', $data['deviceToken']);
        }
        if (isset($data['baiduPushUserId'])) {
            $tmp['baiduPushUserId'] = $data['baiduPushUserId'];
        }
        if(!empty($tmp)) {
            $where = array(
                'e' => array(
                    'uid' => $id,
                ),
            );
            $this->userDb->update($tmp, $where);
        }
 
        return true;
    }

    /**
     * 绑定、或修改手机号
     */
    private function updateMobile($id, $data)
    {
        if (!isset($data['mobile'])) {
            throw new \exception\Model(4002, 'mobile');
        }
        $this->checkRegistered($data['countryCallingCode'], $data['mobile']);
        if (!isset($data['vcode'])) {
            $this->sendVcode($data, 'bindMobile');
            throw new \exception\Model(4002, _('校验码短信已发送，请查收。'));
        }
        $this->useVcode($data, 'bindMobile');
        $where = array(
            'e' => array(
                'uid' => $id,
            ),
        );
        $tmp = array(
            'countryCallingCode'  => $data['countryCallingCode'],
            'mobile' => $data['mobile'],
        );
        $r = $this->userDb->update($tmp, $where); //@todo true false
        return true;
    }

    /**
     * 修改密码
     * @todo 未完成，暂不需要
     */
    private function updatePassword($id, $data)
    {
        if (!isset($data['password']) || !isset($data['newPassword'])) {
            throw new \exception\Model(4002, 'newPassword, password');
        }
        if (empty(trim($data['newPassword'])) || empty(trim($data['password'])) || $data['password'] != $data['newPassword']) {
            throw new \exception\Model(4001, 'newPassword, password');
        }
        $where = array(
            'e' => array(
                'uid' => $id,
            ),
        );
        $column = array(
            'password',
        );
        $user = $this->userDb->selectRow($where, $column);
        //todo
        return true;
    }

    /** 
     * 扣余额 with  订单
     * 
     */
    public function dcdMoneyWitchOrder($uid, $money, $orderId){
        $sql = "UPDATE `murcielago_user` set money=money-{$money} WHERE uid='{$uid}' AND money >= {$money} AND (SELECT COUNT(1) FROM `murcielago_order` WHERE orderId={$orderId} AND payStatus = 0 AND orderStatus = 0 ) > 0";
        $affectedRows = $this->userDb->db->update($sql); 
        if( $affectedRows > 0 ) {
            $sql = 'INSERT INTO `murcielago_moneychange` (`uid`, `money`, `addTime`, `remark`) VALUES (' . $uid . ', ' . -$money . ', ' . time() . ', \'' . _('支付') . '\')';
            return $this->moneyChangeDb->db->insertRow($sql) > 0 ? true : false;
        }
        return false;
    }
    
    /**
     * 送钱
     * @todo 发push，发站内信
     */
    public function showMeTheMoney($data)
    {
        if (!isset($data['id']) || empty($data['id'])) {
            throw new \exception\Model(4002);
        }

        $money = \PEAR2\Conf::get('money', 'register');
        $card = \PEAR2\Conf::get('money', 'register_gift_card_number');
        
        $where = array(
            'e' => array(
                'uid' => $data['id'],
                'cardNumber' => $card,
            ),
        );
        $cnt = $this->moneyChangeDb->selectCount($where);
        if($cnt == 0) {
            $sql = 'UPDATE `murcielago_user` set money=money+' . $money . ' WHERE uid=' . $data['id'] . ' LIMIT 1';
            $affectedRows = $this->userDb->db->update($sql); 
            if($affectedRows == 0) {
                throw new \exception\Model(5000);
            }

            $sql = 'INSERT INTO murcielago_moneychange (uid, money, remark, addTime, cardNumber) VALUES (' . $data['id'] . ',' . $money . ',' . '\'' . _('注册赠送') . '\',' . time() . ',\'' . $card . '\')';
            $this->userDb->db->query($sql);
        }
        return true;
    }


    public function topup($paymentSid)
    {
        $sql = "update `murcielago_user` set `money` = `money` + IFNULL((select amount from murcielago_order_payment where paymentSn='$paymentSid' and `isPaid`=0), 0) where uid=IFNULL((select uid from murcielago_order_payment where paymentSn='$paymentSid' and `isPaid`=0), '')";    
        $r = $this->orderPaymentDb->db->update($sql);    
        if ( $r > 0 ) {
           $sql = "select uid, amount from `murcielago_order_payment` where paymentSn='$paymentSid' and `isPaid`=0";
           $rw = $this->orderPaymentDb->db->selectRow($sql);
           $sql = "INSERT INTO `murcielago_moneychange` (`uid`, `money`, `addTime`, `remark`) VALUES ('{$rw['uid']}', '{$rw['amount']}', '".time()."', '". _('充值')  ."')";
           $this->moneyChangeDb->db->query($sql);
           $sql = "update `murcielago_order_payment` set isPaid=1 where paymentSn='$paymentSid'";
           $this->orderPaymentDb->db->update($sql);

           // 赠送
           $m = new \model\Conf;
           $giveMoney = $m->getTopupGivePlan($rw['amount']);
           if ( is_numeric($giveMoney) && $giveMoney > 0 ){
               $sql = "update `murcielago_user` set `money` = `money` + $giveMoney where `uid`='{$rw['uid']}' ";
               $this->orderPaymentDb->db->update($sql); 
               $sql = "INSERT INTO `murcielago_moneychange` (`uid`, `money`, `addTime`, `remark`) VALUES ('{$rw['uid']}', '$giveMoney', '".time()."', '" . _('充值赠送'). "')";
               $this->moneyChangeDb->db->query($sql);
           }
        }
    }

    /**
     * 检查密码
     */
    public function verifyPassword($uid, $password)
    {
        $where = array(
            'e' => array(
                'uid' => $uid,
            ),
        );
        $user = $this->userDb->selectRow($where, 'password');
        if(empty($user) || \PEAR2\Crypt::verify($password, $user['password']) == false) {
            throw new \exception\Model(4031);
        }
        return true;
    }
}
?>
