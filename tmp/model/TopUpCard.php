<?php
namespace model;
class TopUpCard extends Base
{
    private $topUpCardDb;
    private $moneyChangeDb;

    public function __construct()
    {
        $this->topUpCardDb = new \model\dao\db\iqg2012\MurcielagoCards();
        $this->moneyChangeDb = new \model\dao\db\iqg2012\MurcielagoMoneychange();
    }

    public function checkCardCategory($userId, $categoryId)
    {
        $where = array(
            'e' => array(
                'id' => $categoryId,
            )
        );
        $topUpCardsCategoryDb = new \model\dao\db\iqg2012\MurcielagoCardsCategory();
        $tmp = $topUpCardsCategoryDb->selectRow($where, array('paymentNumbers', 'expiredTime'));
        
        // 判断是否过期
        $expiredTime = $tmp['expiredTime'];
        if ( $expiredTime != 0 && $expiredTime <= time() ) {
            throw new \exception\Model(40318);
        } 

        //这批卡一个人能冲几张
        $paymentNumbers = $tmp['paymentNumbers'];
        if($paymentNumbers > 0) {
            $where = array(
                'e' => array(
                    'uid' => $userId,
                ),
                'ne' => array(
                    'cardNumber' => null,
                ),
            );
            $usedCards = $this->moneyChangeDb->selectRows($where, array('cardNumber'));
            //如果以前冲过卡，则检测是不是同一批
            if(!empty($usedCards)) {
                foreach($usedCards as $one) {
                    $cardsNumbers[] = $one['cardNumber'];
                }
                $where = array(
                    'e' => array(
                        'cardNumber' => $cardsNumbers,
                        'categoryId' => $categoryId,
                    )
                );
                $cnt = $this->topUpCardDb->selectCount($where);
                if($cnt >= $paymentNumbers) {
                    throw new \exception\Model(40313, '', array(
                            'paymentNumbers' => $paymentNumbers,
                        )
                    );
                }
            }
        }
    }

    public function topUp($input)
    {
        //查卡面额
        $where = array(
            'e' => array(
                'cardNumber' => $input['cardNumber'],
                'isEnabled' => 1,
            ),
        );
        $column = array(
            'money',
            'status',
            'categoryId',
        );
        $card = $this->topUpCardDb->selectRow($where, $column);
        if(empty($card)) {
            throw new \exception\Model(4042);
        }
        if($card['status'] == 2) {
            throw new \exception\Model(4036);
        }
        
        $this->checkCardCategory($input['userId'], $card['categoryId']);

        // 充值
        $sql = 'UPDATE `murcielago_user` SET money=money+' . $card['money'] . ' WHERE uid=' . $input['userId'] . ' LIMIT 1';
        $this->topUpCardDb->query($sql);

        // 作废卡
        $data = array(
            'status' => 2,
        );
        $this->topUpCardDb->update($data, $where);

        //记账单
        $userDb = new \model\dao\db\iqg2012\MurcielagoUser();
        $user = $userDb->selectRow(
            array(
                'e' => array(
                    'uid' => $input['userId']
                )
            ),
            array('money')
        );
        $data = array(
            'uid' => $input['userId'],
            'money' => $card['money'],
            'remark' => _('充值'),
            'addTime' => time(),
            'balance' => $user['money'],
            'cardNumber' => $input['cardNumber'],
        );
        $this->moneyChangeDb->insertRow($data);

        return array(
            'add' => $card['money'],
        );
    }
}
?>
