<?php
namespace model;
class Conf extends Base
{
    private $topupGivePlan = null;

    public function __construct()
    {
	$this->topupGivePlan = new \model\dao\db\iqg2012\MurcielagoTopupGiveplan();
    }

    public function getTopupGivePlan($money='')
    {
	$time = time();
	$sql = "select money, give from `murcielago_topup_giveplan` where beginTime <= $time and endTime >= $time and stop=0 order by give desc";
        $res = $this->topupGivePlan->db->selectRows($sql);
	
	if ( $money > 0 ) {
	    foreach($res as $val) {
		if ( $money >= $val['money'] ){
		    return ($val['give']/$val['money']) * $money;
		}
	    }
	}else{
	    foreach($res as $key => $val){
		$res[$key]['price'] = $val['money']*100;  
		$res[$key]['free'] = $val['give']*100;  
		unset($res[$key]['money']);
		unset($res[$key]['give']);
	    }
	    return $res;
	}

	return false;
    }
}
?>
