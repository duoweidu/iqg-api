<?php
/**
 * @author dinghui <catro@vip.qq.com>
 * @author yangzhou <yangzhou@doweidu.com>
 * @copyright Doweidu Network Technology Co., Ltd.
 */
namespace model;
class Notice extends Base
{
    
    /**
     * @var MurcielagoNotice $noticeDb
     */
    private $noticeDb;

    /**
     * 构造函数
     */
    public function __construct(){
        $this->noticeDb = new \model\dao\db\iqg2012\MurcielagoNotice;
    }
    
    /**
     * 发通知
     */
    public function add($input) {
        $now = time();
        $data = array(
            'uid' => $input['userId'],
            'title' => $input['title'],
            'content' => $input['content'],
            'addTime' => $now,
            'startTime' => $now,
            'expireTime' => $now + 8640000,
        );
        return $this->noticeDb->insertRow($data);
    }

    /**
     * 获取通知列表
     * @param array $args 参数
     */
    public function getList($args) {
        $userId = $args['userId'];
        $now = time();

        //所有在有效期内的通知
        $sql = 'SELECT id, title, content, startTime FROM `murcielago_notice` WHERE (uid=' . $userId .' OR uid is null) AND startTime <= '. $now .' AND expireTime >= ' . $now . ' AND (cityId is null OR cityId=(SELECT cityId FROM `murcielago_user` WHERE uid=' . $userId . ')) ORDER BY startTime DESC';
        $all = $this->noticeDb->db->selectRows($sql);
        $data = array(
            'entities' => array(),
            'total' => 0,
        );
        if(empty($all)) {
            return $data;
        }
        $ids = array();
        foreach($all as $one) {
            $ids[] = $one['id'];
        }

        //我看过的通知
        $sql = 'SELECT noticeId FROM `murcielago_notice_status` WHERE isRead=1 AND uid=' . $userId . ' AND noticeId IN (' . implode(',', $ids) . ')';
        $r = $this->noticeDb->db->selectRows($sql);
        $readIds = array();
        foreach($r as $one) {
            $readIds[] = $one['noticeId'];
        }

        $unreadIds = array_diff($ids, $readIds);

        if(isset($args['isRead'])) {
            foreach($all as $one) {
                if((($args['isRead'] == 0) && !in_array($one['id'], $readIds)) || (($args['isRead'] == 1) && in_array($one['id'], $readIds))) {
                    $one['sid'] = self::encryptId($one['id']); 
                    $one['isRead'] = in_array($one['id'], $readIds) ? true : false;
                    unset($one['id']);
                    $data['entities'][] = $one;
                }
            }
        } else {
            foreach($all as $one) {
                $one['sid'] = self::encryptId($one['id']);
                $one['isRead'] = in_array($one['id'], $readIds) ? true : false;
                unset($one['id']);
                $data['entities'][] = $one;
            }
        }
        $data['total'] = count($data['entities']);

        if(isset($args['limit'])) {
            if($args['limit'] == 0) {
                $data['entities'] = array();
            }
        } else {
            if(!empty($unreadIds)) {
                foreach($unreadIds as $id) {
                    try {
                        $this->setReadById($userId, $id);
                    } catch (\model\Exception $e) {
                    }
                }
            }
        }

	return $data;
    }

    public function setRead($userId, $sid) {
        $id = self::decryptSid($sid);
        return $this->setReadById($userId, $id);
    }

    /**
     * 阅读
     */
    private function setReadById($userId, $id) {
        $db = new \model\dao\db\iqg2012\MurcielagoNoticeStatus;
        $data = array(
            'noticeId' => $id,
            'uid' => $userId,
            'isRead' => 1,
        );
        try {
	    $db->insertRow($data);
        } catch(\PDOException $e) {
            throw new \exception\Model(5000, 'had read');
        }
        return true;
    }
}
?>
