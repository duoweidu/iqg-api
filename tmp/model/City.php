<?php
namespace model;
class City extends Base
{
    private $cityDb;

    public function __construct()
    {
        $this->cityDb = new \model\dao\db\iqg2012\MurcielagoCity();
    }

    public function getList()
    {
        $where = array(
            'e' => array(
                'isHidden' => 0,
            ),
        );
        $columns = array(
            'cityId',
            'cityName',
            'address',
            'lng',
            'lat',
        );
        $orderBy = 'sortOrder ASC';
        $r = $this->cityDb->selectRows($where, $columns, '', '', '', $orderBy);
        $data = array();
        foreach($r as $one) {
            $data[$one['cityId']] = array(
                'sid' => self::encryptId($one['cityId']),
                'name' => $one['cityName'],
                'address' => $one['address'],
                'lng' => floatval($one['lng']),
                'lat' => floatval($one['lat']),
            );
        }
        return $data;
    }
}
?>
