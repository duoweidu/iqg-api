<?php
/**
 * 支付Model
 * 
 * @author dinghui <catro@vip.qq.com>
 * @copyright 2013 Doweidu Network Technology Co., Ltd.
 */
namespace model;
class Payment extends Base
{

    const ALIPAY = 1;
    const BALANCE = 2;
    const UPS=3;
    const PAYPAL = 4;
    const SMOOVPAY = 5;

    /**
     * 构造函数
     *
     */
    public function __construct()
    {
	parent::__construct();
    }

    public function getTopUpStatus($paymentSid){
	$orderPaymentDb = new \model\dao\db\iqg2012\MurcielagoOrderPayment();
	$paymentId = $this->decryptPaymentSid($paymentSid);
	if ( !empty($paymentId) ) {
	    $where = array(
		'e' => array(
		    'id' => $paymentId,
		),
	    );
	    $res = $orderPaymentDb->selectRow($where, array('isPaid'));
            $res['isPaid'] = intval($res['isPaid']);
            return $res;
	}
	return false;
    }


    /**
     * 获取充值的快捷支付签名
     *
     * @param string	$amount
     * @param int	$uid
     * @param string	$orderSid 可空
     * return string 签名
     */ 
    public function getTopUpAlipaySign($uid, $amount, $orderSid = null){
	if ( bccomp ($amount, 0, 2) < 0 ){ // amount 必须大于0
	    throw new \exception\Model(4001, 'the param amount must larger than zero!');
	}
	$m = new \model\Order;
	//1=alipay, 2=balance, 3=ups, 4=paypal
	$topUpOrder = $m->makeTopUpOrder($uid, $amount, 1, $orderSid); 

	$alipayData['subject']      = $topUpOrder['goodsName'];
	$alipayData['body']         = $topUpOrder['goodsName'];
	$alipayData['out_trade_no'] = $topUpOrder['paymentSid'];
	$alipayData['total_fee']    = $amount;
	$alipayData['gmt_create']   = date('Y-m-d H:i:s', $topUpOrder['addTime']);
	$alipayData['pay_expire']   = '15';

	$conf = \PEAR2\Conf::get('alipay', 'alipay');
	$key = \PEAR2\Conf::get('alipay', 'alipayKey');

	$alipayConf = array_merge($conf, $key);
	$alipay = new \PEAR2\Service\Alipay($alipayConf, $orderSid);

	return $alipay->getAlipaySign($alipayData);
    }

    /**
     * 获取充值阿里web支付的url
     * 
     * @param string $orderSid 订单的orderSid
     * @return string  客户端应该请求的网址
     */
    public function getTopUpAliwebpayUrl($uid, $amount, $orderSid='') {
	if ( bccomp ($amount, 0, 2) < 0 ){ // amount 必须大于0
	    throw new \exception\Model(4001, 'the param amount must larger than zero!');
	}
	$m = new \model\Order;
	$topUpOrder = $m->makeTopUpOrder($uid, $amount, 1, $orderSid); 

	$conf = \PEAR2\Conf::get('alipay', 'aliwebpay');
	$key = \PEAR2\Conf::get('alipay', 'alipayKey');
	$alipayConf = array_merge($conf, $key);
	$alipay = new \PEAR2\Service\Alipay($alipayConf, $orderSid);

	$data = array();
	$data['subject'] = $topUpOrder['goodsName'];
	$data['out_trade_no'] = $topUpOrder['paymentSid'];
	$data['total_fee'] = $topUpOrder['payPrice'];
	$data['out_user'] = $uid;

	$res = array();
	$res['url'] = $alipay->getAliwebpayUrl($data);
	$res['sid'] = $topUpOrder['paymentSid'];
	return $res;
    }



    /**
     * 获取订单的支付宝快捷支付的签名
     *
     * @param string $orderSid 订单的sid
     *
     * @return string 签名
     */
    public function getOrderAlipaySign($orderSid)
    {
	$m = new \model\Order;
	if ( empty($orderSid) ){
	    throw new \exception\Model(4043);
	}

	$orderAndGoods = $m->getOrderGoods($orderSid);
	$alipayData['subject']      = $orderAndGoods['goods']['goodsName'];
	$alipayData['body']         = $orderAndGoods['goods']['goodsName'];
	$alipayData['out_trade_no'] = $orderSid;
	$alipayData['total_fee']    = $orderAndGoods['order']['payPrice'];
	$alipayData['gmt_create']   = date('Y-m-d H:i:s', $orderAndGoods['order']['addTime']);
	$alipayData['pay_expire']   = '15';

	$conf = \PEAR2\Conf::get('alipay', 'alipay');
	$key = \PEAR2\Conf::get('alipay', 'alipayKey');

	$alipayConf = array_merge($conf, $key);
	$alipay = new \PEAR2\Service\Alipay($alipayConf);

	return $alipay->getAlipaySign($alipayData);
    }

    /**
     * 获取充值的快捷支付签名
     *
     * @param string	$amount
     * @param int	$uid
     * @param string	$orderSid 可空
     * return string 签名
     */ 
    public function getTopUpPaypalSign($uid, $amount, $orderSid = null){
	if ( bccomp ($amount, 0, 2) < 0 ){ // amount 必须大于0
	    throw new \exception\Model(4001, 'the param amount must larger than zero!');
	}
	$m = new \model\Order;
	$topUpOrder = $m->makeTopUpOrder($uid, $amount, 4, $orderSid); 

	$conf = \PEAR2\Conf::get('paypal', 'paypal');

	$data['receiverEmail'] = $conf['receiverEmail'];
	$data['clientId'] = $conf['clientId'];
	$data['shortDescription'] = $topUpOrder['goodsName'];
	$data['amount'] = $amount*100;
	$data['paymentSid'] = $topUpOrder['paymentSid'];
	return $data;

    }

    /**
     * 获取阿里web支付的url
     * 
     * @param string $orderSid 订单的orderSid
     * @return string  客户端应该请求的网址
     */
    public function getOrderAliwebpayUrl($orderSid){
	$m = new \model\Order;
	$orderAndGoods = $m->getOrderGoods( $orderSid );
	$conf = \PEAR2\Conf::get('alipay', 'aliwebpay');
	$key = \PEAR2\Conf::get('alipay', 'alipayKey');
	$alipayConf = array_merge($conf, $key);
	$alipay = new \PEAR2\Service\Alipay($alipayConf);

	$data = array();
	$data['subject'] = $orderAndGoods['goods']['goodsName'];
	$data['out_trade_no'] = $orderSid;
	$data['total_fee'] = $orderAndGoods['order']['payPrice'];
	$data['out_user'] = $orderAndGoods['order']['uid'];

	return $alipay->getAliwebpayUrl($data);
    }


    /**
     * 余额回调接口
     */
    public function balanceCallBack($params){
	$orderSid = $params['orderSid'];

	$m = new \model\Order;
	$order = $m->getOrder($orderSid);

	if ( empty($order) ) {
	    throw new \exception\Model(4043);
	}

	if ( $order['orderStatus'] != 0 ) {
            switch ( $order['orderStatus'] ) {
                case 1 :
                    throw new \exception\Model(40314);
                    break;
                default :
                    throw new \exception\Model(4043);
                    break;
            }
        }

	// 扣除用户的钱
	$m = new \model\User;
	$dcdRes = $m->dcdMoneyWitchOrder($order['uid'], $order['payPrice'], $order['orderId']);
	if ( $dcdRes == true ) {
	    $this->allotEcode($orderSid);
            $this->updateOrderToSuccess($order['orderId'], self::BALANCE);
        }else{
            throw new \exception\Model(40317);
            return false;
        }
	return true;
    }
    
    /**
     * 修改订单状态为 成功
     */
    private function updateOrderToSuccess($orderId, $payway)
    {
        $orderDb = new \model\dao\db\iqg2012\MurcielagoOrder();
        return $orderDb->update(
            array(
                'orderStatus' => 1,
                'payStatus' => 1,
                'payway' => $payway,
            ),
            array(
                'e' => array(
                    'orderId' => $orderId,
                )
            )
        );
    }

    private function allotEcode($orderSid){
	$orderModel = new \model\Order();
	$orderGoods = $orderModel->getOrderGoods($orderSid);

	$goodsShopDb = new \model\dao\db\iqg2012\MurcielagoGoodsShop();
	$where = array(
	    'e' => array(
		'goodsId' => $orderGoods['goods']['goodsId']
	    )
	);
	$goodsShop = $goodsShopDb->selectRow($where);

	$shopEcodePoolDb = new dao\db\iqg2012\MurcielagoShopEcodepool();
	$where = array(
	    'e' => array(
		'shopId' => $goodsShop['shopId'],
		'isMultiShop' => $orderGoods['goods']['isMultiShop'],
	    ),  
	); 
	$shopEcodePool = $shopEcodePoolDb->selectRow($where, array('poolId'));		

	// 发码
	$conf = \PEAR2\Conf::get('ecode', 'api'); 
	$this->iqgEcode = new \PEAR2\Iqg\Ecode($conf);
	$ecode = $this->iqgEcode->allot($shopEcodePool['poolId']);

	$where = array(
	    'e' => array(
		'orderId' => $orderGoods['order']['orderId'],
	    ),  
	);  

	$data = array(
	    'distributionNo' => $ecode['ecodes'][0] 
	);  
	$orderDb = new \model\dao\db\iqg2012\MurcielagoOrder();
	$orderDb->update($data, $where);
    }

    public function verifyPaypalProof($proof, $paymentSid, $lang){
	$conf = \PEAR2\Conf::get('paypal', 'paypal');
	$config = array(
	   'mode'	     => $conf['mode'],
	   'acct1.UserName'  => $conf['apiUsername'],
	   'acct1.Password'  => $conf['apiPassword'],
	   'acct1.Signature' => $conf['signature'], 
	);

	if ( isset($proof['proof_of_payment']['adaptive_payment']) )
	{
	    $requestEnvelope = new \PayPal\Types\Common\RequestEnvelope;
	    $requestEnvelope->errorLanguage = $lang;

	    $paymentDetailsRequest = new \PayPal\Types\AP\PaymentDetailsRequest($requestEnvelope);
	    $paymentDetailsRequest->payKey = $proof['proof_of_payment']['adaptive_payment']['pay_key'];

	    $config['acct1.AppId'] = $proof['proof_of_payment']['adaptive_payment']['app_id'];

	    $service = new \PayPal\Service\AdaptivePaymentsService($config);

	    $response = $service->PaymentDetails($paymentDetailsRequest);

	    $ack = strtoupper( $response->responseEnvelope->ack );

	    if ( $ack == 'SUCCESS') {
		// 付款成功，加钱
		$m = new \model\User;
		$m->topup($paymentSid);
	    }
	    return true;
	}
	else if ( isset($proof['proof_of_payment']['rest_api']) )
	{
	   $apiContext = new \PayPal\Rest\ApiContext(
		new \PayPal\Auth\OAuthTokenCredential($conf['clientId'], $conf['secret'])
           ); 
	   $apiContext->setConfig(array(
		'mode' => $conf['mode'],
                'http.ConnectionTimeOut' => 30,
	   ));
	   $sale = \PayPal\Api\Sale::get($proof['proof_of_payment']['rest_api']['payment_id'], $apiContext); 
	   if ( $sale->state == 'completed' ) {
		$m = new \model\User;
		$m->topup($paymentSid);	
	   }
	   return true;
	}
	return false;
    }

    public function getTopUpSmoovPayUrl($uid, $amount, $orderSid){
	$conf = \PEAR2\Conf::get('smoovpay', 'smoovpay');
	if ( bccomp ($amount, 0, 2) < 0 ){ 
	    throw new \exception\Model(4001, 'the param amount must larger than zero!');
	}

	$m = new \model\Order;
	$topUpOrder = $m->makeTopUpOrder($uid, $amount, 5, $orderSid); 

	$smoovpay = new \PEAR2\Service\SmoovPay($conf);

	$data = array();

	$data['item_name_1']	    = $topUpOrder['goodsName'];
	$data['item_description_1'] = $topUpOrder['goodsDescription'];
	$data['item_quantity_1']    = 1;
	$data['item_amount_1']	    = $topUpOrder['payPrice'];
	$data['ref_id'] = $topUpOrder['paymentSid'];

	$res = array();
	$res['url'] = $smoovpay->getSmoovPayUrl($data);
	$res['sid'] = $topUpOrder['paymentSid'];
	return $res;
    }

    public function smoovPayAccept( $params  ) {
	$conf = \PEAR2\Conf::get('smoovpay', 'smoovpay');
	$smoovpay = new \PEAR2\Service\SmoovPay($conf);
	$paymentSid = $smoovpay->isPaidSuccess($params); 
	if ($paymentSid !== false ){
	    $m = new \model\User;
	    $m->topup($paymentSid);
	    return true; 
	}else{
	    return false;
	}
    }
}

