<?php
namespace model;
class Cbd extends Base
{
    private $cbdDb;

    public function __construct()
    {
        $this->cbdDb = new \model\dao\db\iqg2012\MurcielagoCbd();
    }

    public function getList($input)
    {
        $where = array(
            'e' => array(
                'cityId' => self::decryptSid($input['citySid']),
            ),
        );
        $cbds = $this->cbdDb->selectRows($where, array('name', 'id', 'lat', 'lng'), $input['offset'], $input['limit'], '', 'sortOrder ASC');
        $total = $this->cbdDb->selectCount($where);
        if(empty($cbds)) {
            return array(
                'entities' => array(),
                'total' => $total,
            );
        }
        $data = array();
        foreach($cbds as $cbd) {
            $data[] = array(
                'name' => $cbd['name'],
                'sid' => self::encryptId($cbd['id']),
                'lat' => round($cbd['lat'], 6),
                'lng' => round($cbd['lng'], 6),
            );
        }
        return array(
            'entities' => $data,
            'total' => $total,
        );
    }
}
?>
