<?php
namespace model\dao\db;
abstract class Base
{
    protected $conn;
    public $db;
    protected $dbFakeName;
    protected $tableName;
    
    public function __construct()
    {
        if (empty($this->db)) {
            $tmp = \PEAR2\Conf::get('db', 'db');
            $db_conf = $tmp[$this->dbFakeName];
            $tmp = '\\PEAR2\db\\' . \PEAR2\Str::firstToUpper($db_conf['api']);
            switch($db_conf['api']) {
            case 'pdo' :
                $this->db = new \PEAR2\db\Pdo();
                break;
            }
            $this->db = new $tmp ();
            $this->conn = $this->db->connect($db_conf);
            $tmp = $db_conf;
            $tmp['tableName'] = $this->tableName;
            $this->sql = new \PEAR2\sql\Maker($db_conf['dbProduct'], $this->conn, $tmp);
        }
    }

    public function connect($db_conf)
    {
        switch($db_conf['api']) {
        case 'pdo' :
            break;
        case 'mysqli' :
            //http://www.php.net/manual/zh/mysqli.construct.php
            $this->conn = new mysqli(
                $db_conf['host'], $db_conf['username'], $db_conf['password'],
                $db_conf['dbName'], $db_conf['port'], $db_conf['unix_domain_socket']
            ); //todo persistent
            break;
        case 'pgsql' :
            //http://php.net/manual/en/function.pg-connect.php
            $host = $db_conf['host'];
            if (!empty($db_conf['unix_domain_socket'])) {
                $host = $db_conf['unix_domain_socket'];
            }
            $this->conn = pg_connect(
                'host=' . $host . ' port=' . $db_conf['port'] . ' dbname='
                . $db_conf['dbName'] . ' user=' . $db_conf['username']
                . ' password=' . $db_conf['password']
            );
            break;
        }
        return true;
    }

    public function delete($where='')
    {
        $sql = $this->sql->delete($where);
        return $this->db->delete($sql);
    }
    
    public abstract function insertRow($data);
    
    public abstract function insertRows($data);
    
    public function selectCount($where='')
    {
        $sql = $this->sql->selectCount($where);
        return $this->db->selectCount($sql);
    }
    
    public function selectRow($where='', $column='*',
        $limit_start=0, $group_by='', $order_by=''
    ) {
        $sql = $this->sql->select(
            $where, $column, $limit_start, 1, $group_by, $order_by
        );
        return $this->db->selectRow($sql);
    }
    
    public function selectRows($where='', $column='*',
        $limit_start=0, $limit_size='', $group_by='', $order_by=''
    ) {
        $sql = $this->sql->select(
            $where, $column, $limit_start,
            $limit_size, $group_by, $order_by
        );
        return $this->db->selectRows($sql);
    }
    
    public abstract function update($data, $where='');
    
    public function query($sql)
    {
        return $this->db->query($sql);
    }

    public function queryFetch($sql)
    {
        return $this->db->queryFetch($sql);
    }
}
?>
