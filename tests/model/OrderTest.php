<?php
require_once __DIR__ . '/../../src/plugin/autoload.php';
class OrderTest extends PHPUnit_Framework_TestCase
{
    public function atestAdd()
    {
        $data = array(
            'itemSid' => '505449',
            'userId' => '156107',
            'ip' => '127.0.0.1',
            'lat' => 0,
            'lng' => 0,
        );
     
        $c = new \model\Order();
        try {
            $r = $c->add($data);
        } catch (\Exception $e) {
            var_dump($e->getCode());
            var_dump($e->getMessage());
        }

        var_dump($r);
        $this->assertEquals(true, $r);
    }

    public function testGetList()
    {
        $data = array(
            'userId' => '166036',
            'status' => 2,
            'offset' => 0,
            'limit' => 20,
            //'isCommented' => 1,
        );
     
        $c = new \model\Order();
        try {
            $r = $c->getList($data);
        } catch (\Exception $e) {
            var_dump($e->getCode());
            var_dump($e->getMessage());
        }

        var_dump($r);
        $this->assertEquals(false, $r['entities'][0]['isCommented']);
    }
}
