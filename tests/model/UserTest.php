<?php
require_once __DIR__ . '/../../src/plugin/autoload.php';
class UserTest extends PHPUnit_Framework_TestCase
{
    public function testAdd()
    {
        $data = array(
            'countryCallingCode' => '+86',
            'mobile' => '18068065227',
            'vcode' => '159051',
            'password' => '1',
            'ip' => '127.0.0.1',
        );
     
        $c = new \model\User();
        $r = $c->add($data);

        var_dump($r);
        $this->assertEquals(true, $r);
    }

    public function atestGiveGiftToInviter()
    {
        $data = array(
            'countryCallingCode' => '+86',
            'mobile' => '18068065227',
        );
        $c = new \model\User();
        $r = $c->giveGiftToInviter($data);

        var_dump($r);
        $this->assertEquals(true, $r);
    }

    public function atestMultiCheckRegistered()
    {
        $fullMobiles = array(
            '+8615312159527',
            '+8613800138000',
            '+86138',
        );
        $c = new \model\User();
        $r = $c->multiCheckRegistered($fullMobiles);

        var_dump($r);
        $this->assertEquals(true, $r);
    }

    public function atestSendVcode()
    {
        $data = array(
            'ip' => '127.0.0.1',
            'countryCallingCode' => '+86',
            'mobile' => '18068065227',
            'addTime' => time(),
            'nickname' => 'jim',
        );
        $c = new \model\User();
        $r = $c->sendVcode($data, 'inviteFriend');

        var_dump($r);
        $this->assertEquals(true, $r);
    }
}
?>
