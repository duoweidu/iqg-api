<?php
require_once __DIR__ . '/../../src/plugin/autoload.php';
class FileTest extends PHPUnit_Framework_TestCase
{
    public function testUpload()
    {
        $data = array(
            'filename' => '1.jpg',
            'filePath' => '/home/yangzhou/1.jpg',
            'contentType' => 'image/jpeg',
            'uid' => '156108',
        );
     
        $c = new \model\File();
        $r = $c->upload($data);

        var_dump($r);
        $this->assertEquals(true, isset($r['httpUri']));
    }
}
