<?php
require_once __DIR__ . '/../../src/plugin/autoload.php';
class CommentTest extends PHPUnit_Framework_TestCase
{
    public function atestAdd()
    {
        $data = array(
            'orderSid' => '2734055292955034',
            'userId' => '156107',
            'content' => 'test',
            'ip' => '127.0.0.1',
            'imgs' => array('71f6950d988e75b9b68d1ffcc2d3b5da', 'b8e7128ccd943c0725aa05b9440030e0'),
        );
     
        $c = new \model\Comment();
        try {
            $r = $c->add($data);
        } catch (\Exception $e) {
            var_dump($e->getCode());
            var_dump($e->getMessage());
        }

        var_dump($r);
        $this->assertEquals(true, $r);
    }

    public function testGetList()
    {
        $data = array(
            'userId' => '156107',
            'offset' => 0,
            'limit' => 20,
        );
     
        $c = new \model\Comment();
        try {
            $r = $c->getList($data);
        } catch (\Exception $e) {
            var_dump($e->getCode());
            var_dump($e->getMessage());
        }

        var_dump($r);
        $this->assertEquals(true, $r);
    }
}
