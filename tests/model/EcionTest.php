<?php
require_once __DIR__ . '/../../src/plugin/autoload.php';
class EcionTest extends PHPUnit_Framework_TestCase
{
    public function testGetStatus()
    {
        $input = array(
            'userId' => '156107',
        );
        $c = new \model\Ecion();
        $r = $c->getStatus($input);

        var_dump($r);
        $this->assertEquals(true, $r);
    }

    public function atestGiveLoginGift()
    {
        $input = array(
            'userId' => '155',
        );
        $c = new \model\Ecion();
        $r = $c->giveLoginGift($input);

        var_dump($r);
        $this->assertEquals(true, $r);
    }

    public function atestGiveGift()
    {
        $input = array(
            'userId' => '21173',
            'type' => 'invite',
        );
        $c = new \model\Ecion();
        $r = $c->giveGift($input);

        var_dump($r);
        $this->assertEquals(true, $r);
    }

    public function testExchange()
    {
        $input = array(
            'userId' => '156107',
            'subtotal' => '123',
        );
        $c = new \model\Ecion();
        $r = $c->exchange($input);

        var_dump($r);
        $this->assertEquals(true, $r);
    }

    public function atestGetList()
    {
        $input = array(
            'userId' => '155',
            'type' => '4',
            'offset' => 0,
            'limit' => 7,
        );
        $c = new \model\Ecion();
        $r = $c->getList($input);

        var_dump($r);
        $this->assertEquals(true, $r);
    }
}
?>
