<?php
require_once __DIR__ . '/../../src/plugin/autoload.php';
class InvitationTest extends PHPUnit_Framework_TestCase
{
    public function atestgetInviter()
    {
        $c = new \model\Invitation();
        $r = $c->getInviter('+86', '18068065227');
        var_dump($r);

        $this->assertEquals(true, $r);
    }

    public function testAdd()
    {
        $data = array(
            'ip' => '127.0.0.1',
            'userId' => '156107',
            'fullMobiles' => array('18068065227', '+8613671671796'),
            'addTime' => time(),
            'nickname' => 'jim',
        );
        $c = new \model\Invitation();
        $r = $c->add($data);
        var_dump($r);

        $this->assertEquals(true, $r);
    }

    public function atestGetHistory()
    {
        $data = array(
            'userId' => '21173',
            'offset' => 0,
            'limit' => 2,
        );
        $c = new \model\Invitation();
        $r = $c->getHistory($data);
        var_dump($r);

        $this->assertEquals(true, $r);
    }
}
?>
